<!doctype html>
<html>
<head>
    <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no' />
    <meta charset='utf-8'>
    <link rel="stylesheet" type="text/css" href="styles.css">
    <link rel="manifest" href="manifest.json">
    <style></style>
    <title>Super Pool</title>
    <script src="playcanvas-stable.min.js"></script>
    <script>
        ASSET_PREFIX = "";
        SCRIPT_PREFIX = "";
        SCENE_PATH = "805540.json";
        CONTEXT_OPTIONS = {
            'antialias': true,
            'alpha': false,
            'preserveDrawingBuffer': true,
            'preferWebGl2': true
        };
        SCRIPTS = [ 23193083, 23193084, 23193086, 23193082, 23193085, 23193081, 23193107, 22515121, 22542535, 22546526, 22758646, 22561790, 23915846, 22601280, 22741188, 22748272, 22839060, 22885098, 22990888, 23019914, 23193116, 23193119, 23193120, 23193121, 23195802, 23195830, 23195896, 23197013, 23208520, 23212359, 23306530, 23383809, 23412377, 23414413, 23414471, 23586239, 23702269, 23719520, 24702574, 24708897 ];
        CONFIG_FILENAME = "config.json";
        INPUT_SETTINGS = {
            useKeyboard: true,
            useMouse: true,
            useGamepads: false,
            useTouch: true
        };
        pc.script.legacy = false;
        PRELOAD_MODULES = [
        ];
    </script>
</head>
<body>
    <script src="__start__.js"></script>
    <script src="__loading__.js"></script>
<script type="text/javascript">
var app = pc.Application.getApplication();
app.on('initialize',function(){
       
//       var param = {user_id:'<?php echo $_REQUEST['user_id'];?>',
//       auth_token:'<?php echo $_REQUEST['auth_token'];?>',
//       level:'<?php echo $_REQUEST['level'];?>',
//       points:'<?php echo $_REQUEST['points'];?>',
//       tour_id:'<?php echo $_REQUEST['tour_id'];?>',
//       game_type:'<?php echo $_REQUEST['game_type'];?>',
//       payment_id:'<?php echo $_REQUEST['payment_id'];?>',
//       avatar_url:'<?php echo $_REQUEST['avatar_url'];?>',
//       lang:'<?php echo $_REQUEST['lang'];?>'
//       };
       var param = {user_id:'1',
       auth_token:'token_1',
       level:'0',
       points:'0',
       tour_id:'1',
       game_type:'VS_MODE',
       payment_id:'payment_1',
       avatar_url:'1',
       lang:'en'
       };
       var serverPath = "ws://18.185.99.32:4003";//"wss://play.games360platform.com:4003";
       var homeURL ="https://games360platform.com"
       app.fire("setParams",param,serverPath, homeURL);
       });
       
</script>
</body>
</html>
