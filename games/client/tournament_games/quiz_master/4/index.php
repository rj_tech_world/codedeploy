<!doctype html>
<html>
<head>
<meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no' />
<meta charset='utf-8'>
<link rel="stylesheet" type="text/css" href="styles.css">
<link rel="manifest" href="manifest.json">
<style></style>
<title>QuizMaster</title>
<script src="playcanvas-stable.min.js"></script>
<script>
var ver = 18;
ASSET_PREFIX = "";
SCRIPT_PREFIX = "";
SCENE_PATH = "665656.json?ver="+ver;
CONTEXT_OPTIONS = {
    'antialias': true,
    'alpha': false,
    'preserveDrawingBuffer': true,
    'preferWebGl2': true
};
SCRIPTS = [ 15437577, 15437579, 14943598, 15438502, 15437576, 14943872, 14880334, 14880336, 14880335, 14943599, 14943956, 14946164, 14946808, 14946849, 15007508, 15032645, 15489858, 15490972, 15491470, 15508226, 15512628 ];
CONFIG_FILENAME = "config.json?ver="+ver;
INPUT_SETTINGS = {
useKeyboard: true,
useMouse: true,
useGamepads: false,
useTouch: true
};
pc.script.legacy = false;
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133447238-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-133447238-1');
</script>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-4897574628510665",
    enable_page_level_ads: true
  });
</script>
</head>
<body>
<script src="__start__.js?ver="+ver></script>
<script src="__loading__.js?ver="+ver></script>
<script type="text/javascript">
var app = pc.Application.getApplication();
app.on('initialize',function(){
       
       var param = {user_id:'<?php echo $_REQUEST['user_id'];?>',
       auth_token:'<?php echo $_REQUEST['auth_token'];?>',
       level:'<?php echo $_REQUEST['level'];?>',
       points:'<?php echo $_REQUEST['points'];?>',
       tour_id:'<?php echo $_REQUEST['tour_id'];?>',
       game_type:'<?php echo $_REQUEST['game_type'];?>',
       points_required:'<?php echo $_REQUEST['points_required'];?>',
       payment_id:'<?php echo $_REQUEST['payment_id'];?>',
       avatar_url:'<?php echo $_REQUEST['avatar_url'];?>',
       lang:'<?php echo $_REQUEST['lang'];?>'
       };
       var serverPath = "ws://18.185.99.32:4001";//"wss://play.games360platform.com:4001";
       // var homeURL ="https://games360platform.com"
       app.fire("setParams",param,serverPath/*, homeURL*/);
       });
</script>
</body>
</html>

