import { Room , Client} from "colyseus"; 
import {  database,presence,totalServerClients,failedPaymentIds, LOG,LOG_ERROR} from '../server';
import {  dateTime } from '../server';
import * as promise from 'promise'; 
import  * as Request from "request";   
const importFresh = require('import-fresh'); 
require('dotenv').config();   
//LOG(database);                  
enum MSG_TYPE {                                                                                                                                                                                             
    PLAYER_JOINED= "PLAYER_JOINED",                   
        PLAYER_REJOINED= "PLAYER_REJOINED",                   
        PLAYER_LEFT="PLAYER_LEFT",           
        NO_OPPONENTS =  "NO_OPPONENTS",          
        READY_TO_START = "READY_TO_START",           
        GAME_NOT_AVAILABLE="GAME_NOT_AVAILABLE",   
        GAME_LOADED="GAME_LOADED",  
        START_GAME = "START_GAME",  
        INIT_PLAYER_TURN="INIT_PLAYER_TURN",  
        ANSWER_SUBMIT = "ANSWER_SUBMIT",            
        ANSWER_RESULT= "ANSWER_RESULT",  
        TURN_CHANGED = "TURN_CHANGED",  
        TURN_TIME_OUT = "TURN_TIME_OUT",             
        YOU_WIN = "YOU_WIN",     
        YOU_LOST = "YOU_LOST",           
        GAME_DRAW = "GAME_DRAW",   
        GAME_OVER="GAME_OVER",
        PING_PONG = "PING_PONG",    
        TRANSLATE_TEXT = "TRANSLATE_TEXT", 
        AUTH_FAILED = "AUTH_FAILED", 
        SEND_DATA_TO_SERVER = "SEND_DATA_TO_SERVER",  
        DATA_FROM_SERVER="DATA_FROM_SERVER", 
        GAME_END = "GAME_END"  ,
        UPDATE_ENTITY="UPDATE_ENTITY",
        MOUSE_DOWN="MOUSE_DOWN",
        MOUSE_MOVE="MOUSE_MOVE",
        MOUSE_UP="MOUSE_UP", 
        WHITE_BALL_FORCE="WHITE_BALL_FORCE",  
        SMALL_MSG="SMALL_MSG",
        FOUL_MSG="FOUL_MSG",
        MSG_BOX="MSG_BOX", 
        BALL_SET="BALL_SET", 
        BALL_POTTED="BALL_POTTED",
        FOUL="FOUL",      
            WHITE_BALL_DRAG="WHITE_BALL_DRAG",
            WHITE_BALL_DEND="WHITE_BALL_DEND",
  SET_READY_TO_PLAY="SET_READY_TO_PLAY",
}      
    enum APP_EVENTS {           
        PLAYER_JOINED= "PLAYER_JOINED",  
            PLAYER_REJOINED= "PLAYER_REJOINED",  
            PLAYER_LEFT="PLAYER_LEFT",         
            LOADING_GAME="LOADING_GAME", 
            ON_TANK_REACH="ON_TANK_REACH", 
            ON_EXIT_GAME="ON_EXIT_GAME",    
            GAME_NOT_AVAILABLE="GAME_NOT_AVAILABLE",    
            READY_TO_START = "READY_TO_START",   
            GAME_LOADED="GAME_LOADED",
            START_GAME = "START_GAME", 
            INIT_PLAYER_TURN="INIT_PLAYER_TURN",
            PING_PONG = "PING_PONG",  
            TURN_CHANGED = "TURN_CHANGED", 
            TURN_TIME_OUT = "TURN_TIME_OUT",    
            YOU_WIN = "YOU_WIN", 
            ON_PLAY_AGAIN_EVENT="ON_PLAY_AGAIN_EVENT",
            YOU_LOST = "YOU_LOST",   
            GAME_DRAW = "GAME_DRAW",    
            GAME_OVER="GAME_OVER",
            AUTH_FAILED = "AUTH_FAILED",       
            SEND_DATA_TO_SERVER = "SEND_DATA_TO_SERVER",      
            DATA_FROM_SERVER="DATA_FROM_SERVER",      
            ON_QUIT_BUTTON_CLICK_EVENT="ON_QUIT_BUTTON_CLICK_EVENT",
            ON_CONTINUE_BUTTON_CLICK_EVENT="ON_CONTINUE_BUTTON_CLICK_EVENT",
            ON_PLAY_AGAIN_BUTTON_CLICK_EVENT="ON_PLAY_AGAIN_BUTTON_CLICK_EVENT",
            ADD_AMOUNT_TO_USER_WALLET="ADD_AMOUNT_TO_USER_WALLET",
            SYNC_OBJECT_POSITION="SYNC_OBJECT_POSITION",
            PAY_AMOUNT_FROM_USER_WALLET="PAY_AMOUNT_FROM_USER_WALLET", 
            INSUFFICIENT_WALLET_AMOUNT="INSUFFICIENT_WALLET_AMOUNT", 
            TRANSACTION_FAILED="TRANSACTION_FAILED",
            UPDATE_PLAYER_POSITION="UPDATE_PLAYER_POSITION",     
            UPDATE_PLAYER_ROTATION="UPDATE_PLAYER_ROTATION" ,     
            UPDATE_ENTITY="UPDATE_ENTITY", 
            MOUSE_DOWN="MOUSE_DOWN",
            MOUSE_MOVE="MOUSE_MOVE",
            MOUSE_UP="MOUSE_UP",
            WHITE_BALL_FORCE="WHITE_BALL_FORCE",  
            SMALL_MSG="SMALL_MSG",
            FOUL_MSG="FOUL_MSG",  
            MSG_BOX="MSG_BOX",
            BALL_SET="BALL_SET",
            BALL_POTTED="BALL_POTTED",
            FOUL="FOUL",      
            WHITE_BALL_DRAG="WHITE_BALL_DRAG",
            WHITE_BALL_DEND="WHITE_BALL_DEND",
            SET_READY_TO_PLAY="SET_READY_TO_PLAY",
    }     

        enum GAME_STATE{   
            NONE = "NONE", 
                WAIT_FOR_OPPONENT= "WAIT_FOR_OPPONENT",
                READY_TO_START = "READY_TO_START",
                IN_GAME="IN_GAME",
                GAME_END =  "GAME_END",
                GAME_CLOSE = "GAME_CLOSE",

        } 
            enum GAME_TYPE{     
                NONE = "NONE", 
                    TOUR_MODE= "TOUR_MODE", 
                    VS_MODE="VS_MODE",  
                    FTP_MODE = "FTP_MODE",
            }
                enum LANGUAGE_SUPPORTED{  
                    ENGLISH = "en", 
                        HINDI = "hi",
                        NOT_SUPPORTED = 'NOT_SUPPORTED'
                }
                enum CONNECT_STATUS {    
                    ON_INIT= "ON_INIT",
                        ON_AUTH= "ON_AUTH",
                        ON_REQUEST_JOIN="ON_REQUEST_JOIN", 
                        ON_JOIN =  "ON_JOIN",
                        ON_LEAVE = "ON_LEAVE",
                        REFUND = "REFUND",
                        RECONNECTED = "RECONNECTED"
                }   

                enum TOKEN_COLOR {
                    RED = "RED",  
                        GREEN = "GREEN",
                          YELLOW = "YELLOW",
                        BLUE = "BLUE",
                  }     
                    const SIMULATION_TIME = 30;// interval time to update the game engine
                    const OPPONENT_WAIT_TIME = 30;//in sec   //value+5 will get display on search opponent screen 
                    const ROOM_LOCK_TIME = 5;//in sec    
                    const TURN_TIME_OUT = 60;//in sec        
                    const TURN_TIME_OUT_BUFFER = 1;//in sec ...if change mak sure to update on answer submit condition
                    const CLIENT_REJOIN_WAIT_TIME = 60;//in sec
                    const ROLL_DICE_WAIT_TIME = 1.0;
                     const CHANGE_TURN_WAIT_TIME = 1.0 ;//in sec // delay after turn change
                     const  QUE_IMAGE_PATH =  process.env.QUE_IMAGE_PATH;
                    const READY_TO_START_TIME = 5;
                    const PRESENCE_KEY_HOLD_SEC = 10;  
                     const PING_PONG_TIME = 2; //in sec 

                     // var totalServerClients =[]; 
                    //  var isPresenceSubscribe = false;  
                    var isPlayCanvas = true;
                    var isSkipDataEntry = true;
                    var gameId = 1; 
                    var gameQueue = [];
                    var isLoadingGame = false; 
                    var GameEngine = importFresh("../gameEngine")();
                    export class GameRoom extends Room  {   

                        gameInstance = null;
                    currentGameState =  GAME_STATE.NONE; 
                    game_type = 'undefined';
                    player_available = 0;
                    turnTimeIntervalRef = null;
                    opponentWaitTimeOutRef = null;  
                    loadingWaitTimeOutRef = null;
                    changeTurnTimeOutRef = null; 
                    minClients = 2; 
                    maxPlayers = 2;
                    playersCount =0;
                    totalClients = 0; //all clients valid and invalid 
                    stepCount =1;
                    game_id = "";
                    tour_id = "";  
                    win_points = 0;  
                    level_lives = 0;
                    score = 0;
                    rank = 0; 
                    dice_face_id = 0;
                    isbonusTurn  = false; 
                    isPieceMoved = false; 
                    sixCount = 0; 
                    points_required = 0; 
                    room_level = null; 
                    isDBRoomReadyToStartGame = false;
                    is_free = false;
                    is_fixed_price = false;
                    amount =0;
                    pot_amount = 0;
                    winning_id = 0;
                    win_lives = 0;  
                    gameRewardsData =   {
                        "id": 1,
                        "type": "",
                        "tour_id": 0,
                        "entry_fee": 0,
                        "entry_type": "0",
                        "no_of_players": 0,
                        "reward_dist": "",
                        "fixed_pot": "0",
                        "fixed_pot_value": 0
                    };
                    gameWinRewards =    {

                        "reward_dist": ""

                    };
                    gameLostRewards =   {
                        "reward_dist": ""
                    };
                    options_Data = {
                    };

                    // this room supports only 2 clients connected
                    players_Data = {
                    };  

                    game_data = {
                        0 : { current_pos_index : 0,in_house : false, req_to_enter_house : 6 , total_steps : 0} ,
                        1 : { current_pos_index : 0,in_house : false, req_to_enter_house : 6 , total_steps : 0} ,
                        2 : { current_pos_index : 0,in_house : false, req_to_enter_house : 6 , total_steps : 0} ,
                        3 : { current_pos_index : 0,in_house : false, req_to_enter_house : 6 , total_steps : 0} ,
                    };
                    playersQueue = [];   
                    roomLog = [];
                    ludoBoardBlocksArray = [];

                    // masterQueIndex = 0; 
                    // masterRefillCount = 0;   
                    // master_que_buffer = {"en":[],"hi":[]};    
                    // que_buffer = {"en":[],"hi":[]}; 
                    // currentQueData =  {
                    //               "en":{"question": "question","opt_1": "option 1", "opt_2": "option 2", "opt_3": "option 3","opt_4": "option 4"},
                    //               "hi":{"question": "question","opt_1": "option 1", "opt_2": "option 2", "opt_3": "option 3","opt_4": "option 4"}
                    //           };   
                    room_data = {  
                        currentGameState:GAME_STATE.NONE,
                        currentTurnId: null,
                        current_selected_piece :null, 
                        players: {}, 
                        // que_data:{ "type": 0,"image_path":"","question": "question","opt_1": "option 1", "opt_2": "option 2", "opt_3": "option 3","opt_4": "option 4"},
                        tt: TURN_TIME_OUT,//turn Timer
                        tmt: TURN_TIME_OUT,//turn max timer
                        cbc:{p:{x:0,y:0,z:0},r:{ x:0,y:0,z:0,w:1}}, 
                        cs:{p:{x:0,y:0,z:0},r:{ x:0,y:0,z:0,w:1}}, 
                        bw:{p:{x:0,y:0,z:0},r:{ x:0,y:0,z:0,w:1}}, 
                        b1:{p:{x:0,y:0,z:0},r:{ x:0,y:0,z:0,w:1}}, 
                        b2:{p:{x:0,y:0,z:0},r:{ x:0,y:0,z:0,w:1}}, 
                        b3:{p:{x:0,y:0,z:0},r:{ x:0,y:0,z:0,w:1}}, 
                        b4:{p:{x:0,y:0,z:0},r:{ x:0,y:0,z:0,w:1}}, 
                        b5:{p:{x:0,y:0,z:0},r:{ x:0,y:0,z:0,w:1}},  
                        b6:{p:{x:0,y:0,z:0},r:{ x:0,y:0,z:0,w:1}},  
                        b7:{p:{x:0,y:0,z:0},r:{ x:0,y:0,z:0,w:1}}, 
                        b8:{p:{x:0,y:0,z:0},r:{ x:0,y:0,z:0,w:1}}, 
                        b9:{p:{x:0,y:0,z:0},r:{ x:0,y:0,z:0,w:1}}, 
                        b10:{p:{x:0,y:0,z:0},r:{ x:0,y:0,z:0,w:1}}, 
                        b11:{p:{x:0,y:0,z:0},r:{ x:0,y:0,z:0,w:1}}, 
                        b12:{p:{x:0,y:0,z:0},r:{ x:0,y:0,z:0,w:1}}, 
                        b13:{p:{x:0,y:0,z:0},r:{ x:0,y:0,z:0,w:1}}, 
                        b14:{p:{x:0,y:0,z:0},r:{ x:0,y:0,z:0,w:1}}, 
                        b15:{p:{x:0,y:0,z:0},r:{ x:0,y:0,z:0,w:1}},
                        wbf:{x:0,y:0,z:0},
                        mdp:{x:0,y:4.2,z:0},
                        ist:0,//isShotTaken 
                        ft:0,//foul type 
                        bp:0,//ball potted id
                        bpl:[],//balls potted list
                        bt:-1,//ball type ( 0:solid or 1:stripes)
                        // pot_amount:100,
                        winner_list: {},// common  list for single win player and draw game users 
                        replay_amount:10,
                        points_required:0,
                      
                    };   
                    isBotAvailable = true;
                    isBotActive = false;   
                    botWaitTimeRef = null; 
                    que_level = 1; 
                    prob = [
                        80,//0
                        60,//1
                        40,//2
                        40,//3
                        30,//4
                        30,//5
                        20,//6
                        20,//7
                        20,//8
                    ];
                    isEven = -1;
                    isRoomLock = false;
                    setEvenMode(options){
                      
                      if(!isSkipDataEntry){
                        
                      
                        database.getBotConfig(options).then((result)=>{ 

                            LOG("get........... Config SUCCESS :: "+JSON.stringify(result) );  
                            var _result = JSON.parse(JSON.stringify(result))[0];
                            var val = this.randomInt(1,100);   
                            console.log("value === "+val+"_result.bot_available = "+_result.bot_available);
                            if(isPlayCanvas){
                                this.isBotAvailable  = true;
                            }else{
                                this.isBotAvailable  =  val <=_result.bot_available ?true:false;
                            }


                            this.prob[0] = _result.que_level_0;
                            this.prob[1] = _result.que_level_1;
                            this.prob[2] = _result.que_level_2;
                            this.prob[3] = _result.que_level_3;
                            this.prob[4] = _result.que_level_4;
                            this.prob[5] = _result.que_level_5;


                        },(error)=>{
                            LOG("get...........Config =>Error "+error);
                        });    


                       }


                    }  

                    onInit (options) { 

                        try{  


                            console.log(">>>>>>>>>>>>>>>>>roomId = "+this.roomId);

                            Object.assign(options,{"connect_status":CONNECT_STATUS.ON_INIT});

                            this.setSeatReservationTime(5000);  


                            LOG("STEP "+(this.stepCount++)+": onInit BasicRoom created!", options);


         
                            this.isDBRoomReadyToStartGame = false;


                            this.autoDispose= true; 

                            if(typeof(options.user_id) == 'undefined'
                               //|| typeof(options.auth_token) == 'undefined'
                               || typeof(options.level) == 'undefined' 
                               || typeof(options.points) == 'undefined'
                               || typeof(options.tour_id) == 'undefined'
                               || typeof(options.payment_id) == 'undefined'
                               || typeof(options.game_type) == 'undefined'
                               || typeof(options.avatar_url) == 'undefined'   
                               || typeof(options.lang) == 'undefined' 
                               || (options.user_id+"") == "" 
                               // || (options.auth_token+"") == ""  
                               || (options.level+"") == "" 
                               || (options.points+"") == ""  
                               || (options.tour_id+"") == "" 
                               || (options.payment_id+"") == ""  
                               || (options.game_type+"") == "" 
                               || (options.avatar_url+"") == ""     
                               || (options.lang+"") == ""   
                              ){
                                //this.setSeatReservationTime(2);
                                this.lock();
                                LOG("$$$$$$$$$$$$$$$$$$$$$$ Room lock on init as game_type is undefined");
                                return;
                            }

                            this.room_level = options.level;  
                            this.game_type = options.game_type; 
                            this.tour_id = options.tour_id;

                            this.setState({
                                totalPlayers: this.clients.length,   
                                pot_amount:0
                            });

                            this.maxPlayers = 4;
                            database.getGameConfig(options).then((result)=>{ 

                                this.is_free = JSON.parse(JSON.stringify(result))[0].is_free;
                                this.is_fixed_price = JSON.parse(JSON.stringify(result))[0].is_fixed_price;
                                this.amount =   JSON.parse(JSON.stringify(result))[0].amount;

                                if(this.is_fixed_price){
                                    this.pot_amount = this.amount;
                                }else {
                                    this.pot_amount = this.amount*this.clients.length;
                                }

                                this.state = {"totalPlayers":Object.keys(this.players_Data).length,"pot_amount":this.pot_amount};
                                LOG("getGameConfig =>Success "+JSON.stringify(result) );  
                            },(error)=>{
                                LOG.log("getGameConfig =>Error "+error);
                            });



                            switch(this.game_type){
                                case GAME_TYPE.FTP_MODE:
                                    this.maxPlayers = 2;
                                    //                   database.getGameConfig(options).then((result)=>{ 

                                    //                   this.is_free = JSON.parse(JSON.stringify(result))[0].is_free;
                                    //                   this.is_fixed_price = JSON.parse(JSON.stringify(result))[0].is_fixed_price;
                                    //                   this.amount =   JSON.parse(JSON.stringify(result))[0].amount;

                                    //                   if(this.is_fixed_price){
                                    //                       this.pot_amount = this.amount;
                                    //                   }else {
                                    //                       this.pot_amount = this.amount*this.clients.length;
                                    //                    }

                                    //                   this.state = {"totalPlayers":Object.keys(this.players_Data).length,"pot_amount":this.pot_amount};
                                    //                   LOG("getGameConfig =>Success "+JSON.stringify(result) );  
                                    //               },(error)=>{
                                    //                   LOG.log("getGameConfig =>Error "+error);
                                    //               });


                                    break;
                                case GAME_TYPE.TOUR_MODE:
                                    //  this.maxClients = 2;
                                    this.maxPlayers = 2;

                                    //                     database.getTourGameConfig(options).then((result)=>{ 

                                    //                     this.is_free = false;
                                    //                     this.is_fixed_price = true;
                                    //                     this.amount =   JSON.parse(JSON.stringify(result))[0].amount;
                                    //                     this.winning_id =  JSON.parse(JSON.stringify(result))[0].winning_id;
                                    //                     this.win_lives =   this.winning_id == 1 ? 1 : 0;//winning_id = 1 means give lives is set in database
                                    //                     if(this.is_fixed_price){
                                    //                         this.pot_amount = this.amount;
                                    //                     }else {
                                    //                         this.pot_amount = this.amount*this.clients.length;
                                    //                     } 
                                    //                    this.state = {"totalPlayers":Object.keys(this.players_Data).length,"pot_amount":this.pot_amount};

                                    //                     LOG("getTourGameConfig =>Success "+JSON.stringify(result) );  
                                    //                 },(error)=>{
                                    //                     LOG("getTourGameConfig =>Error "+error);
                                    //                 });
                                    break;
                            }

                            LOG("this.game_type = "+this.game_type+"  this.maxClients = "+this.maxPlayers+"");

                            database.getGameRewardsData(options).then((result)=>{ 

                                LOG("getGameRewardsData =>Success "+JSON.stringify(result) );  
                                var _result = JSON.parse(JSON.stringify(result))[0];

                                Object.assign(this.gameRewardsData,_result);
                                Object.assign(this.gameRewardsData,{reward_dist:JSON.parse(_result.reward_dist)});


                            },(error)=>{
                                LOG("getGameRewardsData =>Error "+error);
                            });

                            //          database.getGameWinRewards(options).then((result)=>{ 

                            //                     LOG("getWinRewards =>Success "+JSON.stringify(result) );  
                            //                   var _result = JSON.parse(JSON.stringify(result))[0];

                            //                  Object.assign(this.gameWinRewards,{"bid_coins":_result.bid_coins,"play_cash":_result.play_cash,"cash":_result.cash,"ticket":_result.ticket});

                            //                 },(error)=>{
                            //                     LOG("getWinRewards =>Error "+error);
                            //                 });
                            //           database.getGameLostRewards(options).then((result)=>{ 

                            //                         LOG("getWinRewards =>Success "+JSON.stringify(result) );  
                            //                       var _result = JSON.parse(JSON.stringify(result))[0];

                            //                      Object.assign(this.gameLostRewards,{"bid_coins":_result.bid_coins,"play_cash":_result.play_cash,"cash":_result.cash,"ticket":_result.ticket});

                            //                     },(error)=>{
                            //                         LOG("getWinRewards =>Error "+error);
                            //                     });    


 
                            this.setEvenMode(options);

                            //this.setSimulationInterval((deltaTime )=> this.update(deltaTime));
                            this.clock.setInterval(this.update.bind(this),SIMULATION_TIME);
                        }catch(e){ 
                            LOG_ERROR("ERROR :: "+e.stack);
                        }

                    }

                    update (deltaTime) {
                        if(this.gameInstance){
                            this.gameInstance.updateGameLoop();
                        }
                        //  console.log("------->"+deltaTime);
                        // implement your physics or world updates here!
                        // this is a good place to update the room state
                    }


                    requestJoin (options,isNewRoom) {    
                        try{

                            if(typeof(options.user_id) == 'undefined'
                               //|| typeof(options.auth_token) == 'undefined'
                               || typeof(options.level) == 'undefined' 
                               || typeof(options.points) == 'undefined'
                               || typeof(options.tour_id) == 'undefined'
                               || typeof(options.payment_id) == 'undefined'
                               || typeof(options.game_type) == 'undefined'
                               || typeof(options.avatar_url) == 'undefined'   
                               || typeof(options.lang) == 'undefined' 
                               || (options.user_id+"") == "" 
                               //|| (options.auth_token+"") == ""  
                               || (options.level+"") == "" 
                               || (options.points+"") == ""  
                               || (options.tour_id+"") == "" 
                               || (options.payment_id+"") == ""  
                               || (options.game_type+"") == "" 
                               || (options.avatar_url+"") == ""     
                               || (options.lang+"") == ""   
                              ){

                                LOG("****************requestJoin Failed due missing user details****************"); 
                                return false;
                            } 


                            LOG("STEP "+(this.stepCount++)+": requestJoin ", options); 
                            Object.assign(options,{"connect_status":CONNECT_STATUS.ON_REQUEST_JOIN});



                            if(this.currentGameState == GAME_STATE.GAME_END || this.currentGameState == GAME_STATE.GAME_CLOSE ){
                                return false;
                            }  
                            //         if(failedPaymentIds.indexOf(""+options.payment_id) >=0){    
                            //         LOG("Request failed for Auth failed payment id");
                            //            Object.assign(options,{"connect_status":"AUTH_FAILED"});

                            //           return false;
                            //         }


                            LOG(this.currentGameState + "******************************************** requestJoin isNewRoom = ",isNewRoom);
                            //  if(options.roomId==this.roomId){
                            // return true;
                            // }
                            LOG("totalServerClients length in cpu ====> "+((totalServerClients).length));
                            LOG(totalServerClients[totalServerClients.indexOf(""+options.payment_id)]+"payment id already exist in this cpu")
                            var isChecked = false;
                            if(!isNewRoom){
                                if(totalServerClients.indexOf(""+options.payment_id) >=0){    
                                    // if(typeof totalServerClients[""+options.payment_id]!='undefined' && totalServerClients[""+options.payment_id] != null){  
                                    LOG(options.payment_id+" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> requestjoin isClientInRoom ");
                                    isChecked = true;
                                    for(var id =0 ;id<Object.keys(this.options_Data).length;id++){ 
                                        LOG("request cheking for "+this.options_Data[id].options.payment_id);

                                        if(typeof this.options_Data[id] != 'undefined' && typeof this.options_Data[id].options != 'undefined'){
                                            if( this.options_Data[id].options.user_id == options.user_id && this.options_Data[id].options.payment_id != options.payment_id){  

                                                LOG("@@@@@@@@@@@@@@@@@@@@@@@@request failed for room "+this.options_Data[id].options.payment_id);
                                                isChecked = true;
                                                return false;
                                            }
                                            if( this.options_Data[id].options.user_id == options.user_id && this.options_Data[id].options.payment_id == options.payment_id){  

                                                LOG("@@@@@@@@@@@@@@@@@@@@@@@@ allowed user for second request "+this.options_Data[id].options.payment_id);
                                                isChecked = true;
                                                if(typeof this.options_Data[id].options.isRefund  != 'undefined' && this.options_Data[id].options.isRefund == true){
                                                    LOG("1 Request join failed due to connect_status as  REFUND"); 
                                                    this.playersCount--;
                                                    return false;
                                                }else{
                                                    return true
                                                }
                                            }
                                        }
                                    }
                                }  

                            }
                            if(typeof(options.user_id) != 'undefined' && typeof(options.payment_id) != 'undefined'){ 
                                for(var id =0 ;id<Object.keys(this.options_Data).length;id++){

                                    if(typeof(this.options_Data[id].options) == 'undefined'){
                                        continue;
                                    }

                                    if(this.options_Data[id].options.user_id == options.user_id && this.options_Data[id].options.payment_id != options.payment_id){  
                                        return false;
                                    }
                                    if( this.options_Data[id].options.user_id == options.user_id && this.options_Data[id].options.payment_id == options.payment_id ){  
                                        LOG(" requestJoin 111111111111  Alredy in game room "+options.sessionId);
                                        if(typeof this.options_Data[id].options.isRefund  != 'undefined' && this.options_Data[id].options.isRefund == true){
                                            LOG("2 Request join failed due to connect_status as  REFUND"); 
                                            return false;
                                        }
                                        switch(this.game_type){

                                            case GAME_TYPE.TOUR_MODE: 
                                                return (this.room_level == options.level && this.game_type == options.game_type && this.tour_id == options.tour_id ?true:false);
                                                break;
                                            case GAME_TYPE.VS_MODE:
                                                LOG(" requestJoin 111111111111  Alredy in game room accept = "+(this.game_type == options.game_type && this.tour_id == options.tour_id));
                                                return (this.game_type == options.game_type && this.tour_id == options.tour_id?true:false);

                                                break;  

                                            default:
                                                return (this.room_level == options.level && this.game_type == options.game_type && this.tour_id == options.tour_id ?true:false);
                                                break;
                                        } 
                                    }
                                } 
                            }


                            if(!isNewRoom){

                                for(var id =0 ;id<Object.keys(this.options_Data).length;id++){ 

                                    if(typeof(this.options_Data[id].options) == 'undefined'){
                                        continue;
                                    }
                                    if(this.options_Data[id].options.sessionId == options.sessionId ){ 

                                        //if(this.options_Data[id].options.roomId == options.roomId && this.options_Data[id].options.sessionId == options.sessionId ){ 
                                        LOG(" requestJoin 22222222222222222222 Alredy in game room "+options.sessionId); 
                                        return true;
                                    }
                                } 

                            }
                            //if(this.clients.length>= this.maxPlayers  
                            if(this.player_available>= this.maxPlayers  
                               // if(this.playersCount>= this.maxPlayers 
                               || this.currentGameState == GAME_STATE.READY_TO_START 
                               || this.currentGameState == GAME_STATE.IN_GAME 
                               || this.isRoomLock
                              ){// if Game Room is full or game is started
                                return false;
                            } 


                            if(!isChecked){
                                if(this.game_type == options.game_type && this.tour_id == options.tour_id ){ 
                                    Object.assign(options,{"score":0});
                                    Object.assign(options,{"isRequestAccepted":1,roomId:this.roomId});

                                    if(typeof this.presence != 'undefined' ){


                                        var presensePromise = this.presence.get(""+options.payment_id);
                                        if(typeof presensePromise != 'undefined'){
                                            presensePromise.then((result)=>{ 
                                                LOG("----------------------->after set "+result);
                                                if(typeof result == 'undefined' || (result+"") == "" || result == null || isNewRoom){


                                                    this.options_Data[this.playersCount] = {"options":options};

                                                    LOG("this.playersCount = "+this.playersCount+" user = "+options.user_id+" with payment_id = "+options.payment_id+" enter in room ");

                                                    this.playersCount++; 
                                                    this.presence.publish("ADD_PAYMENT_ID", ""+options.payment_id);

                                                    this.presence.setex(""+options.payment_id,""+options.payment_id,PRESENCE_KEY_HOLD_SEC);




                                                }else{
                                                    //already request accepted in another room
                                                    Object.assign(options,{"isRequestAccepted":2}); 


                                                } 

                                            });
                                            this.presence.setex(""+options.payment_id,""+options.payment_id,PRESENCE_KEY_HOLD_SEC);


                                        }
                                    }
                                    return true;

                                }else{
                                    return false;
                                }

                            }
                            //return this.clients.filter(c => c.id === options.clientId).length === 0;
                            console.log("Request finally failed for user_id = "+options.user_id+"  payment_id = "+options.payment_id);
                            return true;
                            //return false;
                        }catch(e){
                            LOG_ERROR("ERROR :: "+e.stack);
                        }

                    } 

                    onAuth (options) {

                        try{

                            for(var id =0 ,length = Object.keys(this.options_Data).length;id<length;id++){


                                if(  this.options_Data[id].options.payment_id == options.payment_id  ){ 
                                    Object.assign(options,{"user_id":this.options_Data[id].options.user_id});
                                }
                            }

                            LOG("STEP "+(this.stepCount++)+": onAuth "+JSON.stringify(options));

                            var isNewUser = true;

                            Object.assign(options,{"connect_status":CONNECT_STATUS.ON_AUTH});

                            // Validation for authentication
                            if(typeof(options.user_id) == 'undefined'
                               //|| typeof(options.auth_token) == 'undefined'
                               || typeof(options.level) == 'undefined' 
                               || typeof(options.points) == 'undefined'
                               || typeof(options.tour_id) == 'undefined'
                               || typeof(options.payment_id) == 'undefined'
                               || typeof(options.game_type) == 'undefined'
                               || typeof(options.avatar_url) == 'undefined'   
                               || typeof(options.lang) == 'undefined' 
                               || (options.user_id+"") == "" 
                               //|| (options.auth_token+"") == ""  
                               || (options.level+"") == "" 
                               || (options.points+"") == ""  
                               || (options.tour_id+"") == "" 
                               || (options.payment_id+"") == ""  
                               || (options.game_type+"") == "" 
                               || (options.avatar_url+"") == ""     
                               || (options.lang+"") == ""   
                              ){

                                Object.assign(options,{comment:"onAuthFailed due to undefined paramaters or sessionId mismatch"});
                                this.onAuthFailed(options);
                                return true;  
                            }  
                            // if(failedPaymentIds.indexOf(""+options.payment_id) >=0){
                            //     Object.assign(options,{comment:"onAuthFailed due to previous Failed payment id"});
                            //     this.onAuthFailed(options);
                            //      return true; 
                            //    }

                            for(var id =0 ,length = Object.keys(this.options_Data).length;id<length;id++){

                                if(this.options_Data[id].options.user_id == options.user_id &&
                                   this.options_Data[id].options.payment_id == options.payment_id 
                                  ){ 

                                    isNewUser = false;

                                    if( this.options_Data[id].options.isAuthSuccess == 2){ 

                                        Object.assign(options,{comment:"onAuthFailed due to second request for previous failed attempt"});
                                        this.onAuthFailed(options);
                                        return true;  
                                    } 

                                    //if( this.options_Data[id].options.roomId == options.roomId && this.options_Data[id].options.isAuthSuccess == 1){ 
                                    if(  this.options_Data[id].options.isAuthSuccess == 1){ 

                                        Object.assign(options,{comment:"onAuthSuccess due to second request for previous success attempt"});
                                        this.onAuthSuccess(options);
                                        return true;  
                                    } 
                                }
                            }

                            Object.assign(options,{"is_bot":0});
                            // if(isNewUser){
                            //      this.totalClients++;
                            // }

                            if(isPlayCanvas){
                                Object.assign(options,{"mob_no": "0123456789"});//cheat
                                this.onAuthSuccess(options)//cheat            
                                return true;//cheat 
                            }

                            //first validate user token and then validate level and points
                            return new promise((resolve,reject)=>{ 
                                /*
          database.validateToken(options).then((result)=>{ 

            if(Object.keys(result).length == 0 || typeof(JSON.parse(JSON.stringify(result))[0].id) == 'undefined'){
                LOG("----------------------------------------------Authentication Failed => "+JSON.stringify(options));
               Object.assign(options,{comment:"onAuthFailed due to Invalid token"});
                this.onAuthFailed(options);
                resolve(options);
                 return true;  
            }else{  


                Object.assign(options,{"mob_no": JSON.parse(JSON.stringify(result))[0].mob_no});
*/
                                var promiseTourLives =   database.getUserTourLives(options);
                                return  promiseTourLives.then((result)=>{ 

                                    LOG("getUserTourLives =>Success "+JSON.stringify(result) ); 
                                    if(JSON.parse(JSON.stringify(result)).length>=1 && JSON.parse(JSON.stringify(result))[0].tour_lives >= 1){

                                        var data = JSON.parse(JSON.stringify(result))[0];

                                        Object.assign(options,{"tour_lives": data.tour_lives});

                                        return database.getUserLevelAndPoints(options).then((result)=>{

                                            var data = JSON.parse(JSON.stringify(result))[0];

                                            if(options.level == data.level && options.points == data.points ){      
                                                LOG("----------------------------------------------Authentication Success => "+JSON.stringify(options));
                                                Object.assign(options,{"total_points": data.total_points});
                                                if(isNewUser){
                                                    this.totalClients++;
                                                }
                                                this.onAuthSuccess(options);
                                                resolve(options);
                                                return true;
                                            }else{

                                                LOG("----------------------------------------------Authentication Failed => "+JSON.stringify(options));
                                                Object.assign(options,{comment:"onAuthFailed due to Invalid level or points"});
                                                this.onAuthFailed(options);
                                                resolve(options);
                                                return true;  
                                            }

                                        },(error)=>{  
                                            Object.assign(options,{comment:"onAuthFailed due to getUserLevelAndPoints Error"}); 
                                            this.onAuthFailed(options);resolve(options); 
                                            return true;  
                                        });


                                    }else{

                                        LOG("----------------------------------------------Authentication Failed => "+JSON.stringify(options));
                                        Object.assign(options,{comment:"onAuthFailed due to Tour Lives"}); 
                                        this.onAuthFailed(options);
                                        resolve(options);
                                        return true;  
                                    }

                                },(error)=>{
                                    LOG("getUserTourLives =>Error "+error);
                                    LOG("----------------------------------------------Authentication Failed => "+JSON.stringify(options));
                                    Object.assign(options,{comment:"onAuthFailed due to Tour Lives getUserTourLives =>Error "}); 
                                    this.onAuthFailed(options);
                                    resolve(options);
                                    return true;  
                                });

                                /*    }

                   },(error)=>{ 
                        LOG("----------------------------------------------Authentication Failed => "+JSON.stringify(options));
                       Object.assign(options,{comment:"onAuthFailed due to validateToken Error"}); 
                        this.onAuthFailed(options);
                        resolve(options);
                        return true;   

                         });*/
                            });
                        }catch(e){
                            LOG_ERROR("ERROR :: "+e.stack);
                        }

                    }   
                    onAuthFailed(options){  
                        try{
                            Object.assign(options,{"isAuthSuccess": 0});
                            LOG("--------------------------------------onAuthFailed--------Authentication Failed => "+JSON.stringify(options));

                            this.presence.publish("ADD_FAILED_PAYMENT_ID", ""+options.payment_id);

                            for(var id =0 ;id<Object.keys(this.options_Data).length;id++){ 

                                LOG("onAuthFailed user_id = "+this.options_Data[id].options.user_id+" payment_id = "+this.options_Data[id].options.payment_id+" vs  user_id = "+options.user_id+" vs payment_id = "+options.payment_id);

                                if( this.options_Data[id].options.user_id == options.user_id &&
                                   this.options_Data[id].options.payment_id == options.payment_id 
                                  ){ 
                                    Object.assign(this.options_Data[id].options,options);
                                    Object.assign(this.options_Data[id].options,{"isAuthSuccess": 2});
                                    //this.playersCount--;  
                                    this.presence.publish("DEL_PAYMENT_ID",""+options.payment_id);            

                                }
                            }  


                        }catch(e){
                            LOG_ERROR("ERROR :: "+e.stack); 
                        }
                    }

                    onAuthSuccess(options){
                        try{
                            LOG("--------------------------onAuthSuccess--------------------Authentication Success => "+JSON.stringify(options));

                            for(var id =0 ,length = Object.keys(this.options_Data).length;id<length;id++){ 
                                if(this.options_Data[id].options.user_id == options.user_id &&
                                   this.options_Data[id].options.payment_id == options.payment_id 
                                  ){ 
                                    Object.assign(this.options_Data[id].options,options);
                                    Object.assign(this.options_Data[id].options,{"isAuthSuccess": 1});

                                    LOG("--------------------------onAuthSuccess--------------------Authentication Success => "+JSON.stringify(this.options_Data[id].options));

                                }
                            }  


                        }catch(e){
                            LOG_ERROR("ERROR :: "+e.stack);
                        }
                    }

                    roomLockTimeOut(){
                        this.isRoomLock = true;
                        LOG("@@@@@@@@@@@@@@@@@@@@@@ RoomLock for new users");
                    }
                    noOfBots = 2;
                    botWaitTime(options){
                        try{
                            console.log(this.playersCount+" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> bot time out");
                            if(this.currentGameState == GAME_STATE.IN_GAME || this.currentGameState == GAME_STATE.GAME_END || this.currentGameState == GAME_STATE.GAME_CLOSE  ){
                                return false;
                            } 
                            if(this.playersCount == 1){ 
                                //if (this.player_available == 1) {
                                this.isBotActive = true;   

                                var bot =  {} as Client; 
                                var botPoints = this.randomInt(0,this.points_required-1);
                                if(botPoints <= 0){
                                    botPoints = 0;
                                }
                                this.options_Data[this.playersCount] = {"options":{is_bot:1,isAuthSuccess:1,clientId:"botClientId_"+this.noOfBots,sessionId:"botSessionId_"+this.noOfBots,user_id:'-'+this.noOfBots,level:this.room_level,auth_token:"user_token_2"+this.noOfBots,game_type:""+this.game_type,payment_id:"bot_payment_id_"+this.noOfBots,lang:'en',total_points:0,mob_no:(this.randomInt(7,9)+""+this.randomInt(7,9)+"345678"+this.randomInt(0,9)+""+this.randomInt(0,9)),tour_lives:options.tour_lives,points:botPoints,tour_id:options.tour_id,avatar_url:""+this.randomInt(1,4)}};
                                LOG("this.playersCount = "+this.playersCount+"user = "+this.options_Data[this.playersCount].options.user_id+" with payment_id = "+this.options_Data[this.playersCount].options.payment_id+" enter in room ");

                                this.playersCount++; 
                                this.totalClients++;
                                Object.assign(bot,{id:"botClientId_"+this.noOfBots,sessionId:"botSessionId_"+this.noOfBots}); 
                                this.onJoin(bot,this.options_Data[this.playersCount-1].options); 

                                LOG(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> bot joined to game");
                            }
                            //         if(this.noOfBots>1){

                            //           this.noOfBots--;
                            //           this.botWaitTime(options); 
                            //         }

                        }catch(e){
                            LOG_ERROR("ERROR :: "+e.stack);
                        }
                    }

                    oponentWaitTimeOut(){
                        try{
                            console.log("oponentWaitTimeOut ------------------> player_available = ",this.player_available);
                            console.log("oponentWaitTimeOut ------------------> playersCount = ",this.playersCount);     
                            if (this.player_available >= this.minClients ) 
                            { 
                                if(this.currentGameState == GAME_STATE.WAIT_FOR_OPPONENT)
                                {
                                    this.currentGameState = GAME_STATE.READY_TO_START;
                                    this.clock.setTimeout( this.readyToStartGame.bind(this),ROOM_LOCK_TIME*1000);
                                }
                            }else{
                                this.currentGameState = GAME_STATE.GAME_CLOSE;
                                this.sendMessage(MSG_TYPE.NO_OPPONENTS);           
                                if(this.opponentWaitTimeOutRef){
                                    this.opponentWaitTimeOutRef.clear();
                                }
                                for(var id =0 ;id<Object.keys(this.players_Data).length;id++){  
                                    if( this.game_type == GAME_TYPE.VS_MODE && this.players_Data[id].options.is_bot == 0){
                                        this.players_Data[id].client.close(); 
                                       if(!isSkipDataEntry){
                                        var promiseRefund =  database.refundAmountToUser(this.players_Data[id].options);
                                       }
                                        this.presence.publish("DEL_PAYMENT_ID",""+this.players_Data[id].options.payment_id);
                                        //this.presence.del(""+this.players_Data[id].options.payment_id);
                                    }

                                } 
                                this.onGameEnd();
                                //  this.closeRoom();
                            }   
                        }catch(e){
                            LOG_ERROR("ERROR :: "+e.stack);
                        }
                    }

                    onJoin(client,options){

                        if(typeof client == 'undefined'){
                            return;
                        }
                        if(typeof(options.user_id) == 'undefined'
                           // || typeof(options.auth_token) == 'undefined'
                           || typeof(options.level) == 'undefined' 
                           || typeof(options.points) == 'undefined'
                           || typeof(options.tour_id) == 'undefined'
                           || typeof(options.payment_id) == 'undefined'
                           || typeof(options.game_type) == 'undefined'
                           || typeof(options.avatar_url) == 'undefined'   
                           || typeof(options.lang) == 'undefined' 
                           || (options.user_id+"") == "" 
                           // || (options.auth_token+"") == ""  
                           || (options.level+"") == "" 
                           || (options.points+"") == ""  
                           || (options.tour_id+"") == "" 
                           || (options.payment_id+"") == ""  
                           || (options.game_type+"") == "" 
                           || (options.avatar_url+"") == ""     
                           || (options.lang+"") == ""   
                          ){
                            console.log("************************ OnJoin close undefined or empty options"+options);
                            client.close();
                            return ; 
                        }
                        // this.sendMessage(MSG_TYPE.PING_PONG,client);

                        //      for(var id =0 ,length = Object.keys(this.options_Data).length;id<length;id++){
                        //  LOG("on join "+this.options_Data[id].options.user_id+" with payment_id = "+this.options_Data[id].options.payment_id);                 
                        // }
                        var promise = new Promise((resolve, reject) => {   
                            var intervalRef =   this.clock.setInterval(() => { 
                                //  LOG("ON JOIN :========= this.options_Data length = "+Object.keys(this.options_Data).length);
                                for(var id =0 ,length = Object.keys(this.options_Data).length;id<length;id++){
                                    // LOG(options.user_id+" this.options_Data[id].options = "+this.options_Data[id].options.user_id+" onJoin isAuthSuccess = "+this.options_Data[id].options.isAuthSuccess);

                                    if(this.options_Data[id].options.user_id == options.user_id &&
                                       this.options_Data[id].options.payment_id == options.payment_id 
                                      ){ 
                                        LOG(this.options_Data[id].options.user_id+" onJoin isAuthSuccess = "+this.options_Data[id].options.isAuthSuccess);
                                        if( this.options_Data[id].options.isAuthSuccess == 1){ 
                                            if(intervalRef){
                                                intervalRef.clear();
                                            }
                                            this.onClientJoin(client,this.options_Data[id].options);
                                            resolve({"ready":1});
                                        } 
                                        if( this.options_Data[id].options.isAuthSuccess == 2){ 
                                            if(intervalRef){
                                                intervalRef.clear();
                                            }
                                            this.sendMessage(MSG_TYPE.AUTH_FAILED,client);

                                            resolve({"ready":1});
                                        }  
                                    }

                                }
                            }, 1000);

                        }); 
                        promise.then( (result)=>{ 


                        });
                    }

                    onClientJoin (client,options) {   
                        try{

                            if(typeof client == 'undefined'){
                                return; 
                            }

                            Object.assign(options,{roomId:this.roomId,clientId:client.id,sessionId:client.sessionId});


                            var isNewUser = true;
                            LOG("onClientJoin --> ",options);

                            console.log(this.playersCount+"onClientJoin --> ",options.payment_id);
                            for(var id =0 ;id<Object.keys(this.players_Data).length;id++){ 
                                //  LOG("------------------------------------------this.players_Data[id].options.user_id = ",this.players_Data[id].options.user_id); 
                                if(this.players_Data[id].options.user_id == options.user_id && this.players_Data[id].options.payment_id == options.payment_id && this.players_Data[id].options.is_bot == 0){

                                    LOG( this.clients.length +" clients------------------------------------------ Client already with same userId ",options.user_id);

                                    isNewUser = false;
                                    var oldClient = this.clients[id];
                                    client.playerIndex = id;
                                    this.players_Data[id].client = client;
                                   if(!isSkipDataEntry){
                                    database.saveExistingUserJoinRequest( {"user_id":options.user_id,"payment_id":options.payment_id,"client_id":client.id,"session_id":client.sessionId,"game_id":this.game_id,"game_type":this.game_type}); 
                                   }
                                }

                            }  

                            Object.assign(options,{"connect_status":CONNECT_STATUS.ON_JOIN});


                            //LOG("total players : ",this.clients.length," Session id : " , options.sessionId,"  options = ",options);

                            LOG('client Id :', client.id); 


                            if(isNewUser){

                                if(this.room_data.currentTurnId == null){// for first user  create room ..fill question buffer..join room in database

                                    this.currentGameState = GAME_STATE.WAIT_FOR_OPPONENT; 
/*
                                    database.createGameRoom(options).then((result)=>{ 

                                        LOG("-------------------------------------- createGameRoom => Success  "+JSON.stringify(result) );    
                                        this.game_id = JSON.parse(JSON.stringify(result)).game_id;
*/
                                   if(isSkipDataEntry){
                                     this.game_id = "1";
                                   }
                                        Object.assign(options,{"game_id":this.game_id,"tour_id":this.tour_id});
                                        for(var id =0 ;id<Object.keys(this.players_Data).length;id++){ 
                                            if(this.players_Data[id].options.user_id == options.user_id){
                                                Object.assign(this.players_Data[id].options,{"game_id":this.game_id,"tour_id":this.tour_id});
                                                break;  
                                            }
                                        }
                                   if(!isSkipDataEntry){
                                        var promiseUserConnectStatus = database.setUserConnectStatus(options);
                                   }

                                        if(isPlayCanvas){
                                            this.win_points = 1;
                                            this.level_lives = 1;
                                            this.score =  0;
                                            this.points_required =  1;
                                            this.isDBRoomReadyToStartGame = true;
                                        }else{

                                            Object.assign(options,{"game_id":this.game_id,"tour_id":this.tour_id,"lives":1,"player_count":this.clients.length,"score":0});


                                  if(!isSkipDataEntry){
                                            database.joinUserToGameRoom(options).then((result)=>{ 
                                                //LOG("joinUserToGameRoom =>Success "+JSON.stringify(result) );  
                                            },(error)=>{
                                                console.log("joinUserToGameRoom =>Error "+error);
                                            });

                                          }

                                            database.getLevelData(options).then((result)=>{ 

                                                this.win_points = JSON.parse(JSON.stringify(result))[0].win_points;
                                                this.level_lives = JSON.parse(JSON.stringify(result))[0].lives;
                                                // this.score =   JSON.parse(JSON.stringify(result))[0].score;
                                                this.points_required =  JSON.parse(JSON.stringify(result))[0].points_required;

                                                this.isDBRoomReadyToStartGame = true;
                                                LOG("getLevelData =>Success "+JSON.stringify(result) );  
                                            },(error)=>{
                                                console.log("getLevelData =>Error "+error);
                                            });

                                        }


                                   /* },(error)=>{
                                        console.log("createGameRoom =>Error "+error); 
                                    });*/

                                }else{// for other players directly join room in database



                                    var promise = new Promise((resolve, reject) => {   

                                        var intervalRef =   this.clock.setInterval(() => { 
                                            LOG("check for isDBRoomReadyToStartGame to join user "+this.isDBRoomReadyToStartGame);

                                            if(this.isDBRoomReadyToStartGame){ 
                                                if(intervalRef){
                                                    intervalRef.clear();
                                                }

                                                resolve({"ready":1});
                                            }  
                                        }, 100); 

                                    }); 

                                    promise.then( ( )=>{

                                        // LOG(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> database.joinUserToGameRoom  ",options); 
                                        Object.assign(options,{"game_id":this.game_id,"tour_id":this.tour_id,"lives":1,"player_count":this.clients.length,"score":0});

                                        for(var id =0 ;id<Object.keys(this.players_Data).length;id++){ 
                                            if(this.players_Data[id].options.user_id == options.user_id){
                                                Object.assign(this.players_Data[id].options,{"game_id":this.game_id,"tour_id":this.tour_id});
                                                break;  
                                            }
                                        } 
                                if(!isSkipDataEntry){
                                        var promiseUserConnectStatus = database.setUserConnectStatus(options);


                                        database.joinUserToGameRoom(options).then((result)=>{ 
                                            //LOG("joinUserToGameRoom =>Success "+JSON.stringify(result) );  
                                        },(error)=>{
                                            //console.log("joinUserToGameRoom =>Error "+error);
                                        });  

                                   }

                                  });


                                }               

                                client.playerIndex = this.player_available;       
                                this.player_available++;
                                this.players_Data[client.playerIndex] = {"client":client,"options":options };
                                Object.assign(this.players_Data[client.playerIndex], {"game_data" : this.game_data});
                                this.options_Data[client.playerIndex] = {"options":options};
                                this.options_Data[client.playerIndex].options.sessionId = options.sessionId; 
                                this.options_Data[client.playerIndex].options.clientId = options.clientId; 
                                this.playersQueue[client.playerIndex] = client.playerIndex;
                                LOG("this.player_available ==> ",this.player_available); 
                                //this.options_Data[client.playerIndex] = {"options":options};
                                this.room_data.players[client.playerIndex] = {id: client.playerIndex,"lives":1,"avatar_url":""+(client.playerIndex+1)/*options.avatar_url*/,"mob_no":"player "+(client.playerIndex+1)/*this.getMaskNumber(options.mob_no,2,2,"x")*/,"score":0,win_amount:0,bid_coins:0,tickets:0,tour_lives:options.tour_lives,points:options.points,level:options.level}; 


                                Object.assign(this.room_data.players[client.playerIndex], {"p1_steps" : -1,
                                                                                           "p2_steps" : -1,
                                                                                           "p3_steps" : -1,
                                                                                           "p4_steps" : -1,
                                                                                           "house_count":0
                                                                                          }); 



                                if(this.room_data.currentTurnId == null)
                                {
                                    LOG("***********************************first user**********************************************");
                                    this.room_data.currentTurnId = client.playerIndex; 

                                    if(this.opponentWaitTimeOutRef){
                                        this.opponentWaitTimeOutRef.clear();
                                    }
                                      this.opponentWaitTimeOutRef = this.clock.setTimeout(this.oponentWaitTimeOut.bind(this), OPPONENT_WAIT_TIME * 1000);

                                    if(this.botWaitTimeRef){
                                        this.botWaitTimeRef.clear();
                                    }
                                  // if(this.isBotAvailable){
                                        this.botWaitTimeRef = this.clock.setTimeout(this.botWaitTime.bind(this,options),( this.randomInt(5,5))*1000);// (OPPONENT_WAIT_TIME-20)*1000);//10*1000 // (OPPONENT_WAIT_TIME - this.randomInt(OPPONENT_WAIT_TIME-10,OPPONENT_WAIT_TIME-5))*1000);
                                    //}else{
                                     // console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                                    //} 
                                    //this.generateLudoBoard(0);
                                    this.clock.setTimeout(this.roomLockTimeOut.bind(this), (OPPONENT_WAIT_TIME-ROOM_LOCK_TIME) * 1000);//5(game start in timer)+5(safe buffer) = 10

                                    //  this.gameInstance = new GameEngine();
                                    // this.gameInstance.init(""+gameId,this); 

                                    gameQueue[gameQueue.length] = this;
                                    if(gameQueue.length == 1){
                                        this.onLoadNextGameEvent();
                                    }

                                    this.clock.setInterval(this.sendPing.bind(this), PING_PONG_TIME*1000);
                                }
                            }   

                            if(this.is_fixed_price){
                                this.pot_amount = this.amount;
                            }else { 
                                this.pot_amount = this.amount*this.player_available; 
                            }
                            this.state = {"totalPlayers":Object.keys(this.players_Data).length,"pot_amount":this.pot_amount,level_win_prize:"Win Rs "+this.pot_amount};
                            this.options_Data[client.playerIndex].options.sessionId = client.sessionId; 
                            this.options_Data[client.playerIndex].options.clientId = client.id; 
                            // this.sendMessage(MSG_TYPE.PING_PONG,client);
                            this.sendMessage(isNewUser == true ? MSG_TYPE.PLAYER_JOINED:MSG_TYPE.PLAYER_REJOINED , client); 


                            // this.room_data.winner_list[client.playerIndex] = {id:client.playerIndex,rank:2};//cheat for winner rank test
                            if(!isNewUser){

                                if(this.room_data.players[client.playerIndex].lives <= 0){
                                    this.sendMessage(MSG_TYPE.YOU_LOST, client);
                                }
                                if( typeof this.room_data.winner_list[client.playerIndex] != 'undefined'){
                                    this.sendMessage(MSG_TYPE.YOU_WIN, client);
                                }

                            }

                            if (this.player_available == this.maxPlayers && isNewUser) 
                            {
                                LOG("+++++++++++++++++++++++++Max Count reached+++++++++++++++++++++++++++++++++++++");

                                if(this.currentGameState == GAME_STATE.WAIT_FOR_OPPONENT) 
                                {
                                    this.currentGameState = GAME_STATE.READY_TO_START;
                                    this.clock.setTimeout( this.readyToStartGame.bind(this),ROOM_LOCK_TIME*1000);
                                }
                            }
                        }catch(e){ 
                            LOG_ERROR("ERROR :: "+e.stack);
                        }

                    }
                    sendPing(){
                        for(var id =0 ;id<Object.keys(this.players_Data).length;id++){
                            this.sendMessage(MSG_TYPE.PING_PONG,this.players_Data[id].client);
                        }

                    }
                    readyToStartGame(){
                        try{
                            //if(this.currentGameState == GAME_STATE.WAIT_FOR_OPPONENT)
                            {
                                this.currentGameState = GAME_STATE.READY_TO_START;

                                this.options_Data = [];
                                for(var id =0 ,length = Object.keys(this.players_Data).length;id<length;id++){ 
                                    this.options_Data[id]={options:this.players_Data[id].options};           
                                    //  Object.assign(this.options_Data[id].options,this.players_Data[id].options);
                                    LOG("readyToStartGame :    "+this.options_Data[id].options);

                                }  

                                this.playersCount = Object.keys(this.options_Data).length;
                                LOG("@@@@@@@@@@@@@@@@@@@@@@@@readyToStartGame  this.playersCount "+this.playersCount+" this.player_available"+this.player_available);
                                this.maxPlayers = this.player_available;
                                // this.lock();
                                if(this.opponentWaitTimeOutRef){
                                    this.opponentWaitTimeOutRef.clear();
                                } 

                                if(this.botWaitTimeRef){
                                    this.botWaitTimeRef.clear();
                                }

                                var promise = new Promise((resolve, reject) => {   
                                    var intervalRef =   this.clock.setInterval(() => { 
                                        LOG("check for isDBRoomReadyToStartGame to start game "+this.isDBRoomReadyToStartGame+" this.player_available "+this.player_available+"  this.playersCount = "+this.playersCount);
                                        if(this.isDBRoomReadyToStartGame){                    
                                            // if(this.isDBRoomReadyToStartGame && this.player_available >=  this.playersCount){ 
                                            this.maxPlayers = this.player_available;
                                            if(intervalRef){
                                                intervalRef.clear();
                                            }
                                            resolve({"ready":1});
                                        }  
                                    }, 1000); 

                                }); 
                                promise.then( (result)=>{
                                    LOG("this.room_data = ",this.room_data);
                                    this.maxPlayers = this.player_available; 
                                    this.sendMessage(MSG_TYPE.READY_TO_START);             
                                    this.clock.setTimeout(this.startGame.bind(this), READY_TO_START_TIME * 1000);
                                    this.setGameRewards();
                                });
                            }
                        }catch(e){
                            LOG_ERROR("ERROR :: "+e.stack);
                        }
                    } 
                    setGameRewards(){ 

                        Object.assign(this.gameWinRewards,{reward_dist:this.gameRewardsData.reward_dist[Object.keys(this.players_Data).length]});
                        Object.assign(this.gameLostRewards,{reward_dist:this.gameRewardsData.reward_dist[Object.keys(this.players_Data).length]});
                        this.is_free = (""+this.gameRewardsData.entry_fee )== "0"?true:false;
                        this.is_fixed_price = (""+this.gameRewardsData.fixed_pot)=="1"?true:false;
                        this.amount =   this.gameRewardsData.fixed_pot_value;
                        if(this.is_fixed_price){
                            this.pot_amount = this.amount;
                        }else {
                            this.pot_amount = this.amount*Object.keys(this.players_Data).length;
                        } 

                        this.state = {"totalPlayers":Object.keys(this.players_Data).length,"pot_amount":this.pot_amount};


                    }  

                    loadGame(){

                        if(this.currentGameState == GAME_STATE.GAME_CLOSE){
                            console.log(" As amount is refund to user ...Not loading game ");          
                            this.onLoadNextGameEvent();
                            return;
                        }
                        console.log("******************************************************************************************************************loading game gameId = "+gameId);

                        if(this.loadingWaitTimeOutRef){
                            this.loadingWaitTimeOutRef.clear();
                        } 

                        isLoadingGame = true;
                        // var GameEngine = importFresh("../gameEngine")();
                        this.gameInstance = new GameEngine();
                        this.gameInstance.start(""+gameId,this);     
                        if(isPlayCanvas){
                            this.game_id = ""+gameId;
                        }
                        gameId++;


                    }  
                    onLoadNextGameEvent(){        
                        if(gameQueue.length > 0 && !isLoadingGame){
                            gameQueue[0].loadGame();
                            gameQueue.splice(0,1);
                        }

                    } 

                    startGame(){
                        try{
                            this.currentGameState = GAME_STATE.IN_GAME;
                            this.room_data.points_required = this.points_required ;
                            for(var id =0 ;id<Object.keys(this.players_Data).length;id++){
                                this.room_data.players[id].lives = this.level_lives
                                Object.assign(this.players_Data[id].options,{points_required:this.points_required,win_points:this.win_points});

                                if(this.game_type == GAME_TYPE.VS_MODE && this.players_Data[id].options.connect_status == CONNECT_STATUS.ON_LEAVE){ //to refund amount if user is not connected to game at game start
                                    this.players_Data[id].client.close(); 
                                    Object.assign(this.players_Data[id].options,{"isRefund":true}); 
                                    Object.assign(this.options_Data[id].options,{"isRefund":true}); 
                                   if(!isSkipDataEntry){
                                    var promiseRefund =  database.refundAmountToUser(this.players_Data[id].options);
                                   }

                                }  

                                // Object.assign(this.room_data.players[id], {
                                //                                                    "p1_steps" : this.gameInstance.getPiecePosIndex(id,1),
                                //                                                    "p2_steps" : this.gameInstance.getPiecePosIndex(id,2),
                                //                                                    "p3_steps" : this.gameInstance.getPiecePosIndex(id,3),
                                //                                                    "p4_steps" : this.gameInstance.getPiecePosIndex(id,4)
                                //                                                   });  
                            }  
 if(!isSkipDataEntry){
                            database.startGame({"game_id":this.game_id,"tour_id":this.tour_id}); 
 }
                            this.setRoomPlay();
                            this.sendMessage(MSG_TYPE.START_GAME);
                            this.sendMessage(MSG_TYPE.INIT_PLAYER_TURN);
                            // this.clock.setTimeout(this.sendMessage.bind(this,MSG_TYPE.INIT_PLAYER_TURN),1500);

                        }catch(e){
                            LOG_ERROR("ERROR :: "+e.stack);
                        }

                    } 

                    setRoomPlay()
                    {
                        this.room_data.tt = TURN_TIME_OUT;
                        this.room_data.tmt = TURN_TIME_OUT;
                        if (this.turnTimeIntervalRef) {
                            this.turnTimeIntervalRef.clear();   
                        } 
                       this.turnTimeIntervalRef = this.clock.setInterval(this.turnTimeInterval.bind(this),1000);

                    }

                    turnTimeInterval(){  
                        try{
                            var client = this.players_Data[this.room_data.currentTurnId].client;
                            //  if(client.sessionId == this.players_Data[this.room_data.currentTurnId].client.sessionId){
                            this.room_data.tt--;
                           // LOG("timer = ",this.room_data.tt);
                            if(this.room_data.tt <= -TURN_TIME_OUT_BUFFER ){   //if shot not taken&& this.room_data.ist == 0

                                if (this.turnTimeIntervalRef) {
                                    this.turnTimeIntervalRef.clear();
                                }
                                //this.room_data.tt = 0;

                                // this.answer_status = 0;
                                // this.room_data.players[client.playerIndex].lives--;                    
                                this.sendMessage(MSG_TYPE.TURN_TIME_OUT);



                                // if(this.room_data.players[client.playerIndex].lives <= 0){// player out condition
                                //     this.onPlayerOut(client);
                                // }


                                if(this.changeTurnTimeOutRef){
                                    this.changeTurnTimeOutRef.clear();
                                }
                                this.changeTurnTimeOutRef =    this.clock.setTimeout(this.changeTurn.bind(this), CHANGE_TURN_WAIT_TIME * 1000);
                                //this.changeTurn(); 
                            }
                            //} 
                        }catch(e){
                            LOG_ERROR("ERROR :: "+e.stack);
                        }
                    } 



                    changeTurn(){ 
                        try{ 
                            
                            if(this.changeTurnTimeOutRef){
                                this.changeTurnTimeOutRef.clear();
                            }

                            if (this.turnTimeIntervalRef) {
                                this.turnTimeIntervalRef.clear();
                            }
                           
                                
                              if(this.gameInstance != null){ 
                                                     this.gameInstance.sendDataToGameEngine({
                                                                                           msg_type : MSG_TYPE.DATA_FROM_SERVER,
                                                                                           room_data:this.room_data, 
                                                                                           app_events : APP_EVENTS.TURN_CHANGED,
                                                                                         });
                                        } 
                          
                          return;
                            LOG("change Turn Object.keys(this.players_Data).length = "+Object.keys(this.players_Data).length);
                            LOG("change Turn Object.keys(this.players_Data).length = "+Object.keys(this.room_data.players).length);


                            var id = this.playersQueue.indexOf(this.room_data.currentTurnId); 
                            var winnerId = -1;//this.gameInstance.checkWin(this.room_data.currentTurnId);

                            //if(winnerId == -1 && this.gameInstance.isChangeTurn()){
                            id++;
                            // }




                            if(id >= Object.keys(this.playersQueue).length){ 
                                id = 0;
                            }
                            id = this.playersQueue[id]; 
                            LOG("********************** on changeTurn new id selected = "+id);


                            if( winnerId != -1 || this.player_available ==1 || Object.keys(this.playersQueue).length == 1){// || this.que_available <=0 player win condition || //game over condition         

                                if(winnerId != -1){
                                    this.onPlayerWin(this.players_Data[winnerId].client);
                                    this.onPlayerOut(this.players_Data[winnerId==0?1:0].client);
                                }  

                            }else{// continue game with next turn          
                                this.room_data.tt = 0;
                                this.room_data.currentTurnId = id;
                                this.setRoomPlay(); 
                                this.sendMessage(MSG_TYPE.TURN_CHANGED);



                            } 

                        }catch(e){
                            LOG_ERROR("ERROR :: "+e.stack);
                        }
                    }

                    onBotRollDiceEvent(){
                        try{
                            LOG("--------------------------------------------- onBotRollDice");

                            //         this.onMessage(this.players_Data[this.room_data.currentTurnId].client,{
                            //             msg_type:MSG_TYPE.SEND_DATA_TO_SERVER,
                            //             app_events:APP_EVENTS.ROLL_DICE
                            //             });




                        }catch(e){
                            LOG_ERROR("ERROR :: "+e.stack);
                        }
                    }  
                    onBotMovePieceEvent(){
                        //       try{ 
                        //        LOG("--------------------------------------------- onBotMovePieceEvent");
                        //        var pieceId = this.gameInstance.getPieceToMove(this.room_data.currentTurnId,this.dice_face_id);
                        //        var pieceName = this.gameInstance.getPieceName(this.room_data.currentTurnId,pieceId);
                        //         this.onMessage(this.players_Data[this.room_data.currentTurnId].client,{
                        //             msg_type:MSG_TYPE.SEND_DATA_TO_SERVER,
                        //             app_events:APP_EVENTS.MOVE_PIECE,
                        //             piece_name:pieceName,
                        //             pieceId:pieceId           
                        //             });

                        //          }catch(e){
                        //           LOG_ERROR("ERROR :: "+e.stack);
                        //       }
                    } 
                    async onLeave (client) {
                        // flag client as inactive for other users 
                        //this.state.inactivateClient(client);

                        try {

                            if(typeof client == 'undefined'){
                                return;
                            } 
                            // allow disconnected client to rejoin into this room until 10 seconds
                            LOG("onLeave >>>>>>>>>>>> client.playerIndex = "+client.playerIndex +" client.sessionId = "+client.sessionId); 

                            Object.assign(this.players_Data[client.playerIndex].options,{"connect_status":CONNECT_STATUS.ON_LEAVE});
                           if(!isSkipDataEntry){
                            var promiseUserConnectStatus = database.setUserConnectStatus(this.players_Data[client.playerIndex].options);
                           }
                            var rejoinClient;
                            rejoinClient = await this.allowReconnection(client, CLIENT_REJOIN_WAIT_TIME); 
                            if(this.room_data.players[client.playerIndex].lives >=1 &&  (this.currentGameState == GAME_STATE.WAIT_FOR_OPPONENT ||this.currentGameState == GAME_STATE.IN_GAME  )){

                                LOG("onLeave Reconnected step 1 >>>>>>>>>>>> client.sessionId = "+client.sessionId+" rejoinClient.sessionId =>"+rejoinClient.sessionId);

                                if(this.room_data.players[client.playerIndex].lives >=1 && 
                                   this.players_Data[client.playerIndex].options.isRefund != true &&
                                   (this.currentGameState == GAME_STATE.IN_GAME )){
                                    //if(    this.players_Data[client.playerIndex].options.isRefund != true)
                                    //               {
                                    LOG("onLeave Reconnected step 2 >>>>>>>>>>>> client.sessionId = "+client.sessionId+" rejoinClient.sessionId =>"+rejoinClient.sessionId);

                                    for(var id =0 ;id<Object.keys(this.players_Data).length;id++){ 

                                        if(this.players_Data[id].client.sessionId == client.sessionId){ 

                                            LOG("onLeave Reconnected step 3 >>>>>>>>>>>> client.sessionId = "+client.sessionId+" rejoinClient.sessionId =>"+rejoinClient.sessionId);

                                            rejoinClient.playerIndex = id;
                                            this.players_Data[id].client = rejoinClient; 
                                            this.options_Data[id].options.sessionId = rejoinClient.sessionId;
                                            this.options_Data[id].options.clientId = rejoinClient.clientId;
                                            this.sendMessage(MSG_TYPE.PLAYER_REJOINED,rejoinClient);
                                            //this.sendMessage(MSG_TYPE.PING_PONG,rejoinClient); 
                                            Object.assign(this.players_Data[id].options,{"connect_status":CONNECT_STATUS.RECONNECTED}); 
                                           if(!isSkipDataEntry){
                                            var promiseUserConnectStatus = database.setUserConnectStatus(this.players_Data[id].options);
                                           }

                                        }
                                    }    

                                }
                            } //if close

                        } catch (e) {

                            console.log(CLIENT_REJOIN_WAIT_TIME+" seconds expired. let's remove the client."+client.sessionId);        

                        } 
                    } 

                    sendMessage(msg_type,client = null,data = null){

                        try{

                            if(typeof client == 'undefined'){
                                return;
                            }

                            if(msg_type != MSG_TYPE.PING_PONG && msg_type != MSG_TYPE.UPDATE_ENTITY){
                                LOG("Server send Message--->"+msg_type);
                            }
                            var message ;//= {};
                            this.room_data.currentGameState = this.currentGameState;
                            switch(msg_type){

                                case  MSG_TYPE.PING_PONG:
                                    message = {
                                        msg_type : msg_type                
                                    }; 
                                    for(var id =0 ;id<Object.keys(this.players_Data).length;id++){ 
                                        if(this.players_Data[id].client.sessionId == client.sessionId){
                                            this.send(this.players_Data[id].client,message);  
                                            break;
                                        }
                                    }   
                                    //   this.send(client,message);
                                    break;
                                case  MSG_TYPE.AUTH_FAILED:
                                    message = {
                                        msg_type : msg_type                
                                    }; 
                                    this.send(client,message);
                                    break;
                                case  MSG_TYPE.PLAYER_JOINED:

                                    message = {
                                        msg_type : msg_type,
                                        id:client.playerIndex,
                                        room_data:this.room_data,
                                        OPPONENT_WAIT_TIME:(OPPONENT_WAIT_TIME+ROOM_LOCK_TIME+1)
                                    }; 
                                    // this.setTranslatedRoomQueData(this.players_Data[client.playerIndex].options.lang);//step1
                                    message.room_data = this.room_data;//step2
                                    this.send(client,message);
                                    break;
                                case  MSG_TYPE.PLAYER_REJOINED:

                                    message = {
                                        msg_type : msg_type,
                                        id:client.playerIndex,
                                        room_data:this.room_data 
                                    }; 
                                    // this.setTranslatedRoomQueData(this.players_Data[client.playerIndex].options.lang);//step1
                                    message.room_data = this.room_data;//step2
                                    this.send(client,message);
                                    break; 
                                case  MSG_TYPE.NO_OPPONENTS:
                                    message = {
                                        msg_type : msg_type        
                                    }; 
                                    for(var id =0 ;id<Object.keys(this.players_Data).length;id++){                
                                        this.send(this.players_Data[id].client,message);                              
                                    } 
                                    break; 
                                case  MSG_TYPE.READY_TO_START:
                                    message = {
                                        msg_type : msg_type,
                                        room_data:this.room_data,
                                        READY_TO_START_TIME:READY_TO_START_TIME
                                    };          
                                    // this.broadcast(message); 

                                    for(var id =0 ;id<Object.keys(this.players_Data).length;id++){                       
                                        //this.setTranslatedRoomQueData(this.players_Data[id].options.lang);
                                        message.room_data = this.room_data;
                                        this.send(this.players_Data[id].client,message);                   

                                    } 
                                    break;

                                case  MSG_TYPE.START_GAME:
                                case MSG_TYPE.INIT_PLAYER_TURN:
                                    message = {
                                        msg_type : MSG_TYPE.DATA_FROM_SERVER,
                                        room_data:this.room_data,
                                        app_events :msg_type 
                                    }; 

                                    for(var id =0 ;id<Object.keys(this.players_Data).length;id++){
                                        if(this.room_data.players[id].lives >=1){
                                            //  this.setTranslatedRoomQueData(this.players_Data[id].options.lang);
                                            // message.room_data = this.room_data;
                                            this.send(this.players_Data[id].client,message);                   
                                        }
                                    } 
                                    if(this.gameInstance != null){ 
                                        this.gameInstance.sendDataToGameEngine(message);
                                    }  

                                    break; 

                                case MSG_TYPE.ANSWER_RESULT:
                                    message = {
                                        msg_type : msg_type,
                                        room_data:this.room_data, 
                                        // answer_status:this.answer_status 
                                    }; 
                                    //this.broadcast(message);  
                                    for(var id =0 ;id<Object.keys(this.players_Data).length;id++){                     
                                        //this.setTranslatedRoomQueData(this.players_Data[id].options.lang);
                                        message.room_data = this.room_data;
                                        this.send(this.players_Data[id].client,message);                   

                                    } 
                                    break;

                                case MSG_TYPE.TURN_CHANGED:      

                                    message = {
                                        msg_type :  MSG_TYPE.DATA_FROM_SERVER,
                                        room_data:this.room_data,
                                        app_events :msg_type              
                                    }; 
                                    // this.broadcast(message); 
                                    for(var id =0 ;id<Object.keys(this.players_Data).length;id++){
                                        if(this.room_data.players[id].lives >=1){
                                            //  message.room_data = this.room_data;
                                            this.send(this.players_Data[id].client,message);
                                        }
                                    }   
                                    break; 

 

                                case MSG_TYPE.TURN_TIME_OUT:
                                    message = {
                                        msg_type : MSG_TYPE.DATA_FROM_SERVER,
                                        room_data:this.room_data, 
                                        app_events : msg_type,
                                        
                                    };   
                                    // this.broadcast(message); 
                                    for(var id =0 ;id<Object.keys(this.players_Data).length;id++){

                                        // this.setTranslatedRoomQueData(this.players_Data[id].options.lang);
                                        // message.room_data = this.room_data;
                                        this.send(this.players_Data[id].client,message);                   
                                    } 
                                    
                                    break;  



                                case MSG_TYPE.YOU_WIN:      

                                    message = {
                                        msg_type : msg_type,
                                        room_data:this.room_data
                                    };  
                                    //this.setTranslatedRoomQueData(this.players_Data[client.playerIndex].options.lang);//step1
                                    message.room_data = this.room_data;
                                    this.send(client,message);
                                    // this.send(this.players_Data[this.room_data.currentTurnId],message);
                                    break;
                                case MSG_TYPE.YOU_LOST:      

                                    message = {
                                        msg_type : msg_type,
                                        room_data:this.room_data
                                    };  
                                    //this.setTranslatedRoomQueData(this.players_Data[client.playerIndex].options.lang);//step1
                                    message.room_data = this.room_data;
                                    this.send(client,message);
                                    // this.send(this.players_Data[this.room_data.currentTurnId],message);

                                    break;

                                case MSG_TYPE.GAME_DRAW:      

                                    message = {
                                        msg_type : msg_type,
                                        room_data:this.room_data
                                    }; 
                                    for(var id =0 ;id<Object.keys(this.players_Data).length;id++){
                                        if(this.room_data.players[id].lives >=1){
                                            //this.setTranslatedRoomQueData(this.players_Data[id].options.lang);
                                            message.room_data = this.room_data;
                                            this.send(this.players_Data[id].client,message);                   
                                        }
                                    } 
                                    break;
                            }
                        }catch(e){
                            LOG_ERROR("ERROR :: "+e.stack);
                        }
                    } 

                    onMessage (client, data) { 
                        try{
                            if(typeof client == 'undefined'){
                                 return;
                             }  

                            if(data.msg_type != MSG_TYPE.PING_PONG && data.msg_type != MSG_TYPE.SEND_DATA_TO_SERVER){
                                LOG("message from client  : "+client.id+" client.sessionId "+client.sessionId ,JSON.stringify(data));
                            }
                            var msg_type = data["msg_type"];
                            var message ;
                            switch(msg_type){  
                                case MSG_TYPE.PING_PONG:
                                    //  this.sendMessage(MSG_TYPE.PING_PONG,client); 
                                    break;  
                                case   MSG_TYPE.SEND_DATA_TO_SERVER:

                                  if(client.sessionId == this.players_Data[this.room_data.currentTurnId].client.sessionId){
                            
                                    if(typeof data.app_events !== "undefined" && this.room_data.tt > -TURN_TIME_OUT_BUFFER )
                                    {              
  
                                         switch(data.app_events){
                                           case APP_EVENTS.WHITE_BALL_FORCE:
                                                        if (this.turnTimeIntervalRef) {
                                                          this.turnTimeIntervalRef.clear();   
                                                           } 
                                               break;
                                          }
                                
                                         if(this.gameInstance != null){ 
                                            this.gameInstance.sendDataToGameEngine(data);
                                        }  
                                    } 
                                  }else{
                                   
                              //  console.log("Alert ::  Message received by wrong user"+data);
                             
                                  }
                                    break;
                            }
                        }catch(e){ 
                             LOG_ERROR("ERROR :: "+e.stack);
                        }
                     } 

                    onMessageFromGameEngine(data){

                        // if(typeof data.room_data != 'undefined'){
                        //     Object.assign(this.room_data,data.room_data);
                        //     data.room_data = this.room_data;
                        // }
                        // LOG("onMessageFromGameEngine :: ",data);
                        switch(data.msg_type){
                            case MSG_TYPE.SEND_DATA_TO_SERVER: 
                                data.msg_type = MSG_TYPE.DATA_FROM_SERVER;             

                                switch(data.app_events){
                                    case APP_EVENTS.GAME_LOADED:
                                        isLoadingGame = false;
                                        this.onLoadNextGameEvent();
                                        //this.closeRoom();//cheat for load test 
                                        break;


                                    case APP_EVENTS.UPDATE_ENTITY: 

                                        this.room_data.cbc = data.cbc;
                                        this.room_data.cs = data.cs; 
                                        this.room_data.bw = data.bw;
                                        this.room_data.b1 = data.b1;
                                        this.room_data.b2 = data.b2;
                                        this.room_data.b3 = data.b3;
                                        this.room_data.b4 = data.b4;
                                        this.room_data.b5 = data.b5;
                                        this.room_data.b6 = data.b6;
                                        this.room_data.b7 = data.b7;
                                        this.room_data.b8 = data.b8;
                                        this.room_data.b9 = data.b9;
                                        this.room_data.b10 = data.b10;
                                        this.room_data.b11 = data.b11;
                                        this.room_data.b12 = data.b12;
                                        this.room_data.b13 = data.b13;
                                        this.room_data.b14 = data.b14;
                                        this.room_data.b15 = data.b15;
                                        this.room_data.ist = data.ist;


                                        break;
                                     case APP_EVENTS.WHITE_BALL_FORCE:
                                                          this.room_data.wbf =data.wbf;
                                               break; 
                                     case  APP_EVENTS.TURN_CHANGED :
                                        this.room_data.currentTurnId = data.id; 
                                        this.setRoomPlay();  
                                      if(this.room_data.currentTurnId == 1 && typeof this.players_Data[this.room_data.currentTurnId]!= 'undefined' && this.players_Data[this.room_data.currentTurnId].options.is_bot == 1)
                                      {
                                        if(this.gameInstance != null){ 
                                                     this.gameInstance.sendDataToGameEngine({
                                                                                           msg_type : MSG_TYPE.DATA_FROM_SERVER,
                                                                                           room_data:this.room_data, 
                                                                                            app_events : APP_EVENTS.SET_READY_TO_PLAY,
                                                                                         });
                                        } 
                                        
                                      } 
  
                                        break;
                                    case  APP_EVENTS.FOUL :                           
                                         this.room_data.ft =data.ft;//ft -->for foul type
                                        break; 
                                    case  APP_EVENTS.BALL_POTTED :                          
                                          this.room_data.bp =data.bp;//bt -->for ball potted id
                                          this.room_data.bpl.push(data.bp);//bpl for ball potted list
                                        break; 
                                    case  APP_EVENTS.BALL_SET :                          
                                        this.room_data.bt =data.bt;//bt -->for ball type (solids or stripes) set for player
                                        break; 
                                     case  APP_EVENTS.WHITE_BALL_DRAG :                          
                                        this.room_data.mdp =data.mdp;//mdp -->for mose drag position when ball in hand available
                                    //console.log(this.room_data.bw.p+"}}}}}}}}}"+this.room_data.mdp);   
                                     break;
                                     case  APP_EVENTS.WHITE_BALL_DEND :                          
                                        this.room_data.mdp =data.mdp;//mdp -->for mose drag position when ball in hand available
                                        break;
                                    case APP_EVENTS.GAME_OVER:
                                        var winnerId = data.winnerId;
                                        if(winnerId != -1){
                                            this.onPlayerWin(this.players_Data[winnerId].client);
                                            this.onPlayerOut(this.players_Data[winnerId==0?1:0].client);
                                        }   
                                        return;
                                        break;

                                    default:
 
                                        break; 
                                }      

                                var message = {
                                    msg_type : MSG_TYPE.DATA_FROM_SERVER,
                                    room_data:this.room_data,
                                    app_events :data.app_events 
                                }; 

                                for(var id =0 ;id<Object.keys(this.players_Data).length;id++){
                                    this.send(this.players_Data[id].client,message);                   

                                } 


                                break


                        }



                    }

                    // 
                    onGameOver(){

                        this.currentGameState = GAME_STATE.GAME_END;
                        // this.lock();

                        // for(var id =0;id<Object.keys(this.players_Data).length;id++){          
                        //      if(this.room_data.players[id].house_count < 4){
                        //          this.sendMessage(MSG_TYPE.YOU_LOST,this.players_Data[id].client);
                        //        //this.clock.setTimeout(this.sendMessage.bind(this,MSG_TYPE.YOU_LOST,this.players_Data[id].client), (2) * 1000);
                        //      }
                        // }

                        this.onGameEnd ();

                    } 
                    onPlayerWin(client){
                        try{
                            if(typeof client == 'undefined'){
                                return;
                            }       
                            this.sendMessage(MSG_TYPE.YOU_WIN,client);
                            this.clock.setTimeout(this.onGameOver.bind(this), (1) * 1000);
                             return;
                            // for(var i =0;i<Object.keys(this.players_Data).length;i++){   
                            var i = client.playerIndex;
                            this.room_data.players[i].win_amount = this.gameWinRewards.reward_dist[this.rank];
                            Object.assign(this.players_Data[i].options,{"win_amount":this.room_data.players[i].win_amount});                     

                            this.players_Data[client.playerIndex].options.score += this.room_data.players[client.playerIndex].win_amount+5;
                            this.room_data.players[client.playerIndex].score += this.room_data.players[client.playerIndex].win_amount+5;                                     

                            this.player_available--; 
                            this.postScoreToLB(client.playerIndex,true);       


                            if(this.players_Data[i].options.isRefund != true){
                                this.room_data.winner_list[i] = {id:i,rank:(this.rank+1)};                       

                                //  Object.assign( this.room_data.players[i],{rewards:this.gameWinRewards,"winning_id":this.winning_id});
                                //  Object.assign( this.players_Data[i].options,{rewards:this.gameWinRewards,"winning_id":this.winning_id});

                                // var promiseRewards = database.insertUserRewards(this.players_Data[i].options);

                                switch(this.game_type){
                                    case GAME_TYPE.FTP_MODE:
                                        // if( this.players_Data[i].options.user_id>12){// 1--12 are test users ..and
                                        this.players_Data[i].options.tour_lives = 0;
                                        //}
                                        this.players_Data[i].options.level = 0;
                                        this.players_Data[i].options.points = 0; 
                                        this.room_data.players[i].points = 0; 
                                        var promiseTourLives =  database.updateLivesCount(this.players_Data[i].options);
                                     
                                        if(this.players_Data[client.playerIndex].options.isRefund != true && this.room_data.players[client.playerIndex].win_amount > 0){
                                            var promiseUpdateCoinRewards = database.updateCoinRewards({"game_id":this.game_id,"tour_id":this.tour_id,"user_id":this.players_Data[client.playerIndex].options.user_id,amount:this.players_Data[client.playerIndex].options.win_amount,"reward_type":"coin"}); 
                                        }  
                                     
                                        //  var promiseAddAmountToWallet = database.addAmountToUserWallet(this.players_Data[i].options);  
                                        //  var promise2 =  database.addUserToWinnerList({"game_id":this.game_id,"tour_id":this.tour_id,"user_id":this.players_Data[i].options.user_id,"payment_id":this.players_Data[i].options.payment_id,"points":0,"level":0});

                                        break;
                                    case GAME_TYPE.TOUR_MODE:
                                        this.players_Data[i].options.tour_lives = 0;
                                        //}
                                        this.players_Data[i].options.level = 0;
                                        this.players_Data[i].options.points = 0; 
                                        this.room_data.players[i].points = 0; 
                                        var promiseTourLives =  database.updateLivesCount(this.players_Data[i].options);
                                        if(this.players_Data[client.playerIndex].options.isRefund != true && this.room_data.players[client.playerIndex].win_amount > 0){
                                            var promiseUpdateCoinRewards = database.updateCoinRewards({"game_id":this.game_id,"tour_id":this.tour_id,"user_id":this.players_Data[client.playerIndex].options.user_id,amount:this.players_Data[client.playerIndex].options.win_amount,"reward_type":"coin"}); 
                                        }  
                                        //  var promiseAddAmountToWallet = database.addAmountToUserWallet(this.players_Data[i].options);  
                                        //   var promise2 =  database.addUserToWinnerList({"game_id":this.game_id,"tour_id":this.tour_id,"user_id":this.players_Data[i].options.user_id,"payment_id":this.players_Data[i].options.payment_id,"points":0,"level":0});


                                        //                                this.players_Data[i].options.points += this.win_points; 
                                        //                                this.room_data.players[i].points += this.win_points;                         
                                        //                                this.players_Data[i].options.total_points+= this.win_points;   
                                        //                                    //  this.players_Data[i].options.level = 0;//cheat
                                        //                                    // this.players_Data[i].options.points = 0;//cheat
                                        //                                    //  this.players_Data[i].options.total_points = 0;//cheat
                                        //                                 if(this.players_Data[i].options.points  >= this.players_Data[i].options.points_required){                                  

                                        //                                     this.players_Data[i].options.tour_lives += this.win_lives;                                  

                                        //                                     //this.players_Data[i].options.level++;// Level up
                                        //                                    // if(this.players_Data[i].options.level > 11){
                                        //                                    //   this.players_Data[i].options.level = 0;
                                        //                                    //   this.players_Data[i].options.total_points = 0;
                                        //                                    //   this.players_Data[i].options.tour_lives  = 0;
                                        //                                    // }
                                        //                                     this.players_Data[i].options.points = 0; 
                                        //                                     var promiseTourLives =  database.updateLivesCount(this.players_Data[i].options);
                                        //                                     var promiseAddAmountToWallet = database.addAmountToUserWallet(this.players_Data[i].options); 
                                        //                                  } 

                                        //                                 var promise =  database.setUserLevelAndPoints(this.players_Data[i].options);
                                        //                                 promise.then((result)=>{  

                                        //                                     LOG("Adding data to winner list "+JSON.stringify(result));
                                        //                                     var user_id = JSON.parse(JSON.stringify(result)).user_id;
                                        //                                     var points  = JSON.parse(JSON.stringify(result)).points;
                                        //                                    // var level   = JSON.parse(JSON.stringify(result)).level;                        
                                        //                                     var promise3 =  database.addUserToWinnerList({"game_id":this.game_id,"tour_id":this.tour_id,"user_id":user_id,"points":points}); //,"level":level
                                        //                                 },(error)=>{

                                        //                                 });

                                        break;

                                }

                         if(!isSkipDataEntry){
                                    var promise1 =  database.updateUserTable({"game_id":this.game_id,"tour_id":this.tour_id,"user_id":this.players_Data[i].options.user_id,"lives":this.room_data.players[i].lives,"score":this.room_data.players[i].score,"status":1});

                                 }
                            }   

                            //}

                            this.rank++;



                            // this.sendMessage(MSG_TYPE.YOU_WIN,client);
                        }catch(e){
                            LOG_ERROR("ERROR :: "+e.stack);
                        }

                    }
                    onPlayerOut(client){
                        try{
                            if(typeof client == 'undefined'){
                                return;
                            }  
                            this.sendMessage(MSG_TYPE.YOU_LOST,client);
                            return;
                            this.room_data.players[client.playerIndex].tour_lives--;
                            this.players_Data[client.playerIndex].options.tour_lives--;
                            if(this.players_Data[client.playerIndex].options.tour_lives <= 0){
                                this.players_Data[client.playerIndex].options.points = 0;  
                                this.players_Data[client.playerIndex].options.level = 0;  
                                this.players_Data[client.playerIndex].options.total_points = 0;
                            }
                            this.room_data.players[client.playerIndex].win_amount = this.gameWinRewards.reward_dist[this.rank];
                            Object.assign(this.players_Data[client.playerIndex].options,{"win_amount":this.room_data.players[client.playerIndex].win_amount});
                            this.players_Data[client.playerIndex].options.score += this.room_data.players[client.playerIndex].win_amount+5;
                            this.room_data.players[client.playerIndex].score += this.room_data.players[client.playerIndex].win_amount+5;        

                            this.postScoreToLB(client.playerIndex,false); 


                            if(this.players_Data[client.playerIndex].options.isRefund != true && this.room_data.players[client.playerIndex].win_amount > 0  && this.game_type == GAME_TYPE.FTP_MODE){
                                var promiseUpdateCoinRewards = database.updateCoinRewards({"game_id":this.game_id,"tour_id":this.tour_id,"user_id":this.players_Data[client.playerIndex].options.user_id,amount:this.players_Data[client.playerIndex].options.win_amount,"reward_type":"coin"}); 
                            }  

                            var promiseTourLives = database.updateLivesCount(this.players_Data[client.playerIndex].options);

                            var promise =  database.setUserLevelAndPoints(this.players_Data[client.playerIndex].options);
                           if(!isSkipDataEntry){
                            var promise1 =  database.updateUserTable({"game_id":this.game_id,"tour_id":this.tour_id,"user_id":this.players_Data[client.playerIndex].options.user_id,"payment_id":this.players_Data[client.playerIndex].options.payment_id,"lives":this.room_data.players[client.playerIndex].lives,"score":this.room_data.players[client.playerIndex].score,"status":0});
                           }
                            this.clock.setTimeout(this.sendMessage.bind(this,MSG_TYPE.YOU_LOST,client), 2* 1000);

                            this.player_available--; 
                        }catch(e){
                            LOG_ERROR("ERROR :: "+e.stack);
                        }
                    }


                    postScoreToLB(player_id,isWinner){  

                        var promiseLB = database.postScoreToLB({auth_token:this.players_Data[player_id].options.user_id,"tour_id":this.tour_id,"user_id":this.players_Data[player_id].options.user_id,score:this.room_data.players[player_id].score}).then((result)=>{ 

                            LOG("postScoreToLB =>Success "+JSON.stringify(result) ); 

                            Object.assign( this.room_data.players[player_id],{"lb_data":result});  

                            Object.assign(this.players_Data[player_id].options,{"lb_data":result});  

                            if(this.player_available <=1 && isWinner){// send win msg when game is complete


                                this.clock.setTimeout(this.sendMessage.bind(this,MSG_TYPE.YOU_WIN), (1) * 1000);

                                this.clock.setTimeout(this.onGameOver.bind(this), (3) * 1000);
                            }



                        },(error)=>{
                            LOG("postScoreToLB =>Error "+error);

                        });

                    }   

                    onGameEnd(){
                        try{
                            LOG("_________________________________________ onGameEnd _________________________",this.game_id);
                            // this.lock();
                            if(this.currentGameState != GAME_STATE.NONE){
                                LOG("**********************************  Room Log = "+this.currentGameState );         


                                //             for(var id = 0 ;id<Object.keys(this.players_Data).length;id++){
                                //                 // give coins to lost user
                                //                 if(this.players_Data[id].options.is_bot== 0 && this.players_Data[id].options.score > 0 ){
                                //                     database.updateCoinRewards({"game_id":this.game_id,"user_id":this.players_Data[id].options.user_id,"payment_id":this.players_Data[id].options.payment_id,"score":this.room_data.players[id].score});
                                //                 }

                                //             } 
                                for(var id =0 ;id<Object.keys(this.options_Data).length;id++){ 
                                    var payment_id = this.options_Data[id].options.payment_id;
                                    if(payment_id != 'bot_payment_id'){
                                        this.presence.publish("DEL_PAYMENT_ID",""+payment_id);                       
                                    }
                                } 

                                //this.presence.unsubscribe("ADD_PAYMENT_ID");
                                //  this.presence.unsubscribe("DEL_PAYMENT_ID");  
                                     if(!isSkipDataEntry){
                                          database.saveLog({"game_id":this.game_id,"tour_id":this.tour_id,"end_time":null,"game_state":this.currentGameState,"room_log":this.roomLog});
                                     }
                              }
                            // if(this.currentGameState != GAME_STATE.GAME_CLOSE){
                            //  this.clock.setTimeout(this.closeRoom.bind(this), CLIENT_REJOIN_WAIT_TIME * 3000);
                            // }else{
                            this.closeRoom();
                            // }

                        }catch(e){
                            LOG_ERROR("ERROR :: "+e.stack);
                        }

                    }
                    closeRoom(){
                        try{
                            this.lock();
                            //this.setSeatReservationTime(1);  
                            //this.disconnect ();
                            for(var id =0 ,length = this.clients.length;id<length;id++){   
                                this.clients[id].close();        
                            } 
                        }catch(e){
                            LOG_ERROR("ERROR :: "+e.stack);
                        }
                    }

                    onDispose () {


                        /*
  for(var i=0;i<Object.keys(this.roomLog).length;i++){
         LOG("** ",JSON.stringify(this.roomLog[i]));
  }
  */
                        try{

                           if(!isSkipDataEntry){
                            if(this.currentGameState == GAME_STATE.IN_GAME){
                                LOG("**********************************  Room Log = "+this.currentGameState );
                                database.saveLog({"game_id":this.game_id,"tour_id":this.tour_id,"end_time":null,"game_state":this.currentGameState,"room_log":this.roomLog});
                            }
                           }


                            // this.disconnect ();

                            if(this.opponentWaitTimeOutRef){
                                this.opponentWaitTimeOutRef.clear();
                            }

                            if(this.botWaitTimeRef){
                                this.botWaitTimeRef.clear();
                            } 

                            if (this.turnTimeIntervalRef) {
                                this.turnTimeIntervalRef.clear();
                            }

                            this.clock.clear();
                            LOG("Dispose BasicRoom");
                        }catch(e){
                            LOG_ERROR("ERROR :: "+e.stack);
                        }
                    }

                    generateLudoBoard(startIndex)
                    {
                        if(startIndex  >= 52)
                        {
                            return;
                        }
                        this.ludoBoardBlocksArray[startIndex] = -2;
                        for(var i =startIndex +1; i <=startIndex + 12; i++)
                        {
                            this.ludoBoardBlocksArray[i] = -1;
                            if(i = startIndex + 8)
                            {
                                this.ludoBoardBlocksArray[i] = -2;
                            }
                        }
                        this.generateLudoBoard(startIndex+13);
                    }
                    randomInt(min,max) // min and max included
                    {
                        return Math.floor(Math.random()*(max-min+1)+min);
                    } 
                    getMaskNumber(number,startUnMask,endUnMask,maskBy){
                        var first = number.substring(0, startUnMask);  
                        var last = number.substring(number.length - endUnMask);

                        var mask = number.substring(startUnMask, number.length - endUnMask).replace(/\d/g,maskBy);
                        return (first + mask + last);
                    } 


                } 