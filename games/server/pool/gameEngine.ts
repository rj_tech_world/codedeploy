import { LOG,LOG_ERROR} from './server';
//import  {pc,DOM} from  "./playcanvas/playcanvas";  
import  * as ammo from  "./playcanvas/ammo"; 
var JSDOM = require("jsdom").JSDOM;
//var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

//var xhr = new XMLHttpRequest();
const fs = require('fs'); 
enum  APP_EVENTS  { 
    SEND_DATA_TO_SERVER="SEND_DATA_TO_SERVER",   
    DATA_FROM_SERVER="DATA_FROM_SERVER",
    UPDATE_LOOP="UPDATE_LOOP", 
}
require('dotenv').config(); 
 
  
 class GameEngine {    
   gameId = -1; 
  name = ""; 
   playObj;  
   playApp = null; 
myName(){ 
  console.log("name = "+this.name); 
} 
  
  sendDataToGameEngine(data){
   this.playApp.fire(APP_EVENTS.DATA_FROM_SERVER,data);
  }
    updateGameLoop(){
   this.playApp.fire(APP_EVENTS.UPDATE_LOOP);
  }
start (gameId,roomInstance){
        try{
     this.name = gameId; 
    this.gameId = gameId;
     var importFresh = require('import-fresh');
     this.playObj = new (importFresh("./playcanvas/playcanvas"))(gameId);
    console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>",this.playObj.obj._pc.version);

    var pc = (this.playObj.obj._pc);
var DOM =  (this.playObj.obj.DOM);  
 if(DOM == null){
DOM =  new JSDOM("<!DOCTYPE html><head></head><body><div>Foo</div></body></html>",{
                      resources: 'usable',
                      runScripts: 'dangerously',  
                      });
 }
var window = DOM.window;  
 var navigator = window.navigator;
 var document = window.document;
var canvasMockify = require("canvas-mock"); 

// create a fake window environment to be able to run PlayCanvas
var canvas = window.document.createElement("canvas");
  canvas.id = gameId;
canvasMockify(canvas);
try{
 var _this = this; 
 
var httpServer;  
var wsServer;
var ball = null;
 var ASSET_PREFIX = process.env.SERVER_GAME_URL;
  var      SCRIPT_PREFIX = process.env.SERVER_GAME_URL;
  var      SCENE_PATH = process.env.SERVER_GAME_URL+"813610.json";
  var      CONTEXT_OPTIONS = {
            'antialias': false,
            'alpha': false,
            'preserveDrawingBuffer': false, 
            'preferWebGl2': false
        }; 
  var      SCRIPTS = [ 23741058, 23741022, 23741119, 23741025, 23741024, 23741043, 23741174];
   var     CONFIG_FILENAME = process.env.SERVER_GAME_URL+"config.json";
   var     INPUT_SETTINGS = {
            useKeyboard: true,
            useMouse: true,
            useGamepads: false,
            useTouch: true  
        };
        pc.script.legacy = false;  
var options = {
  resources: 'usable',
  runScripts: 'dangerously', 
};
 _this.playApp = new pc.Application(canvas, {            
            graphicsDeviceOptions: CONTEXT_OPTIONS,
            assetPrefix: ASSET_PREFIX ,
            scriptPrefix: SCRIPT_PREFIX,
            scriptsOrder: SCRIPTS 
        }); 
  

 
} catch (e) {
        if (e instanceof pc.UnsupportedBrowserError) {
            console.log('This page requires a browser that supports WebGL.<br/>' +
                    '<a href="http://get.webgl.org">Click here to find out more.</a>');
        } else if (e instanceof pc.ContextCreationError) {
            console.log("It doesn't appear your computer can support WebGL.<br/>" +
                    '<a href="http://get.webgl.org/troubleshooting/">Click here for more information.</a>');
        } else {
            console.log('Could not initialize application. Error: ' + e);
        }

       
    }   
      
  document.data = pc;//to get refrence of pc object in externally loaded scripts
 var pp = this.playObj;

  
  _this.playApp.configure(CONFIG_FILENAME, function (err) {
        if (err) {
            console.error(err);
        }
         console.log(_this.playApp._fillMode, _this.playApp._width, _this.playApp._height);  
 

      _this.playApp.preload(function (err) {
          console.log("preload>>>>>>>>>>>>>>>>>>>>>>> "+gameId); 
  
           
            _this.playApp.on(APP_EVENTS.SEND_DATA_TO_SERVER,roomInstance.onMessageFromGameEngine, roomInstance);  
      
             _this.playApp.loadScene(SCENE_PATH, function (err, scene) {
                if (err) {
                    console.error(err); 
                }
           
 
             console.log("Loaded ");
              _this.playApp.start();
            
            });
          
         
               
        });       
        
  
    });
      }catch(e){
      LOG_ERROR("ERROR :: "+e.stack);
      }
  
}  
   destroyApp(){ 
     try{
      console.log("------------------------- destroyApp gameId =  "+this.gameId ); 
     this.playApp.timeScale = 0;
    this.playApp.destroy();
    this.releaseMemory();
     }catch(e){
                LOG_ERROR("ERROR :: "+e.stack);
            }
       
   } 
    releaseMemory(){ 
      this.playObj.obj.DOM.window.close();
      this.gameId  = null;  
      this. name  = null; 
      this.playObj.obj.pc = null
       this.playObj.obj.DOM = null;   
     this.playObj = null; 
      this.playApp = null;  
    }

}  
module.exports = function(){
      return  GameEngine;
      };                                           