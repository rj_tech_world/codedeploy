import * as path from 'path';
import * as express from 'express';
import * as promise from 'promise'; 
import * as cluster from "cluster";
import * as os from "os";

//import * as RedisPresence from 'redis'; 
//import * as serveIndex from 'serve-index';

import { createServer } from 'http';
//import { createServer } from 'https';
import { Server,RedisPresence ,MemsharedPresence} from  "colyseus"; 
import {DateTime} from  "./datetime"; 
//import { monitor } from '@colyseus/monitor';
// Import demo room handlers
 import { GameRoom } from "./rooms/GameRoom";
 import { Database } from "./database"; 
import  * as Request from "request";
require('dotenv').config();
const fs = require('fs');

// import { StateHandlerRoom } from "./rooms/02-state-handler";
// import { AuthRoom } from "./rooms/03-auth";
// import { CreateOrJoinRoom } from "./rooms/04-create-or-join-room";
 var log_file = fs.createWriteStream(__dirname+'/log_error.txt', {flags : 'a'});
 //var log_debugfile = fs.createWriteStream(__dirname+'/debug_log.txt', {flags : 'a'});
var log_stdout = process.stdout;
export var LOG = process.env.DEBUG == 'true' ?console.log.bind(console) : function () {}; 
//export var LOG = process.env.DEBUG == 'true' ?function(msg){log_debugfile.write(msg);} : function () {};
  
export var LOG_ERROR = process.env.DEBUG_ERROR == 'true' ?function(error){log_file.write(error);console.log(error); } : function () {};
   
//const port = Number(4001);
const port = Number(process.env.PORT);
const app = express();
const Origins: (string)[] = ["https://launch.playcanvas.com","http://launch.playcanvas.com","http://play.games360platform.com","https://play.games360platform.com"]; 
export var presence = new  RedisPresence();
export const totalServerClients = [];
export const failedPaymentIds = [];
export const addPaymentId = function(payment_id){
  
         var index =  totalServerClients.indexOf(""+payment_id);
        if(index < 0 ){
        totalServerClients[totalServerClients.length] = ""+payment_id;
        LOG("_______________________________ payment_id added = "+totalServerClients[totalServerClients.length-1]);
         }
      } 
 export  const delPaymentId = function(payment_id){
      
         var index =  totalServerClients.indexOf(""+payment_id);
         if(index >=0 ){
         totalServerClients.splice(index,1);
         presence.del(""+payment_id);
        LOG("_______________________________ payment_id deleted = "+payment_id+ " at index = "+index);
         }
      }
 export const addFailedPaymentId = function(payment_id){
  
         var index =  failedPaymentIds.indexOf(""+payment_id);
        if(index < 0 ){
         if(failedPaymentIds.length >= 200){
            failedPaymentIds.splice(0,100);
            } 
        failedPaymentIds[failedPaymentIds.length] = ""+payment_id;
        LOG("_______________________________Failed payment_id added = "+failedPaymentIds[failedPaymentIds.length-1]);
         }
      } 
  export  const delFailedPaymentId = function(payment_id){
      
         var index =  failedPaymentIds.indexOf(""+payment_id);
         if(index >=0 ){
         failedPaymentIds.splice(index,1);
         //presence.del(""+payment_id);
        LOG("_______________________________Failed payment_id deleted = "+payment_id+ " at index = "+index);
         }
      }
 //const ssl_options = {
 //	key:  fs.readFileSync("/etc/letsencrypt/live/qm.play927.com/privkey.pem"),
 //    cert: fs.readFileSync("/etc/letsencrypt/live/qm.play927.com/fullchain.pem")
 //};
// Attach WebSocket Server on HTTP Server.
if (process.env.IS_CPU_CLUSTER_ON == 'true' && cluster.isMaster) {
    // This only happens on the master server
    const cpus = os.cpus().length;
  console.log("Total CPU Length "+cpus);
    for (let i = 0; i < 3; ++i) { 
        cluster.fork();
    }

} else {
  

  var gameServer = null;
  if(process.env.IS_CPU_CLUSTER_ON == 'true'){
    gameServer = new Server({
    pingTimeout:10000,
 //  server: createServer(ssl_options,app),   
 server: createServer(app),   
 //   presence: new RedisPresence(),
  presence: new MemsharedPresence(),
   verifyClient: function (info, next) {   
    
    var isClientValid = Origins.filter(c => c == info.origin).length == 1 ? true:false;
    if(!isClientValid){
     LOG_ERROR("---------------------------------ALERT::---------------------------------------- Verification of client info = "+JSON.stringify(info)+" isClientValid = "+isClientValid);
               }
     
    next (isClientValid); 
    // validate 'info'
    //
    // - next(false) will reject the websocket handshake
    // - next(true) will accept the websocket handshake
  }
    
});
     }else {
       gameServer = new Server({
       pingTimeout:10000,
  // server: createServer(ssl_options,app),   
 server: createServer(app),   
    presence: presence,
 //presence: new MemsharedPresence(),
   verifyClient: function (info, next) {   
    
    var isClientValid = Origins.filter(c => c == info.origin).length == 1 ? true:false;
    if(!isClientValid){
     LOG_ERROR("---------------------------------ALERT::---------------------------------------- Verification of client info = "+JSON.stringify(info)+" isClientValid = "+isClientValid);
               }
     
    next (isClientValid); 
    // validate 'info'
    //
    // - next(false) will reject the websocket handshake
    // - next(true) will accept the websocket handshake
  }
    
});

    presence.subscribe("ADD_PAYMENT_ID", addPaymentId);
    presence.subscribe("DEL_PAYMENT_ID", delPaymentId);
    presence.subscribe("ADD_FAILED_PAYMENT_ID", addFailedPaymentId);
    presence.subscribe("DEL_FAILED_PAYMENT_ID", delFailedPaymentId);
     }
 //gameServer.attach({ server: createServer(), presence:  new  RedisPresence() });
// Register GameRoom as "gameroom"
gameServer.register("gameroom", GameRoom);
// Register ChatRoom with initial options, as "chat_with_options"
// onInit(options) will receive client join options + options registered here.
// gameServer.register("chat_with_options", ChatRoom, {
//     custom_options: "you can use me on Room#onInit"
// });

// // Register StateHandlerRoom as "state_handler"
// gameServer.register("state_handler", StateHandlerRoom);

// // Register StateHandlerRoom as "state_handler"
// gameServer.register("auth", AuthRoom);

// // Register CreateOrJoin as "create_or_join"
// gameServer.register("create_or_join", CreateOrJoinRoom);

app.use('/', express.static(path.join(__dirname, "static")));
//app.use('/', serveIndex(path.join(__dirname, "static"), {'icons': true}))
// (optional) attach web monitoring panel
//app.use('/colyseus', monitor(gameServer));
   
gameServer.onShutdown(function(){
  database.closeAllDBConnection();
  LOG('game server is going down.');
});  
 gameServer.listen(port); 
LOG(`Listening on http://localhost:${ port }`); 
 }
export var dateTime = new DateTime();
export var database = new Database();

export const updateClientList= function(payment_id,isAdd){
  // console.log("totalServerClients before = ",Object.keys(totalServerClients).length);
         
  if(isAdd){
    //  if(isClientInRoom(payment_id)){
    // return true;
    // }
    
   totalServerClients[Object.keys(totalServerClients).length] = ""+payment_id;
     //console.log(payment_id+" updateClientList "+totalServerClients[Object.keys(totalServerClients).length-1]);
  
  }else{
    totalServerClients.splice(totalServerClients.indexOf(""+payment_id),1);
  } 
   //console.log("totalServerClients after = ",Object.keys(totalServerClients).length);
  
}
export const isClientInRoom= function(payment_id){
 for(var i=0,length = Object.keys(totalServerClients).length;i<Object.keys(totalServerClients).length;i++){
     // console.log(payment_id+" isClientInRoom "+totalServerClients[i]); 
if(totalServerClients[i].localeCompare(""+payment_id)>=1){   
      //  console.log(payment_id+" isClientInRoom ");
          return true;
    }  
 } 
  return false; 
} 
database.handleDisconnect();



//database.getQuestions(ques);
//var promiseGetQuestions = database.getQuestions();
//promiseGetQuestions.then(ques,ques);
 //     function ques(result){
   //     console.log("hi : "+ (JSON.stringify(result))); 
 //     }
/*
var message = "";



function getP1(){
return new Promise((resolve, reject) => {
    setTimeout(() => {
       console.log("promise1");
        message += "my";
        resolve("1");
    }, 2000)
});
} 
var promise1 = getP1()
promise1.then((result)=>{message += " hello";});
var promise2 = new Promise((resolve, reject) => {
 
    setTimeout(() => {
      console.log("promise2");
        message += " first";
        resolve("2");
    }, 2000)
});

var promise3 = new Promise((resolve, reject) => {

    setTimeout(() => {
      console.log("promise3");
        message += " promise";
        resolve("3");
    }, 2000)
}); 
var Result = (results) => {console.log("Results = ", results, "message = ", message);}
var Error = (Error) => {console.log("Error = ", Error);}
Promise.all([promise2, promise1, promise3]).then(Result,Error);
*/
// function randomInt(min,max) // min and max included
// {
//     return Math.floor(Math.random()*(max-min+1)+min);
// }
LOG(">>>>>>>>>>> Date = "+dateTime.getDateTime(process.env.DATE_LOCALE,{timeZone:process.env.DATE_TIMEZONE},'yyyy-MM-dd hh:mm:ss'));
console.log("QUE_IMAGE_PATH = "+process.env.QUE_IMAGE_PATH); 
console.log("POST_COIN_REWARD_URL = "+process.env.POST_COIN_REWARD_URL); 
console.log("POST_CASH_REWARD_URL = "+process.env.POST_CASH_REWARD_URL); 
console.log("POST_GAME_REWARD_URL = "+process.env.POST_GAME_REWARD_URL); 

/*
Request.post({
    "headers": { 'content-type' : 'application/x-www-form-urlencoded'},
    "url": process.env.POST_CASH_REWARD_URL,//"http://play927.com/test/test/check_post",
     "body":  "order_id=WCT_19_273_VS_MODE_4_312" 
}, (error, response, body) => {       
    if(error) {          
      console.log("error = ",error);
        return console.dir(error);
    }  
    console.log("body = ",(body));  
  //console.log("response = ",response);
});
*/
/*
Request.post({
    "headers": { "content-type": "application/json" },
    "url": "http://httpbin.org/post",
    "body": JSON.stringify({
        "firstname": "Nic",
        "lastname": "Raboy"
    })
}, (error, response, body) => {
    if(error) {
        return console.dir(error);
    }
    //console.dir(JSON.parse(body));
});*/

