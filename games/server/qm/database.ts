
import * as mysql from "mysql";
import * as promise from "promise";
import {  dateTime,LOG } from './server';
import  * as Request from "request";
require('dotenv').config(); 
var isHindiOn = false;
export class Database{

    db_config = {
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        port:process.env.DB_PORT,
        database: process.env.DB_DATABASE 
    };
//var mysql      = require('mysql'); 
 gamePrefix = "QM";  
closeAllDBConnection(){ 
  console.log('***************************  DB :: closeAllDBConnection ');
  this.dbconnection.end(function(err) {
  // The connection is terminated now
     console.log('The connection is terminated now: any error ? = '+err);
    });
  //this.dbconnection.destroy();
}  
handleDisconnect() { 

   // if(this.dbconnection){
   //   this.closeAllDBConnection();
   // }
    this.dbconnection = mysql.createConnection({
        host: process.env.DB_HOST, 
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        port:process.env.DB_PORT,
        database: process.env.DB_DATABASE  
    }); // Recreate the connection, since

    this.dbconnection.connect((err)  =>{              // The server is either down
        if(err) {                                     // or restarting (takes a while sometimes).
            console.log('error when connecting to db:', err); 
           setTimeout(this.handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
            return;
           
        }   
        console.log('Connected to db:'); // to avoid a hot loop, and to allow our node script to 

    

    });  


    // the old one cannot be reused.
    // process asynchronous requests in the meantime.
    // If you're also serving http, display a 503 error.
    this.dbconnection.on('error', (err) =>{
        console.log('db error', err);
        //if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
            this.handleDisconnect();                         // lost due to either server restart, or a
       // } else {                                      // connnection idle timeout (the wait_timeout
        //    throw err;                                  // server variable configures this)
        //}
    });

} 
 
validateToken(options) {

    return new promise((resolve,reject)=>{   
        LOG(" options.auth_token = "+options.auth_token + " options.user_id = "+options.user_id );
        ///*
        this.dbconnection.query("SELECT user_id as unique_reference ,mobile_number as mob_no FROM p_users where id = '"+options.user_id+"'" ,  (err, result, fields) => { //temp query need to remove
            if (err) {
                console.log("ERROR :: "+err); 
                resolve(err);
            }else{
                // result(JSON.parse(JSON.stringify(result)));
                LOG(" validateToken result "+(JSON.stringify(result))); 

                if(typeof(result) == 'undefined'|| typeof(JSON.parse(JSON.stringify(result))[0]) == 'undefined' ||   Object.keys(result).length == 0  ){
                    resolve({});
                  return;
                }

                var user_id = JSON.parse(JSON.stringify(result))[0].unique_reference
                var mob_no = JSON.parse(JSON.stringify(result))[0].mob_no

                if(typeof(user_id) == 'undefined' || typeof(mob_no) == 'undefined'){
                    resolve({});
                    return;
                }
                this.dbconnection.query("SELECT id FROM p_user_login_token where token = '"+options.auth_token +"' AND user_id = '"+user_id+"' LIMIT 0,1",  (err, result, fields) => {
                    if (err) {
                        console.log("ERROR :: "+err); 
                        resolve(err);
                    }else{ 
                        var _result = {};
                      LOG(" ----------------> p_user_login_token result "+(JSON.stringify(result))); 
                      if(typeof(JSON.parse(JSON.stringify(result))[0]) == 'undefined' ){
                         resolve(_result);
                         return;
                      }
                      
                      
                        if(Object.keys(result).length >= 1){
                            _result = [{"id":JSON.parse(JSON.stringify(result))[0].id,"mob_no":mob_no}];
                        } 

                        LOG(" ----------------> result "+(JSON.stringify(_result))); 
                        resolve(_result); 
                    }

                });    
            }

        });
        //  */
        /*

         this.dbconnection.query("SELECT id FROM p_user_login_token where token = '"+options.auth_token +"' AND user_id = '"+options.user_id+"'",  (err, result, fields) => {
           if (err) {
           console.log("ERROR :: "+err); 
           reject(err);
           }else{
                // result(JSON.parse(JSON.stringify(result)));
              LOG(" result "+(JSON.stringify(result))); 
             resolve(result); 
           }

       });

        */

    });


} 
getUserInfo(options){ 

    return new promise((resolve,reject)=>{  

        this.dbconnection.query("select level,points from qm_user_levels where tour_id = "+options.tour_id+" AND user_id = "+options.user_id+";",  (err, result, fields) => {
            if (err) {
                console.log("getUserInfo ERROR :: "+err);
                resolve(err);
            }else{
                LOG("getUserInfo SUCCESS :: "+JSON.stringify(result));
                resolve(result);             

            }            

        });
    });


}
getUserAvatar(options){
return new promise((resolve,reject)=>{  

        this.dbconnection.query("SELECT avatar_image FROM p_users as U INNER JOIN avatar_master as AM ON AM.id = U.avatar_id WHERE U.id = "+options.user_id+" UNION SELECT 'default_value_if_no_record' LIMIT 0,1; ",  (err, result, fields) => {
            if (err) {
                console.log("getUserAvatar ERROR :: "+err);
                resolve(err);
            }else{
                LOG("getUserAvatar SUCCESS :: "+JSON.stringify(result));
                resolve(result);             

            }            

        });
    });
}  
  
getGameConfig(options){

    return new promise((resolve,reject)=>{  

        this.dbconnection.query("SELECT is_free,is_fixed_price,amount, bot_available FROM qm_config_table where id = "+options.tour_id+";",  (err, result, fields) => {
            if (err) {
                console.log("getGameConfig ERROR :: "+err);
                resolve(err);
            }else{
                LOG("getGameConfig SUCCESS :: "+JSON.stringify(result)); 

                resolve(result);             

            }            

        });
    });
}
 getTourGameConfig(options){

    return new promise((resolve,reject)=>{  

        this.dbconnection.query("select reward_id as winning_id, amount , kyc_required as kyc from p_tournament_rewards as ptr INNER JOIN qm_game_levels as qgm on ptr.id = qgm.reward_id where tour_id = "+options.tour_id+" AND level = "+options.level+";",  (err, result, fields) => {
            if (err) {
                console.log("getTourGameConfig ERROR :: "+err);
                resolve(err);
            }else{
                LOG("getTourGameConfig SUCCESS :: "+JSON.stringify(result)); 

                resolve(result);             

            }            

        });
    });
} 
    getBotConfig(options){

    return new promise((resolve,reject)=>{  

        this.dbconnection.query("SELECT * FROM bot_config where tour_id = "+options.tour_id+" AND level_id = "+options.level+";",  (err, result, fields) => {
            if (err) {
                console.log("DB:: get...........Config ERROR :: "+err);
                resolve(err);
            }else{
                LOG("DB::get...........Config SUCCESS :: "+JSON.stringify(result)); 

                resolve(result);             

            }            

        });
    });
}
  
getUserLevelAndPoints(options){ 

    return new promise((resolve,reject)=>{  

        this.dbconnection.query("select level,points,total_points from qm_user_levels where tour_id = "+options.tour_id+" AND user_id = "+options.user_id+";",  (err, result, fields) => {
            if (err) {
                console.log("getUserLevelAndPoints ERROR :: "+err);
                resolve(err);
            }else{
                LOG("getUserLevelAndPoints SUCCESS :: "+JSON.stringify(result));
                if(Object.keys(result).length == 0){

                    // need to remove insert query after payment success flow complete  resolve([{"points":-1,"level":-1}]); 

                    this.dbconnection.query(" INSERT INTO qm_user_levels ( user_id , points, level,tour_id) VALUES ( "+options.user_id+",0,0,"+options.tour_id+");",  (err, result, fields) => {
                        if (err) {
                            console.log("getUserLevelAndPoints Insert  ERROR :: "+err);
                            resolve(err);
                        }else{
                            LOG("getUserLevelAndPoints Insert   SUCCESS :: "+JSON.stringify(result));


                            resolve([{"points":0,"level":0,"total_points":0}]);                       
                        }            

                    });

                }else{
                    LOG(JSON.stringify(fields)+"fields ==> "+fields);
                    resolve(result); 
                }


            }            

        });
    });


}

setUserLevelAndPoints(data){

    return new promise((resolve,reject)=>{

        this.dbconnection.query("UPDATE qm_user_levels SET points = "+data.points+" , total_points = "+data.total_points+",  level = "+data.level+" where tour_id = "+data.tour_id+" AND user_id = "+data.user_id+" ;",  (err, result, fields) => {
     if (err) {
                console.log("setUserLevelAndPoints ERROR :: "+err);
                resolve(err);
            }else{
                LOG("setUserLevelAndPoints SUCCESS :: ",data); 
                resolve(data);    
               
              //  var promise =  this.updateUserLevel(data);
               // promise.then((result)=>{resolve(result);},(error)=>{reject(error);});
              
            }            

        });
    });
}

updateUserLevel(data){

    return new promise((resolve,reject)=>{

        // this.dbconnection.query(" SELECT points_required FROM qm_game_levels where tour_id = "+data.tour_id+" AND  level <= "+data.level+" order by level desc limit 0,1;",  (err, result, fields) => {
        //   if (err) {
        //   console.log("updateUserLevel get level ERROR :: "+err);
        //   reject(err);
        //   }else{
        //      LOG("updateUserLevel get level  SUCCESS :: "+JSON.stringify(result));
        //resolve(result);  
        var  points_required = data.points_required;
        LOG("points_required => ",points_required);
        var total_points = data.total_points ;
        var currentLevel = data.level;//JSON.parse(JSON.stringify(result))[0].level;
        var points = data.points;
        if(data.tour_lives<=0){
          total_points = 0;
        }else{
          total_points += data.win_points;
        }        
        if(points >= points_required){
            currentLevel++;
            points = 0;
           this.addAmountToUserWallet(data);
        } 
        LOG(">>>>>>>>>>>>>>>>>>>>>>>>> data.level "+data.level+"  currentLevel = "+currentLevel+" total_points = "+total_points+" points = "+points+"");
        Object.assign(data,{"level":currentLevel});   

        this.dbconnection.query("UPDATE qm_user_levels SET points = "+points+" , total_points = "+total_points+",  level = "+currentLevel+" where tour_id = "+data.tour_id+" AND user_id = "+data.user_id+" ;",  (err, result, fields) => {

            if (err) {
                console.log("updateUserLevel  ERROR :: "+err);
                resolve(err);
            }else{
                LOG("updateUserLevel SUCCESS :: "+JSON.stringify(result));
                resolve(data);    

            }            
        });


        //}            

        //});
    });

} 
getLevelData(data){
    return new promise((resolve,reject)=>{

        this.dbconnection.query("SELECT win_points ,lives, ans_score ,points_required FROM qm_game_levels WHERE level = '"+data.level+"' AND tour_id = '"+data.tour_id+"'",  (err, result, fields) => {
            if (err) {
                console.log("B:: getLevelData ERROR :: "+err);
                resolve(err);
            }else{
                LOG("DB:: getLevelData Success "+JSON.stringify(result) ); 
                resolve(result); 
            }

        });

    });
} 
 
getUserTourLives(data){
    return new promise((resolve,reject)=>{
 this.dbconnection.query("SELECT tour_lives FROM qm_tbl_payment WHERE status = 0 AND  tour_id = "+data.tour_id+" AND user_id = "+data.user_id+" AND payment_id = '"+data.payment_id+"' order by id desc limit 0,1",  (err, result, fields) => {
       
        //this.dbconnection.query("SELECT tour_lives FROM qm_tbl_payment WHERE  status = 0 AND tour_id = "+data.tour_id+" AND user_id = "+data.user_id+" limit 0,1",  (err, result, fields) => {
            if (err) {
                console.log("B:: getUserTourLives ERROR :: "+err);
                resolve(err);
            }else{
                LOG("DB:: getUserTourLives Success "+JSON.stringify(result) ); 
                
                resolve(result); 
            }

        });

    });
}

createGameRoom(options){


    return new promise((resolve,reject)=>{

        this.dbconnection.query("INSERT INTO qm_game_tbl_que (level, player_count, status ,tour_id,game_type,created_date) VALUES ('"+options.level+"', '1', '0','"+options.tour_id+"','"+options.game_type+"','"+dateTime.getDateTime(process.env.DATE_LOCALE,{timeZone:process.env.DATE_TIMEZONE},'yyyy-MM-dd hh:mm:ss')+"');",  (err, result, fields) => {
            if (err) {
                console.log("ERROR :: "+err);
                resolve(err);
            }else{
                //resolve(result);            
                var promise =   this.createUserTable({"user_id":options.user_id,"level":options.level,"game_id":JSON.parse(JSON.stringify(result)).insertId,"lives":3,"player_count":1});
                promise.then((result)=>{resolve(result);},(error)=>{resolve(error);});


            }


        });
    });

}
createUserTable(data){

    return new promise((resolve,reject)=>{ 

        var table_name = 'qm_live_game_user_'+data.game_id;
        LOG("tablename  "+table_name); 

        this.dbconnection.query("SHOW TABLES LIKE '"+table_name+"' ;",  (err, result, fields) => { 
            if (err) {
                console.log("show table =>Error "+err);
                resolve(err);
            }else{
                LOG("show table =>Success "+JSON.stringify(result) );
                this.dbconnection.query("CREATE TABLE "+table_name+" (id INT NOT NULL AUTO_INCREMENT ,game_id INT NULL DEFAULT 0, user_id INT NULL DEFAULT 0,level INT NULL DEFAULT 0,lives INT NULL DEFAULT 3,status INT NULL DEFAULT 0,ans_score INT NULL DEFAULT 0, PRIMARY KEY (id)) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;",  (err, result, fields) => {
                    if (err) {
                        console.log("createUserTable =>Error "+err);
                        resolve(err);
                    }else{
                        LOG("createUserTable =>Success "+JSON.stringify(result) );
                        //resolve(result); 
                        var   promise =   this.createQuestionTable(data);                                                      
                        promise.then((result)=>{resolve(result);},(error)=>{resolve(error);});
                    }

                });



            }

        });

    });

} 
createQuestionTable(data){

    return new promise((resolve,reject)=>{

        var table_name = 'qm_live_game_user_'+data.game_id+"_questions";

        this.dbconnection.query(" CREATE TABLE "+table_name+"("
                                +"id int(11) NOT NULL AUTO_INCREMENT,"
                                +"game_id int(11) NULL DEFAULT NULL,"
                                +"user_id int(11) NULL DEFAULT NULL,"
                                +"que_id int(11) NULL DEFAULT NULL,"  
                                +"ans_id int(11) NULL DEFAULT NULL,"
                                +"ans_submit varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,"                                   
                                +"ans_time int(11) NULL DEFAULT NULL,"
                                +"ans_status int(11) NULL DEFAULT NULL,"
                                +"lives int(255) NULL DEFAULT NULL,"
                                +" PRIMARY KEY (id) USING BTREE"
                                +") ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;",  (err, result, fields) => {
            if (err) {
                console.log("DB:: createQuestionTable ERROR :: "+err);
                resolve(err);
            }else{
                LOG("DB:: createQuestionTable Success "+JSON.stringify(result) ); 
                //resolve(result); 
                var   promise =    this.updateTableGameQue(data);
                promise.then((result)=>{resolve(result);},(error)=>{resolve(error);});
            }

        });

    });


} 


updateTableGameQue(data){

    return new promise((resolve,reject)=>{
        //LOG((JSON.stringify(data)));
        var table_name = 'qm_live_game_user_'+data.game_id;

        this.dbconnection.query("UPDATE qm_game_tbl_que SET user_table_name = '"+table_name+"' ,question_table_name = '"+table_name+"_questions'  WHERE id = "+data.game_id+";",  (err, result, fields) => {
            if (err) {
                console.log("updateTableGameQue =>ERROR "+err); 
                resolve(err);
            }else{
                LOG("updateTableGameQue =>Success "+JSON.stringify(result) ); 
                resolve(data); 
                //var   promise =    this.getQuestionsFromMaster(data);
                // promise.then((result)=>{resolve(result);});
            }


        });
    });
}  

getQuestionsFromMaster(data){ 

    return new promise((resolve,reject)=>{ 
       var queIds = [];
        var queData_en = [];
        var queData_hi = [];
        var noOfTimes = 3;
        var  maxLimit = 5;
        var successCount = 0;
        var questions = {en:{},hi:{}};
        
       for(var i=0;i<noOfTimes;i++){

             var queLevel = ""; 
         
//             switch(i){
//                 case 0:
//                     queLevel = "que_level >= 0 AND que_level <= 1";
//                     break;
//                 case 1:
//                     queLevel = "que_level >= 2  AND que_level <= 3";
//                     break;
//                 case 2:
//                     queLevel = "que_level > 3 ";
//                     break;
//               default:
//                 queLevel = "que_level > 3 ";
//                 break
//             }
        // switch(data.masterRefillCount){
        //    case 1:
        //     queLevel = "que_level >= 0 AND que_level < 1";
        //     break;
        //     case 2:
        //     case 3:
        //      queLevel = "que_level >= 1 AND que_level < 2";
        //     break;
        //     case 4:
        //     queLevel = "que_level >= 2 AND que_level < 3";
        //     break;
        //     case 5:
        //     queLevel = "que_level >= 3 AND que_level < 4";
        //     break;
        //   default:
        //      queLevel = "que_level > 3 ";
        //     break;
        //        }
        switch(data.masterRefillCount){
           case 1:
            queLevel = "que_level >= 0 AND que_level <= 2";
            break;
            case 2:            
             queLevel = "que_level >= 1 AND que_level <= 3";
            break;
            case 3:
            queLevel = "que_level >= 2 AND que_level <= 4";
            break;
           default:
             queLevel = "que_level >= 3 ";
            break;
               }

//             if(data.masterRefillCount >= 2){
//                 queLevel = "que_level > 3 ";
//             }
        
        LOG("***************************** queLevel =>"+queLevel);
        //AND que_type = 0
        var queIdQuery = ""; 
              if(isHindiOn){
                 queIdQuery = "select que_id as id FROM test_question_db where 1 AND is_delete = 0 AND status = 1 AND "+queLevel+"  AND que_language = 'Hindi' order by count,rand() limit 0,"+maxLimit+";";
                 }else{
                  queIdQuery = "select que_id as id FROM test_question_db where 1 AND is_delete = 0 AND status = 1 AND "+queLevel+" order by count,rand() limit 0,"+maxLimit+";";
                 }
        
            this.dbconnection.query(queIdQuery,  (err, result, fields) => {

                if (err) {
                    console.log("DB:: get que_id From Master ERROR :: "+err);
                    resolve(err);
                }else{   
                    LOG("DB:: get que_id From Master Success "+JSON.stringify(result) ); 
                    successCount++; 
                    for(var count = 0,parseResult = JSON.parse(JSON.stringify(result)),length = parseResult.length;count<length;count++){
                        queIds= queIds.concat(parseResult[count]); 
                    }
                    if(successCount == (noOfTimes) ){
                      successCount = 0;
                      for(var j =0;j<queIds.length;j++){ 
                        
                        var que_id = queIds[j].id;
                        //For English Questions
                             this.dbconnection.query("select que_level ,que_id as id,que_type as type,que_text as question,option_1 as opt_1,option_2 as opt_2,option_3 as opt_3,option_4 as opt_4,answer as ans_id,content_name as image_path FROM test_question_db where que_id = "+que_id+";",  (err, result, fields) => {

                                      if (err) {
                                          console.log("DB::English getQuestionsFromMaster ERROR :: "+err);
                                          resolve(err);
                                      }else{
                                         // LOG("DB:: English getQuestionsFromMaster Success "+JSON.stringify(result) );                                          
                                          for(var count = 0,parseResult = JSON.parse(JSON.stringify(result)),length = parseResult.length;count<length;count++){
                                              queData_en= queData_en.concat(parseResult[count]); 
                                          }    
                                          if(!isHindiOn){
                                                                   successCount++;
                                                                 if(successCount == (queIds.length) ){    
                                                                     questions.en = JSON.parse(JSON.stringify(queData_en)); 
                                                                     questions.hi = JSON.parse(JSON.stringify(queData_en));
                                                                     
                                                                     LOG("DB:: note Hindi language is off ::Questions length Hindi  =  "+JSON.parse(JSON.stringify(questions.en )).length ); 
                                                                     LOG("DB:: Questions length English =  "+JSON.parse(JSON.stringify(questions.hi)).length ); 
                                                                      resolve(questions);  
                                                                  }     
                                          }
                                      }

                                  });
                       
                          //For Hindi questions
                        if(isHindiOn){
                                                this.dbconnection.query("select que_id as id,que_text as question,option_1 as opt_1,option_2 as opt_2,option_3 as opt_3,option_4 as opt_4 FROM test_question_hindi_db where  que_id = "+que_id+";",  (err, result, fields) => {

                                                              if (err) { 
                                                                  console.log("DB::Hindi getQuestionsFromMaster ERROR :: "+err);
                                                                  resolve(err);
                                                              }else{
                                                                 // LOG("DB:: Hindi getQuestionsFromMaster Success "+JSON.stringify(result) ); 
                                                                  successCount++;
                                                                  for(var count = 0,parseResult = JSON.parse(JSON.stringify(result)),length = parseResult.length;count<length;count++){
                                                                      queData_hi= queData_hi.concat(parseResult[count]); 
                                                                  }
                                                                  if(successCount == (queIds.length) ){    
                                                                     questions.en = JSON.parse(JSON.stringify(queData_en)); 
                                                                      questions.hi = JSON.parse(JSON.stringify(queData_hi));
                                                                     
                                                                     LOG("DB:: Questions length Hindi =  "+JSON.parse(JSON.stringify(questions.en )).length ); 
                                                                     LOG("DB:: Questions length English =  "+JSON.parse(JSON.stringify(questions.hi)).length ); 
                                                                      resolve(questions);  
                                                                  }                                        
                                                              }
                                                          });    

                                            }
                        
                        
                              }     
                      
                        //resolve(questions);  
                    }
                    //var   promise =    this.fillQuestionsTable(data,result);
                    //promise.then((result)=>{resolve(result);});
                }

            });

        }
      
//         for(var i=0;i<noOfTimes;i++){

//             var queLevel = ""; 
//             switch(i){
//                 case 0:
//                     queLevel = "que_level > 0 AND que_level <= 1";
//                     break;
//                 case 1:
//                     queLevel = "que_level >= 2  AND que_level <= 3";
//                     break;
//                 case 2:
//                     queLevel = "que_level > 3 ";
//                     break;
//               default:
//                 queLevel = "que_level > 3 ";
//                 break
//             }

//             if(data.masterRefillCount >= 2){
//                 queLevel = "que_level > 3 ";
//             }
//             this.dbconnection.query("select que_id as id,que_type as type,que_text as question,option_1 as opt_1,option_2 as opt_2,option_3 as opt_3,option_4 as opt_4,answer as ans_id,content_name as image_path FROM question_db where 1 AND "+queLevel+" AND que_type = 0 order by count,rand() limit 0,"+maxLimit+";",  (err, result, fields) => {

//                 if (err) {
//                     console.log("DB:: getQuestionsFromMaster ERROR :: "+err);
//                     reject(err);
//                 }else{
//                     LOG("DB:: getQuestionsFromMaster Success "+JSON.stringify(result) ); 
//                     successCount++;
//                     for(var count = 0,parseResult = JSON.parse(JSON.stringify(result)),length = parseResult.length;count<length;count++){
//                         queData= queData.concat(parseResult[count]); 
//                     }
//                     if(successCount == (noOfTimes) ){
//                         questions.en = JSON.parse(JSON.stringify(queData));
//                         questions.hi = JSON.parse(JSON.stringify(queData));
//                         resolve(questions);  
//                     }
//                     //var   promise =    this.fillQuestionsTable(data,result);
//                     //promise.then((result)=>{resolve(result);});
//                 }

//             });

//         }


    });
 
} 

fillQuestionsTable(data,questions){

    return new promise((resolve,reject)=>{ 
        // LOG((JSON.stringify(data)));
        var table_name = 'qm_live_game_user_'+data.game_id+"_questions";

        for(var id =0 ;id<Object.keys(questions).length;id++){
            // LOG(""+result[id].id ); 

            this.dbconnection.query("INSERT INTO "+table_name+" (game_id, question_id) VALUES ("+data.game_id+", "+questions[id].id+" );",  (err, result, fields) => {
                if (err) {
                    console.log("fillQuestionsTable =>Error "+err);
                    resolve(err);
                }else{
                    LOG("fillQuestionsTable =>Success "+JSON.stringify(result) );
                    if(id == JSON.parse(JSON.stringify(result)).insertId ){
                        resolve(data);  
                    }
                }

            });

        } 


    });


}




joinUserToGameRoom(data){

    return new promise((resolve,reject)=>{
         LOG((JSON.stringify(data)));

        this.dbconnection.query("UPDATE qm_game_tbl_que SET player_count = '"+data.player_count+"' WHERE id = "+data.game_id+";",  (err, result, fields) => {
            if (err) {
                console.log("ERROR :: "+err);
                console.log("joinUserToGameRoom =>ERROR "+err);  
                resolve(err);
            }else{
                LOG("joinUserToGameRoom =>Success "+JSON.stringify(result) ); 

                this.insertToUserTable(data);
                this.setUserGameHistory(data);
                resolve(result); 
            }


        });
    });
}

insertToUserTable(data){ 
    return new promise((resolve,reject)=>{  
        // LOG((JSON.stringify(data)));
        var table_name = 'qm_live_game_user_'+data.game_id;

        this.dbconnection.query("INSERT INTO "+table_name+" (game_id, user_id, level, lives ,ans_score, status) VALUES ("+data.game_id+", "+data.user_id+", "+data.level+","+data.lives+","+data.ans_score+",1);",  (err, result, fields) => {
            if (err) {
                console.log("insertToUserTable =>Error "+err);
                resolve(err);
            }else{
                LOG("insertToUserTable =>Success "+JSON.stringify(result) );
                resolve(result); 

            }

        });
    });
}

updateUserTable(data){ 
    return new promise((resolve,reject)=>{ 
        // LOG((JSON.stringify(data)));
        var table_name = 'qm_live_game_user_'+data.game_id;
        // TODO:ans_score = "+data.ans_score
        this.dbconnection.query("UPDATE "+table_name+" SET lives = "+data.lives+",ans_score = "+data.ans_score+",status = "+data.status+" WHERE user_id = "+data.user_id+";",  (err, result, fields) => {
            if (err) {
                console.log("updateUserTable =>Error "+err);
                resolve(err);
            }else{
                LOG("updateUserTable =>Success "+JSON.stringify(result) );
                resolve(result); 

            }

        });
    });
}

addUserToWinnerList(data){

    return new promise((resolve,reject)=>{ 
        // LOG((JSON.stringify(data)));

        this.dbconnection.query("INSERT INTO qm_winner_list (game_id, user_id, level , points ,date_time,tour_id) VALUES ("+data.game_id+", "+data.user_id+", "+data.level+","+data.points+",'"+dateTime.getDateTime(process.env.DATE_LOCALE,{timeZone:process.env.DATE_TIMEZONE},'yyyy-MM-dd hh:mm:ss')+"',"+data.tour_id+");",  (err, result, fields) => {
            if (err) {
                console.log("addUserToWinnerList =>Error "+err);
                resolve(err);
            }else{
                LOG("addUserToWinnerList =>Success "+JSON.stringify(result) );
                resolve(result); 

            }
        });
    });

}


setUserGameHistory(data){

    return new promise((resolve,reject)=>{
        LOG("setUserGameHistory data = ", ("INSERT INTO qm_game_play_history (game_id, level,  user_id, status ,date_time,tour_id,payment_id) VALUES ("+data.game_id+", "+data.level+", "+data.user_id+", '1','"+dateTime.getDateTime(process.env.DATE_LOCALE,{timeZone:process.env.DATE_TIMEZONE},'yyyy-MM-dd hh:mm:ss')+"',"+data.tour_id+",'"+data.payment_id+"');"));
        var nDate = new Date().toLocaleString('en-US', { timeZone: 'Asia/Kolkata' });

        this.dbconnection.query("INSERT INTO qm_game_play_history (game_id, level,  user_id, status ,date_time,tour_id,payment_id) VALUES ("+data.game_id+", "+data.level+", "+data.user_id+", '1','"+dateTime.getDateTime(process.env.DATE_LOCALE,{timeZone:process.env.DATE_TIMEZONE},'yyyy-MM-dd hh:mm:ss')+"',"+data.tour_id+",'"+data.payment_id+"');",  (err, result, fields) => {
            if (err) {
                console.log("ERROR :: "+err);
                console.log("setUserGameHistory =>ERROR "+err); 
                resolve(err);
            }else{
                LOG("setUserGameHistory =>Success "+JSON.stringify(result) ); 
                resolve(result); 

            }


        });
    });
}
updateUserGameHistory(data){

    return new promise((resolve,reject)=>{
        // LOG((JSON.stringify(data)));


        this.dbconnection.query("UPDATE qm_game_play_history SET status = 2 , date_time = '"+dateTime.getDateTime(process.env.DATE_LOCALE,{timeZone:process.env.DATE_TIMEZONE},'yyyy-MM-dd hh:mm:ss')+"' WHERE user_id = "+data.user_id+" AND game_id = "+data.game_id+";",  (err, result, fields) => {
            if (err) {
                console.log("ERROR :: "+err);
                console.log("updateUserGameHistory =>ERROR "+err); 
                resolve(err);
            }else{
                LOG("updateUserGameHistory =>Success "+JSON.stringify(result) ); 
                resolve(result); 

            }


        });
    });
}

startGame(data){

    return new promise((resolve,reject)=>{

        this.dbconnection.query("UPDATE qm_game_tbl_que SET status = 1 , start_time = '"+dateTime.getDateTime(process.env.DATE_LOCALE,{timeZone:process.env.DATE_TIMEZONE},'yyyy-MM-dd hh:mm:ss')+"' WHERE tour_id = "+data.tour_id+" AND id = "+data.game_id+";",  (err, result, fields) => {
            if (err) {
                console.log("DB:: startGame ERROR :: "+err);
                resolve(err);
            }else{
                LOG("DB:: startGame Success "+JSON.stringify(result) ); 
                resolve(result); 
            }

        });

    });
}
getQuestions(data) {


    return new promise((resolve,reject)=>{
        var table_name = 'qm_live_game_user_'+data.game_id+"_questions";
        LOG("getQuestions =>> table_name = "+table_name);
        this.dbconnection.query("SELECT dqm.id as col_id, que_id as id,que_type as type,que_text as question,option_1 as opt_1,option_2 as opt_2,option_3 as opt_3,option_4 as opt_4,answer as ans_id,content_name as img_path FROM test_question_db as qdb INNER JOIN "+table_name+" as dqm ON qdb.que_id = dqm.question_id where que_type = 1 AND dqm.id  >"+data.col_id+" limit 0,5",  (err, result, fields) => {
            // this.dbconnection.query("SELECT dqm.id as col_id, que_id as id,que_type as type,que_text as question,option_1 as opt_1,option_2 as opt_2,option_3 as opt_3,option_4 as opt_4,answer as ans_id FROM question_db as qdb INNER JOIN "+table_name+" as dqm ON qdb.que_id = dqm.question_id where dqm.id  >"+data.col_id+" limit 0,5",  (err, result, fields) => {
            if (err) {
                console.log("B:: getQuestions ERROR :: "+err);
                resolve(err);
            }else{ 
                LOG("DB:: getQuestions Success "+JSON.stringify(result) ); 
                resolve(result); 
            }

        });

    });
} 

updateQuestionServeCount(data) {       

    return new promise((resolve,reject)=>{

        this.dbconnection.query("UPDATE test_question_db set count = count + 1 where que_id = "+data.que_id,  (err, result, fields) => {
            if (err) {
                console.log("B:: updateQuestionServeCount ERROR :: "+err);
                resolve(err);
            }else{
                LOG("DB:: updateQuestionServeCount Success "+JSON.stringify(result) ); 
                resolve(result); 
            }

        });

    });
} 

updateLivesCount(data) {        

    return new promise((resolve,reject)=>{
        var status = (data.tour_lives <= 0?1:0);
      LOG("DB :: updateLivesCount Data = ",data);
        this.dbconnection.query("UPDATE qm_tbl_payment SET tour_lives = "+data.tour_lives+",status = "+status+" WHERE status = 0 and tour_id = "+data.tour_id+" and user_id = "+data.user_id+" AND payment_id = '"+data.payment_id+"';",  (err, result, fields) => {
            if (err) {
                console.log("B:: updateTourLifeCount ERROR :: "+err);
                resolve(err);
            }else{
                LOG("DB:: updateTourLifeCount Success "+JSON.stringify(result) ); 
               // if(status >= 1){
               // this.resetUserTournamentData(data);
               //  }
                resolve(result); 
              
               
            }

        });

    });
} 
  
resetUserTournamentData(data){
   return new promise((resolve,reject)=>{
     
     var query = "UPDATE qm_user_levels SET points = 0 , total_points = 0,  level = 0 where tour_id = "+data.tour_id+" AND user_id = "+data.user_id+" ;";
     LOG("resetUserTournamentData = ",query);
            this.dbconnection.query(query,  (err, result, fields) => {

                    if (err) {
                        console.log("resetUserTournamentData  ERROR :: "+err);
                        resolve(err);
                    }else{
                        LOG("resetUserTournamentData SUCCESS :: "+JSON.stringify(result));
                        resolve(data);    

                    }            
                });
       });
}  



updateUserConnectStatus(data){
}

saveLog(data) {

    return new promise((resolve,reject)=>{



        this.dbconnection.query("UPDATE qm_game_tbl_que SET status = 2 , game_state = '"+data.game_state+"' , end_time = '"+dateTime.getDateTime(process.env.DATE_LOCALE,{timeZone:process.env.DATE_TIMEZONE},'yyyy-MM-dd hh:mm:ss')+"' WHERE id = "+data.game_id+";",  (err, result, fields) => {
            if (err) {
                console.log("DB:: saveLog ERROR :: "+err);
                resolve(err);
            }else{
                LOG("DB:: saveLog Success "+JSON.stringify(result) ); 
                //resolve(result); 
            }

        });


        var table_name = 'qm_live_game_user_'+data.game_id+"_questions";

        for(var i =0 ;i<Object.keys(data.room_log).length;i++){
            LOG("  values to Save"+data.game_id+", "+data.room_log[i].user_id+", "+data.room_log[i].que_id+","+data.room_log[i].ans_id+","+data.room_log[i].ans_submit+","+data.room_log[i].ans_status+","+data.room_log[i].ans_time+","+data.room_log[i].lives ); 

            this.updateUserGameHistory({"game_id":data.game_id,"user_id":data.room_log[i].user_id});


            this.dbconnection.query("INSERT INTO "+table_name+" (game_id ,user_id, que_id ,ans_id ,ans_submit,ans_status,ans_time,lives) VALUES ("+data.game_id+", "+data.room_log[i].user_id+", "+data.room_log[i].que_id+",'"+data.room_log[i].ans_id+"','"+data.room_log[i].ans_submit+"',"+data.room_log[i].ans_status+","+data.room_log[i].ans_time+","+data.room_log[i].lives+" );",  (err, result, fields) => {
                if (err) {
                    console.log("saveLog "+table_name+" =>Error "+err);
                    resolve(err);
                }else{
                    LOG("saveLog "+table_name+" =>Success "+JSON.stringify(result) );
                    if(i == JSON.parse(JSON.stringify(result)).insertId ){
                        //resolve(data);  
                    }
                }

            });

        }     



    });


} 

saveExistingUserJoinRequest(data){
    return new promise((resolve,reject)=>{
       
         this.dbconnection.query("INSERT INTO invalid_game_access (user_id,client_id,session_id,game_id,game_type,date_time) VALUES ('"+data.user_id+"','"+data.client_id+"','"+data.session_id+"','"+data.game_id+"','"+data.game_type+"','"+dateTime.getDateTime(process.env.DATE_LOCALE,{timeZone:process.env.DATE_TIMEZONE},'yyyy-MM-dd hh:mm:ss')+"');",  (err, result, fields) => {
            if (err) {
                console.log("DB:: saveExistingUserJoinRequest ERROR :: "+err);
                resolve(err);
            }else{
                LOG("DB:: saveExistingUserJoinRequest Success "+JSON.stringify(result) ); 
                resolve(result); 
            }

        });
    });

}
  setUserConnectStatus(data){
     console.log(">>>>>>>>>>>>>>>>>>>>>>>setUserConnectStatus>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+JSON.stringify(data));

    return new promise((resolve,reject)=>{
 
   
        this.dbconnection.query("INSERT INTO user_connect_status (user_id,client_id,session_id,game_id,game_type,date_time,connect_status,room_id) VALUES ('"+data.user_id+"','"+data.clientId+"','"+data.sessionId+"','"+data.game_id+"','"+data.game_type+"','"+dateTime.getDateTime(process.env.DATE_LOCALE,{timeZone:process.env.DATE_TIMEZONE},'yyyy-MM-dd hh:mm:ss')+"','"+data.connect_status+"','"+data.roomId+"');",  (err, result, fields) => {
            if (err) {
                console.log("DB:: setUserConnectStatus ERROR :: "+err);
                resolve(err);
            }else{
                LOG("DB:: setUserConnectStatus Success "+JSON.stringify(result) ); 
                resolve(result); 
            }

        });
    });

}

updateCoinRewards(data){  //coin Rewards 

    return new promise((resolve,reject)=>{
      
      if(data.user_id <= 0){
        resolve("user is bot"); 
         LOG("DB:: updateCoinRewards  user is bot"); 
        return true;
      }

        this.dbconnection.query("INSERT INTO qm_coin_rewards(user_id, game_id, amount,issued_status, created_date) VALUES ("+data.user_id+","+data.game_id+","+data.ans_score+", 0,'"+dateTime.getDateTime(process.env.DATE_LOCALE,{timeZone:process.env.DATE_TIMEZONE},'yyyy-MM-dd hh:mm:ss')+ "')",  (err, result, fields) => {
            if (err) {
                console.log("DB:: INSERT updateCoinRewards ERROR :: "+err);
                resolve(err);
            }else{
                LOG("DB:: INSERT updateCoinRewards Success "+JSON.stringify(result) ); 
                //resolve(result); 
                var insertId =  JSON.parse(JSON.stringify(result)).insertId;
                var order_id = "QMR_"+data.user_id+"_"+data.game_id+"_"+insertId+"_"+data.ans_score;
                this.dbconnection.query("UPDATE qm_coin_rewards SET order_id = '"+order_id+"',issued_status = 1 where id = "+insertId+" ;",  (err, result, fields) => {
                    if (err) {
                        console.log("DB::UPDATE qm_coin_rewards ERROR :: "+err);
                        resolve(err);
                    }else{
                        LOG("DB:: UPDATE qm_coin_rewards Success "+JSON.stringify(result) ); 
                       //this.postData(process.env.POST_COIN_REWARD_URL,"order_id="+order_id);
                      this.postData(process.env.POST_COIN_REWARD_URL,"order_id="+order_id+"&mobile_number="+data.mob_no+"&token="+data.auth_token+"&tour_id="+data.tour_id);
                  
                        resolve(result); 
                    }

                });


            }

        });

    });
}

addAmountToUserWallet(data){//cash rewards

    return new promise((resolve,reject)=>{

        LOG("addAmountToUserWallet data = ",data);
        var category = "won";
        var amount = data.win_amount;
        var issued_status = "0";
 if(data.user_id == '-2'){
        resolve("user is bot"); 
        LOG("addAmountToUserWallet : user is bot ==> ");
        return true;
      }
        this.dbconnection.query("INSERT INTO playcash_rewards (category,amount,issued_status,create_date) VALUES ('"+category+"','"+amount+"','"+issued_status+"','"+dateTime.getDateTime(process.env.DATE_LOCALE,{timeZone:process.env.DATE_TIMEZONE},'yyyy-MM-dd hh:mm:ss')+"');",  (err, result, fields) => {
            if (err) {
                console.log("DB:: INSERT addAmountToUserWallet ERROR :: "+err);
                resolve(err);
            }else{
                LOG("DB:: INSERT addAmountToUserWallet Success "+JSON.stringify(result) ); 
                //resolve(result); 
                var insertId =  JSON.parse(JSON.stringify(result)).insertId;
                var order_id = "WCT_"+data.user_id+"_"+data.game_id+"_"+data.game_type+"_"+data.tour_id+"_"+insertId;//WCT_UserId_GameId_Mode_TourId_AI
                LOG("order_id ==> "+order_id+"  insertId = ",insertId);    
                this.dbconnection.query("UPDATE playcash_rewards SET order_id = '"+order_id+"',issued_status = 1 where id = "+insertId+" ;",  (err, result, fields) => {
                    if (err) {
                        console.log("DB:: UPDATE addAmountToUserWallet ERROR :: "+err);
                        resolve(err);
                    }else{
                        LOG("DB:: UPDATE addAmountToUserWallet Success "+JSON.stringify(result) ); 
                      // this.postData(process.env.POST_CASH_REWARD_URL,"order_id="+order_id);
                      this.postData(process.env.POST_CASH_REWARD_URL,"order_id="+order_id+"&mobile_number="+data.mob_no+"&token="+data.auth_token+"&amount="+amount+"&tour_id="+data.tour_id);
                      // var postData = "order_id="+order_id+"";
                      // switch(data.tour_id){
                      //   case 5://practice mode                          
                      //      this.postCashData("http://stawallet.maxamind.in/api/playcash/add_playcash_amount/"+data.mob_no,data,postData);                     
                      //      break;
                      //   default:
                      //       this.postCashData("http://stawallet.maxamind.in/api/cash/add_cash/"+data.mob_no,data,postData);                        
                      //     break;
                      //        }
                     
                        resolve(result); 
                    }

                });


            }

        });

    });

}
  postCashData(url,data,bodyData){
  
  Request.post({
    "headers": { 'content-type' : 'application/x-www-form-urlencoded','Token':data.auth_token},
    "url": url,
     "body":  bodyData
      }, (error, response, body) => {
          if(error) {
            console.log("error = ",error);
              return console.dir(error);
          } 
          LOG("body = ",JSON.stringify(body)); 
        // console.log("response = ",response);
      }); 
  
  }
  
  
  postData(url,data){
  
  Request.post({
    "headers": { 'content-type' : 'application/x-www-form-urlencoded'},
    "url": url,
     "body":  data
      }, (error, response, body) => {
          if(error) {
            console.log("error = ",error);
              return console.dir(error);
          } 
          LOG("URL = "+url+" body = ",JSON.stringify(body));  
        // console.log("response = ",response);
      }); 
  
  }
  
  getGameWinRewards(data){
    //bid_coins,play_cash,cash,ticket
    return new promise((resolve,reject)=>{

        this.dbconnection.query("SELECT * FROM game_reward_config WHERE game_id = '"+data.tour_id+"' AND game_type = '"+data.game_type+"' AND game_status = 'won'",  (err, result, fields) => {
            if (err) {
                console.log("DB:: getGameWinRewards ERROR :: "+err);
                resolve(err);
            }else{
                LOG("DB:: getGameWinRewards Success "+JSON.stringify(result) ); 
                   resolve(result); 
            }

        });

    });
} 
   getGameLostRewards(data){
     return new promise((resolve,reject)=>{

        this.dbconnection.query("SELECT * FROM game_reward_config WHERE game_id = '"+data.tour_id+"' AND game_type = '"+data.game_type+"' AND game_status = 'lost'",  (err, result, fields) => {
            if (err) {
                console.log("DB:: getGameLostRewards ERROR :: "+err);
                resolve(err);
            }else{
                LOG("DB:: getGameLostRewards Success "+JSON.stringify(result) ); 
                  resolve(result); 
            }

        });

    });
} 
  insertUserRewards(data){
    
    return new promise((resolve,reject)=>{
        var order_id = "R_"+data.game_id+"_"+data.user_id+"_"+data.tour_id+"_"+data.game_type;
        LOG("insertUserRewards : order_id ==> ",order_id);
       if(data.user_id == '-2'){
        resolve("user is bot"); 
          LOG("insertUserRewards : user is bot ==> ");
        return true;
      }
        this.dbconnection.query("INSERT INTO game_rewards (order_id, user_id, game_id, bid_coins, cash, playcash, ticket, issued_status) VALUES ('"+order_id+"',"+data.user_id+","+data.game_id+","+data.rewards.bid_coins+", "+data.rewards.cash+","+data.rewards.play_cash+","+data.rewards.ticket+", 0)",  (err, result, fields) => {
            if (err) {
                console.log("DB:: insertUserRewards ERROR :: "+err);
                resolve(err);
            }else{
                LOG("DB:: insertUserRewards Success "+JSON.stringify(result) ); 
                this.postData(process.env.POST_GAME_REWARD_URL,"order_id="+order_id);
                 resolve(result); 
               
            }

        });

    });
  
  }
  refundAmountToUser(data){
     LOG("DB:: refundAmountToUser --------------------------------------------------------------------------------->data.user_id = "+data.user_id+" data.payment_id = "+data.payment_id);
     return new promise((resolve,reject)=>{
 
        this.dbconnection.query("UPDATE qm_tbl_payment SET status = 3 WHERE  payment_id = '"+data.payment_id+"';",  (err, result, fields) => {
            if (err) {
                console.log("DB:: refundAmountToUser ERROR :: "+err);
                resolve(err);
            }else{
                LOG("DB:: refundAmountToUser Success "+JSON.stringify(result) ); 
                  //resolve(result); 
              
                this.dbconnection.query("INSERT INTO refund_logs (payment_id,game_type,tour_id,user_id,created_date,game_id) VALUES ('"+data.payment_id+"','"+data.game_type+"',"+data.tour_id+","+data.user_id+",'"+dateTime.getDateTime(process.env.DATE_LOCALE,{timeZone:process.env.DATE_TIMEZONE},'yyyy-MM-dd hh:mm:ss')+"',"+data.game_id+");",  (err, result, fields) => {
                    if (err) {
                        console.log("DB:: refundAmountToUser refund_logs ERROR :: "+err);
                        resolve(err);
                    }else{ 
                        LOG("DB:: refundAmountToUser  refund_logs Success "+JSON.stringify(result) ); 
                          this.postData(process.env.POST_REFUND_URL,"order_id="+data.payment_id);
                          resolve(result); 
                    }
                   });
            }
            
           } );
     });
  }
}
//handleDisconnect();

/*

in case user wins
data = select user_id from qz_user_levels where user_id = 1

if(data){
	UPDATE qz_user_levels SET point = point + 1 WHERE user_id = 1
}else{
	INSERT INTO qz_user_levels (`id`, `user_id`, `point`, `level`) VALUES (1, 1, 1, 0);
}

//if lost

data = select user_id from qz_user_levels where user_id = 1

if(data){
	UPDATE qz_user_levels SET point = 0,level = 0 WHERE user_id = 1
}
//to get level
SELECT level FROM `qm_game_levels` where point_required <= point order by level desc limit 0,1
//UPDATE qz_user_levels SET level = 0 WHERE user_id = 1 (level value should be same you got after hitting qm_game_levels query)

*/