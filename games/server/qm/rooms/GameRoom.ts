import { Room , Client} from "colyseus";
import {  database,presence,totalServerClients,failedPaymentIds, LOG,LOG_ERROR} from '../server';
import {  dateTime } from '../server';
import * as promise from 'promise';
import  * as Request from "request"; 
require('dotenv').config(); 
//LOG(database); 
enum MSG_TYPE {      
    PLAYER_JOINED= "PLAYER_JOINED",
        PLAYER_REJOINED= "PLAYER_REJOINED",
        PLAYER_LEFT="PLAYER_LEFT", 
        NO_OPPONENTS =  "NO_OPPONENTS",
        READY_TO_START = "READY_TO_START",
        START_GAME = "START_GAME",        
        ANSWER_SUBMIT = "ANSWER_SUBMIT",
        ANSWER_RESULT= "ANSWER_RESULT",
        QUE_TIME_OUT = "QUE_TIME_OUT",
        TURN_CHANGED = "TURN_CHANGED",
        YOU_WIN = "YOU_WIN", 
        YOU_LOST = "YOU_LOST",
        GAME_DRAW = "GAME_DRAW",
        PING_PONG = "PING_PONG",
        TRANSLATE_TEXT = "TRANSLATE_TEXT",
       AUTH_FAILED = "AUTH_FAILED"
}  
enum GAME_STATE{ 
    NONE = "NONE",
        WAIT_FOR_OPPONENT= "WAIT_FOR_OPPONENT",
        READY_TO_START = "READY_TO_START",
        IN_GAME="IN_GAME",
        GAME_END =  "GAME_END",
        GAME_CLOSE = "GAME_CLOSE"
}
enum GAME_TYPE{ 
    NONE = "NONE",
        TOUR_MODE= "TOUR_MODE",
        VS_MODE="VS_MODE", 

}
    enum LANGUAGE_SUPPORTED{
        ENGLISH = "en",
            HINDI = "hi",
            NOT_SUPPORTED = 'NOT_SUPPORTED'
    }
enum CONNECT_STATUS {  
        ON_INIT= "ON_INIT",
        ON_AUTH= "ON_AUTH",
        ON_REQUEST_JOIN="ON_REQUEST_JOIN",
        ON_JOIN =  "ON_JOIN",
        ON_LEAVE = "ON_LEAVE",
        REFUND = "REFUND",
        RECONNECTED = "RECONNECTED"
}   
    const OPPONENT_WAIT_TIME = 30;//in sec   //value+5 will get display on search opponent screen 
    const ROOM_LOCK_TIME = 5;//in sec    
    const QUE_TIME = 10;//in sec        
    const QUE_TIME_OUT_BUFFER = 2;//in sec ...if change mak sure to update on answer submit condition
    const CLIENT_REJOIN_WAIT_TIME = 60;//in sec
    const CHANGE_TURN_WAIT_TIME = 2;//in sec
    const  QUE_IMAGE_PATH =  process.env.QUE_IMAGE_PATH;
    const READY_TO_START_TIME = 5;
    const PRESENCE_KEY_HOLD_SEC = 10;  
  // var totalServerClients =[];
   //  var isPresenceSubscribe = false;
      var isPlayCanvas = false;
    export class GameRoom extends Room  { 

   
        currentGameState =  GAME_STATE.NONE; 
    game_type = 'undefined';  
    answer_id = "opt_1";
    answer_status = -1;  
    que_available  = 100;
    current_que_index = -1;
    player_available = 0;
    queTimeIntervalRef = null;
    opponentWaitTimeOutRef = null;  
    minClients = 2; 
    maxPlayers = 2;
    playersCount =0;
    totalClients = 0; //all clients valid and invalid 
    stepCount =1;
    game_id = "";
    tour_id = "";  
    win_points = 0;  
    level_lives = 0;
    ans_score = 0;
    points_required = 0; 
    room_level = null; 
    isDBRoomReadyToStartGame = false;
    is_free = false;
    is_fixed_price = false;
    amount =0;
    pot_amount = 0;
     winning_id = 0;
     win_lives = 0; 
     gameWinRewards = {"bid_coins":0,"play_cash":0,"cash":0,"ticket":0};
     gameLostRewards  = {"bid_coins":0,"play_cash":0,"cash":0,"ticket":0};
    options_Data = {
    };
      
    // this room supports only 2 clients connected
    players_Data = {
    };   
    playersQueue = [];  
    roomLog = [];
    masterQueIndex = 0; 
    masterRefillCount = 0;   
    master_que_buffer = {"en":[],"hi":[]};    
    que_buffer = {"en":[],"hi":[]}; 
    currentQueData =  {
                  "en":{"question": "question","opt_1": "option 1", "opt_2": "option 2", "opt_3": "option 3","opt_4": "option 4"},
                  "hi":{"question": "question","opt_1": "option 1", "opt_2": "option 2", "opt_3": "option 3","opt_4": "option 4"}
              };  
    room_data = {
        currentGameState:GAME_STATE.NONE,
        currentTurnId: null,
        players: {}, 
        que_data:{ "type": 0,"image_path":"","question": "question","opt_1": "option 1", "opt_2": "option 2", "opt_3": "option 3","opt_4": "option 4"},
        que_time:QUE_TIME,
        // pot_amount:100,
        winner_list: {},// common  list for single win player and draw game users 
        replay_amount:10,
        points_required:0
    }; 
      isBotAvailable = false;
    isBotActive = false;   
    botWaitTimeRef = null; 
     que_level = 1; 
    prob = [
          80,//0
          60,//1
          40,//2
          40,//3
          30,//4
          30,//5
          20,//6
          20,//7
          20,//8
        ];
     isEven = -1;

      setEvenMode(options){
                          database.getBotConfig(options).then((result)=>{ 

                                  LOG("get........... Config SUCCESS :: "+JSON.stringify(result) );  
                                var _result = JSON.parse(JSON.stringify(result))[0];
                                 var val = this.randomInt(1,100);   
                                console.log("value === "+val+"_result.bot_available = "+_result.bot_available);
                            if(isPlayCanvas){
                                this.isBotAvailable  = true;
                            }else{
                                this.isBotAvailable  =  val <=_result.bot_available ?true:false;
                            }
                           
                           
                             this.prob[0] = _result.que_level_0;
                             this.prob[1] = _result.que_level_1;
                             this.prob[2] = _result.que_level_2;
                             this.prob[3] = _result.que_level_3;
                             this.prob[4] = _result.que_level_4;
                             this.prob[5] = _result.que_level_5;
                         

                              },(error)=>{
                                  LOG("get...........Config =>Error "+error);
                              });    
                            
                            
         
        
         
      }
    onInit (options) { 
      
      try{
       
        
        
        console.log(">>>>>>>>>>>>>>>>>roomId = "+this.roomId);
        
        Object.assign(options,{"connect_status":CONNECT_STATUS.ON_INIT});

        this.setSeatReservationTime(5000);  
        
      
        LOG("STEP "+(this.stepCount++)+": onInit BasicRoom created!", options);

      

        this.isDBRoomReadyToStartGame = false;
         

       this.autoDispose= true; 

         if(typeof(options.user_id) == 'undefined'
           || typeof(options.auth_token) == 'undefined'
           || typeof(options.level) == 'undefined' 
           || typeof(options.points) == 'undefined'
           || typeof(options.tour_id) == 'undefined'
           || typeof(options.payment_id) == 'undefined'
           || typeof(options.game_type) == 'undefined'
           || typeof(options.avatar_url) == 'undefined'   
           || typeof(options.lang) == 'undefined' 
           || (options.user_id+"") == "" 
           || (options.auth_token+"") == ""  
           || (options.level+"") == "" 
           || (options.points+"") == ""  
           || (options.tour_id+"") == "" 
           || (options.payment_id+"") == ""  
           || (options.game_type+"") == "" 
           || (options.avatar_url+"") == ""     
           || (options.lang+"") == ""   
          ){
            //this.setSeatReservationTime(2);
            this.lock();
            LOG("$$$$$$$$$$$$$$$$$$$$$$ Room lock on init as game_type is undefined");
            return;
        }
  
        this.room_level = options.level;  
        this.game_type = options.game_type; 
        this.tour_id = options.tour_id;
        
        this.setState({
            totalPlayers: this.clients.length,   
            pot_amount:0
        });
        
       
     
        switch(this.game_type){
            case GAME_TYPE.VS_MODE:
                //   this.maxClients = 5;
                this.maxPlayers = 5;
                  database.getGameConfig(options).then((result)=>{ 

                  this.is_free = JSON.parse(JSON.stringify(result))[0].is_free;
                  this.is_fixed_price = JSON.parse(JSON.stringify(result))[0].is_fixed_price;
                  this.amount =   JSON.parse(JSON.stringify(result))[0].amount;

                  if(this.is_fixed_price){
                      this.pot_amount = this.amount;
                  }else {
                      this.pot_amount = this.amount*this.clients.length;
                   }
                 
                  this.state = {"totalPlayers":Object.keys(this.players_Data).length,"pot_amount":this.pot_amount};
                  LOG("getGameConfig =>Success "+JSON.stringify(result) );  
              },(error)=>{
                  LOG.log("getGameConfig =>Error "+error);
              });
            
                break;
            case GAME_TYPE.TOUR_MODE:
                //  this.maxClients = 2;
                this.maxPlayers = 2;
            
                    database.getTourGameConfig(options).then((result)=>{ 

                    this.is_free = false;
                    this.is_fixed_price = true;
                    this.amount =   JSON.parse(JSON.stringify(result))[0].amount;
                    this.winning_id =  JSON.parse(JSON.stringify(result))[0].winning_id;
                    this.win_lives =   this.winning_id == 1 ? 1 : 0;//winning_id = 1 means give lives is set in database
                    if(this.is_fixed_price){
                        this.pot_amount = this.amount;
                    }else {
                        this.pot_amount = this.amount*this.clients.length;
                    } 
                   this.state = {"totalPlayers":Object.keys(this.players_Data).length,"pot_amount":this.pot_amount};
                   
                    LOG("getTourGameConfig =>Success "+JSON.stringify(result) );  
                },(error)=>{
                    LOG("getTourGameConfig =>Error "+error);
                });
                break;

        }
  LOG("this.game_type = "+this.game_type+"  this.maxClients = "+this.maxPlayers+"");
       
         database.getGameWinRewards(options).then((result)=>{ 

                    LOG("getWinRewards =>Success "+JSON.stringify(result) );  
                  var _result = JSON.parse(JSON.stringify(result))[0];
               
                 Object.assign(this.gameWinRewards,{"bid_coins":_result.bid_coins,"play_cash":_result.play_cash,"cash":_result.cash,"ticket":_result.ticket});
              
                },(error)=>{
                    LOG("getWinRewards =>Error "+error);
                });
          database.getGameLostRewards(options).then((result)=>{ 

                        LOG("getWinRewards =>Success "+JSON.stringify(result) );  
                      var _result = JSON.parse(JSON.stringify(result))[0];

                     Object.assign(this.gameLostRewards,{"bid_coins":_result.bid_coins,"play_cash":_result.play_cash,"cash":_result.cash,"ticket":_result.ticket});

                    },(error)=>{
                        LOG("getWinRewards =>Error "+error);
                    });    
        
       
        
         this.setEvenMode(options);
        
      }catch(e){
      LOG_ERROR("ERROR :: "+e.stack);
      }
       
    }


   
  
    requestJoin (options,isNewRoom) {    
      try{
         
           if(typeof(options.user_id) == 'undefined'
           || typeof(options.auth_token) == 'undefined'
           || typeof(options.level) == 'undefined' 
           || typeof(options.points) == 'undefined'
           || typeof(options.tour_id) == 'undefined'
           || typeof(options.payment_id) == 'undefined'
           || typeof(options.game_type) == 'undefined'
           || typeof(options.avatar_url) == 'undefined'   
           || typeof(options.lang) == 'undefined' 
           || (options.user_id+"") == "" 
           || (options.auth_token+"") == ""  
           || (options.level+"") == "" 
           || (options.points+"") == ""  
           || (options.tour_id+"") == "" 
           || (options.payment_id+"") == ""  
           || (options.game_type+"") == "" 
           || (options.avatar_url+"") == ""     
           || (options.lang+"") == ""   
          ){
   
         LOG("****************requestJoin Failed due missing user details****************"); 
            return false;
        } 
        
        
      LOG("STEP "+(this.stepCount++)+": requestJoin ", options); 
        Object.assign(options,{"connect_status":CONNECT_STATUS.ON_REQUEST_JOIN});
       
       

        if(this.currentGameState == GAME_STATE.GAME_END || this.currentGameState == GAME_STATE.GAME_CLOSE ){
            return false;
        }  
//         if(failedPaymentIds.indexOf(""+options.payment_id) >=0){    
//         LOG("Request failed for Auth failed payment id");
//            Object.assign(options,{"connect_status":"AUTH_FAILED"});
      
//           return false;
//         }
       
        
        LOG(this.currentGameState + "******************************************** requestJoin isNewRoom = ",isNewRoom);
       //  if(options.roomId==this.roomId){
       // return true;
       // }
    LOG("totalServerClients length in cpu ====> "+((totalServerClients).length));
   LOG(totalServerClients[totalServerClients.indexOf(""+options.payment_id)]+"payment id already exist in this cpu")
        var isChecked = false;
        if(!isNewRoom){
           if(totalServerClients.indexOf(""+options.payment_id) >=0){    
         // if(typeof totalServerClients[""+options.payment_id]!='undefined' && totalServerClients[""+options.payment_id] != null){  
                                LOG(options.payment_id+" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> requestjoin isClientInRoom ");
                                   isChecked = true;
                                    for(var id =0 ;id<Object.keys(this.options_Data).length;id++){ 
                                    
                                     if(typeof this.options_Data[id] != 'undefined' && typeof this.options_Data[id].options != 'undefined'){
                                           if( this.options_Data[id].options.user_id == options.user_id && this.options_Data[id].options.payment_id != options.payment_id){  

                                             LOG("@@@@@@@@@@@@@@@@@@@@@@@@request failed for room "+this.options_Data[id].options.payment_id);
                                               isChecked = true;
                                            return false;
                                            }
                                      if( this.options_Data[id].options.user_id == options.user_id && this.options_Data[id].options.payment_id == options.payment_id){  

                                             LOG("@@@@@@@@@@@@@@@@@@@@@@@@ allowed user for second request "+this.options_Data[id].options.payment_id);
                                          isChecked = true;
                                             if(typeof this.options_Data[id].options.isRefund  != 'undefined' && this.options_Data[id].options.isRefund == true){
                                                     LOG("1 Request join failed due to connect_status as  REFUND"); 
                                                      this.playersCount--;
                                                       return false;
                                                 }else{
                                                      return true
                                              }
                                           }
                                      }
                                   }
                            }  
                   
        }
        if(typeof(options.user_id) != 'undefined' && typeof(options.payment_id) != 'undefined'){ 
            for(var id =0 ;id<Object.keys(this.options_Data).length;id++){
              
              if(typeof(this.options_Data[id].options) == 'undefined'){
              continue;
              }
              
               if(this.options_Data[id].options.user_id == options.user_id && this.options_Data[id].options.payment_id != options.payment_id){  
               return false;
               }
                if( this.options_Data[id].options.user_id == options.user_id && this.options_Data[id].options.payment_id == options.payment_id ){  
                    LOG(" requestJoin 111111111111  Alredy in game room "+options.sessionId);
                  if(typeof this.options_Data[id].options.isRefund  != 'undefined' && this.options_Data[id].options.isRefund == true){
                       LOG("2 Request join failed due to connect_status as  REFUND"); 
                         return false;
                }
                    switch(this.game_type){
                        case GAME_TYPE.TOUR_MODE: 
                            return (this.room_level == options.level && this.game_type == options.game_type && this.tour_id == options.tour_id ?true:false);
                            break;
                        case GAME_TYPE.VS_MODE:
                        LOG(" requestJoin 111111111111  Alredy in game room accept = "+(this.game_type == options.game_type && this.tour_id == options.tour_id));
                            return (this.game_type == options.game_type && this.tour_id == options.tour_id?true:false);
                       
                        break;  
                    } 
                }
            } 
        }
     
     
        if(!isNewRoom){
           
            for(var id =0 ;id<Object.keys(this.options_Data).length;id++){ 

                if(typeof(this.options_Data[id].options) == 'undefined'){
              continue;
              }
               if(this.options_Data[id].options.sessionId == options.sessionId ){ 
               
                //if(this.options_Data[id].options.roomId == options.roomId && this.options_Data[id].options.sessionId == options.sessionId ){ 
                    LOG(" requestJoin 22222222222222222222 Alredy in game room "+options.sessionId); 
                    return true;
                }
            } 
 
        }
         //if(this.clients.length>= this.maxPlayers  
        if(this.player_available>= this.maxPlayers  
       // if(this.playersCount>= this.maxPlayers 
           || this.currentGameState == GAME_STATE.READY_TO_START 
           || this.currentGameState == GAME_STATE.IN_GAME 
           
           ){// if Game Room is full or game is started
            return false;
        } 
  

if(!isChecked){
        switch(this.game_type){
            case GAME_TYPE.TOUR_MODE:  
                LOG("___________________________this.room_level = "+this.room_level+" tthis.game_type = "+this.game_type);
                if(this.room_level == options.level && this.game_type == options.game_type && this.tour_id == options.tour_id){
                     Object.assign(options,{"ans_score":0});
                    Object.assign(options,{"isRequestAccepted":1,roomId:this.roomId});           
                       
                    if(typeof this.presence != 'undefined' ){
                   
                      
                   var presensePromise = this.presence.get(""+options.payment_id);
                     if(typeof presensePromise != 'undefined'){
                          presensePromise.then((result)=>{ 
                            LOG("----------------------->after set "+result);
                                  if(typeof result == 'undefined' || (result+"") == "" || result == null || isNewRoom){
                                 
                                   
                                    //if(this.playersCount < this.maxPlayers  )
                                       {
                                         this.options_Data[this.playersCount] = {"options":options};
                                        
                                              LOG("this.playersCount = "+this.playersCount+" user = "+options.user_id+" with payment_id = "+options.payment_id+" enter in room ");

                                        this.playersCount++; 
                                         this.presence.publish("ADD_PAYMENT_ID", ""+options.payment_id);
                                          this.presence.setex(""+options.payment_id,""+options.payment_id,PRESENCE_KEY_HOLD_SEC);
                                      } 
                                    
                                    
                                    
                                  }else{
                                    //already request accepted in another room
                                   Object.assign(options,{"isRequestAccepted":2}); 
                                     

                                  }
                                    
                      });
                       
                                 this.presence.setex(""+options.payment_id,""+options.payment_id,PRESENCE_KEY_HOLD_SEC);
                               
                       
                     }
                  }
                     return true;

                }else{
                    return false;
                }


                break;
            case GAME_TYPE.VS_MODE: 

                if(this.game_type == options.game_type && this.tour_id == options.tour_id ){ 
                    Object.assign(options,{"ans_score":0});
                    Object.assign(options,{"isRequestAccepted":1,roomId:this.roomId});
                      
                      //  this.options_Data[this.playersCount] = {"options":options};
                      //   this.playersCount++; 
                      // this.presence.publish("ADD_PAYMENT_ID", ""+options.payment_id);
                  if(typeof this.presence != 'undefined' ){
                   
                   
                   var presensePromise = this.presence.get(""+options.payment_id);
                     if(typeof presensePromise != 'undefined'){
                          presensePromise.then((result)=>{ 
                            LOG("----------------------->after set "+result);
                                  if(typeof result == 'undefined' || (result+"") == "" || result == null || isNewRoom){
                                     
                                    //if(this.playersCount < this.maxPlayers  )
                                       {
                                         this.options_Data[this.playersCount] = {"options":options};
                                        
                                              LOG("this.playersCount = "+this.playersCount+" user = "+options.user_id+" with payment_id = "+options.payment_id+" enter in room ");

                                        this.playersCount++; 
                                         this.presence.publish("ADD_PAYMENT_ID", ""+options.payment_id);
                               
                                          this.presence.setex(""+options.payment_id,""+options.payment_id,PRESENCE_KEY_HOLD_SEC);
                                      } 
                                    
                                    
                                    
                                  }else{
                                    //already request accepted in another room
                                   Object.assign(options,{"isRequestAccepted":2}); 
                                     

                                  } 
                                    
                      });
                                 this.presence.setex(""+options.payment_id,""+options.payment_id,PRESENCE_KEY_HOLD_SEC);
                               
                       
                     }
                  }
                     return true;

                }else{
                    return false;
                }

                break;
        }
}
        //return this.clients.filter(c => c.id === options.clientId).length === 0;
        console.log("Request finally failed for user_id = "+options.user_id+"  payment_id = "+options.payment_id);
        return true;
        }catch(e){
      LOG_ERROR("ERROR :: "+e.stack);
      }
        
    } 
    onAuth (options) {

      try{
        
       
        
        LOG("STEP "+(this.stepCount++)+": onAuth "+JSON.stringify(options));

var isNewUser = true;

        Object.assign(options,{"connect_status":CONNECT_STATUS.ON_AUTH});
       
        // Validation for authentication
        if(typeof(options.user_id) == 'undefined'
           || typeof(options.auth_token) == 'undefined'
           || typeof(options.level) == 'undefined' 
           || typeof(options.points) == 'undefined'
           || typeof(options.tour_id) == 'undefined'
           || typeof(options.payment_id) == 'undefined'
           || typeof(options.game_type) == 'undefined'
           || typeof(options.avatar_url) == 'undefined'   
           || typeof(options.lang) == 'undefined' 
           || (options.user_id+"") == "" 
           || (options.auth_token+"") == ""  
           || (options.level+"") == "" 
           || (options.points+"") == ""  
           || (options.tour_id+"") == "" 
           || (options.payment_id+"") == ""  
           || (options.game_type+"") == "" 
           || (options.avatar_url+"") == ""     
           || (options.lang+"") == ""   
          ){
 
          Object.assign(options,{comment:"onAuthFailed due to undefined paramaters or sessionId mismatch"});
            this.onAuthFailed(options);
             return true;  
        }  
        // if(failedPaymentIds.indexOf(""+options.payment_id) >=0){
        //     Object.assign(options,{comment:"onAuthFailed due to previous Failed payment id"});
        //     this.onAuthFailed(options);
        //      return true; 
        //    }
        
         for(var id =0 ,length = Object.keys(this.options_Data).length;id<length;id++){
                    
                         if(this.options_Data[id].options.user_id == options.user_id &&
                            this.options_Data[id].options.payment_id == options.payment_id 
                           ){ 
                                                                
                                   isNewUser = false;
                                 
                           if( this.options_Data[id].options.isAuthSuccess == 2){ 
                               
                                      Object.assign(options,{comment:"onAuthFailed due to second request for previous failed attempt"});
                                     this.onAuthFailed(options);
                                      return true;  
                                  } 
                         
                            //if( this.options_Data[id].options.roomId == options.roomId && this.options_Data[id].options.isAuthSuccess == 1){ 
                               if(  this.options_Data[id].options.isAuthSuccess == 1){ 
                            
                                      Object.assign(options,{comment:"onAuthSuccess due to second request for previous success attempt"});
                                     this.onAuthSuccess(options);
                                      return true;  
                                  } 
                        }
         }
         
        Object.assign(options,{"is_bot":0});
           // if(isNewUser){
           //      this.totalClients++;
           // }
      
        if(isPlayCanvas){
                   Object.assign(options,{"mob_no": "0123456789"});//cheat
               this.onAuthSuccess(options)//cheat            
               return true;//cheat 
        }
        
        //first validate user token and then validate level and points
        return new promise((resolve,reject)=>{ database.validateToken(options).then((result)=>{ 

            if(Object.keys(result).length == 0 || typeof(JSON.parse(JSON.stringify(result))[0].id) == 'undefined'){
                LOG("----------------------------------------------Authentication Failed => "+JSON.stringify(options));
               Object.assign(options,{comment:"onAuthFailed due to Invalid token"});
                this.onAuthFailed(options);
                resolve(options);
                 return true;  
            }else{  


                Object.assign(options,{"mob_no": JSON.parse(JSON.stringify(result))[0].mob_no});

                var promiseTourLives =   database.getUserTourLives(options);
                return  promiseTourLives.then((result)=>{ 

                    LOG("getUserTourLives =>Success "+JSON.stringify(result) ); 
                    if(JSON.parse(JSON.stringify(result)).length>=1 && JSON.parse(JSON.stringify(result))[0].tour_lives >= 1){

                        var data = JSON.parse(JSON.stringify(result))[0];

                        Object.assign(options,{"tour_lives": data.tour_lives});

                        return database.getUserLevelAndPoints(options).then((result)=>{

                            var data = JSON.parse(JSON.stringify(result))[0];

                            if(options.level == data.level && options.points == data.points ){      
                                LOG("----------------------------------------------Authentication Success => "+JSON.stringify(options));
                                Object.assign(options,{"total_points": data.total_points});
                             if(isNewUser){
                                 this.totalClients++;
                               }
                                 this.onAuthSuccess(options);
                                resolve(options);
                                return true;
                            }else{
                               
                                LOG("----------------------------------------------Authentication Failed => "+JSON.stringify(options));
                              Object.assign(options,{comment:"onAuthFailed due to Invalid level or points"});
                                this.onAuthFailed(options);
                                resolve(options);
                                 return true;  
                            }

                        },(error)=>{  
                          Object.assign(options,{comment:"onAuthFailed due to getUserLevelAndPoints Error"}); 
                          this.onAuthFailed(options);resolve(options); 
                           return true;  
                        });


                    }else{

                        LOG("----------------------------------------------Authentication Failed => "+JSON.stringify(options));
                        Object.assign(options,{comment:"onAuthFailed due to Tour Lives"}); 
                        this.onAuthFailed(options);
                        resolve(options);
                         return true;  
                    }

                },(error)=>{
                    LOG("getUserTourLives =>Error "+error);
                    LOG("----------------------------------------------Authentication Failed => "+JSON.stringify(options));
                   Object.assign(options,{comment:"onAuthFailed due to Tour Lives getUserTourLives =>Error "}); 
                    this.onAuthFailed(options);
                    resolve(options);
                     return true;  
                });



            }

        },(error)=>{ 
            LOG("----------------------------------------------Authentication Failed => "+JSON.stringify(options));
           Object.assign(options,{comment:"onAuthFailed due to validateToken Error"}); 
            this.onAuthFailed(options);
            resolve(options);
            return true;   
        
             });
                                             });
          }catch(e){
      LOG_ERROR("ERROR :: "+e.stack);
      }
 
    }   
    onAuthFailed(options){  
      try{
         Object.assign(options,{"isAuthSuccess": 0});
        LOG("--------------------------------------onAuthFailed--------Authentication Failed => "+JSON.stringify(options));
        
        this.presence.publish("ADD_FAILED_PAYMENT_ID", ""+options.payment_id);
                                
         for(var id =0 ;id<Object.keys(this.options_Data).length;id++){ 
           
           LOG("onAuthFailed user_id = "+this.options_Data[id].options.user_id+" payment_id = "+this.options_Data[id].options.payment_id+" vs  user_id = "+options.user_id+" vs payment_id = "+options.payment_id);
            
           if( this.options_Data[id].options.user_id == options.user_id &&
                this.options_Data[id].options.payment_id == options.payment_id 
               ){ 
                Object.assign(this.options_Data[id].options,options);
                Object.assign(this.options_Data[id].options,{"isAuthSuccess": 2});
                //this.playersCount--;  
                this.presence.publish("DEL_PAYMENT_ID",""+options.payment_id);            
          
            }
        }  
       
        
     }catch(e){
      LOG_ERROR("ERROR :: "+e.stack); 
      }
    }
    onAuthSuccess(options){
      try{
          LOG("--------------------------onAuthSuccess--------------------Authentication Success => "+JSON.stringify(options));
   
         for(var id =0 ,length = Object.keys(this.options_Data).length;id<length;id++){ 
             if(this.options_Data[id].options.user_id == options.user_id &&
                this.options_Data[id].options.payment_id == options.payment_id 
               ){ 
                Object.assign(this.options_Data[id].options,options);
                Object.assign(this.options_Data[id].options,{"isAuthSuccess": 1});
               
                   LOG("--------------------------onAuthSuccess--------------------Authentication Success => "+JSON.stringify(this.options_Data[id].options));
   
            }
        }  
         
         
      }catch(e){
    LOG_ERROR("ERROR :: "+e.stack);
      }
    }

       botWaitTime(options){
      try{
        console.log(this.playersCount+" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> bot time out");
        if(this.currentGameState == GAME_STATE.IN_GAME || this.currentGameState == GAME_STATE.GAME_END || this.currentGameState == GAME_STATE.GAME_CLOSE  ){
            return false;
        } 
        if(this.playersCount == 1){ 
        //if (this.player_available == 1) {
            this.isBotActive = true;   
 
            var bot =  {} as Client; 
            var botPoints = this.randomInt(0,this.points_required-1);
            if(botPoints <= 0){
            botPoints = 0;
            }
            this.options_Data[this.playersCount] = {"options":{is_bot:1,isAuthSuccess:1,clientId:"botClientId",sessionId:"botSessionId",user_id:'-2',level:this.room_level,auth_token:"user_token_2",game_type:""+this.game_type,payment_id:"bot_payment_id",lang:'en',total_points:0,mob_no:(this.randomInt(7,9)+""+this.randomInt(7,9)+"345678"+this.randomInt(0,9)+""+this.randomInt(0,9)),tour_lives:options.tour_lives,points:botPoints,tour_id:options.tour_id,avatar_url:""+this.randomInt(1,19)}};
           LOG("this.playersCount = "+this.playersCount+"user = "+this.options_Data[this.playersCount].options.user_id+" with payment_id = "+this.options_Data[this.playersCount].options.payment_id+" enter in room ");
 
          this.playersCount++; 
            this.totalClients++;
           Object.assign(bot,{id:"botClientId",sessionId:"botSessionId"});
            this.onJoin(bot,this.options_Data[this.playersCount-1].options); 
              
            LOG(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> bot joined to game");
        }
        }catch(e){
     LOG_ERROR("ERROR :: "+e.stack);
      }
    }
  
    oponentWaitTimeOut(){
      try{
 console.log("oponentWaitTimeOut ------------------> player_available = ",this.player_available);
 console.log("oponentWaitTimeOut ------------------> playersCount = ",this.playersCount);     
        if (this.player_available >= this.minClients ) 
        { 
              if(this.currentGameState == GAME_STATE.WAIT_FOR_OPPONENT)
                {
                    this.currentGameState = GAME_STATE.READY_TO_START;
                    this.clock.setTimeout( this.readyToStartGame.bind(this),ROOM_LOCK_TIME*1000);
                }
        }else{
            this.currentGameState = GAME_STATE.GAME_CLOSE;
            this.sendMessage(MSG_TYPE.NO_OPPONENTS);           
            if(this.opponentWaitTimeOutRef){
                this.opponentWaitTimeOutRef.clear();
            }
            for(var id =0 ;id<Object.keys(this.players_Data).length;id++){  
                if( this.game_type == GAME_TYPE.VS_MODE && this.players_Data[id].options.is_bot == 0){
                   this.players_Data[id].client.close(); 
                   var promiseRefund =  database.refundAmountToUser(this.players_Data[id].options);
                  this.presence.publish("DEL_PAYMENT_ID",""+this.players_Data[id].options.payment_id);
                  //this.presence.del(""+this.players_Data[id].options.payment_id);
                }

            } 
            this.onGameEnd();
          //  this.closeRoom();
        }   
         }catch(e){
     LOG_ERROR("ERROR :: "+e.stack);
      }
    }
     
    onJoin(client,options){

      if(typeof client == 'undefined'){
       return;
       }
  if(typeof(options.user_id) == 'undefined'
           || typeof(options.auth_token) == 'undefined'
           || typeof(options.level) == 'undefined' 
           || typeof(options.points) == 'undefined'
           || typeof(options.tour_id) == 'undefined'
           || typeof(options.payment_id) == 'undefined'
           || typeof(options.game_type) == 'undefined'
           || typeof(options.avatar_url) == 'undefined'   
           || typeof(options.lang) == 'undefined' 
           || (options.user_id+"") == "" 
           || (options.auth_token+"") == ""  
           || (options.level+"") == "" 
           || (options.points+"") == ""  
           || (options.tour_id+"") == "" 
           || (options.payment_id+"") == ""  
           || (options.game_type+"") == "" 
           || (options.avatar_url+"") == ""     
           || (options.lang+"") == ""   
          ){
    console.log("************************ OnJoin close undefined or empty options"+options);
    client.close();
return ; 
}
   // this.sendMessage(MSG_TYPE.PING_PONG,client);
      
  //      for(var id =0 ,length = Object.keys(this.options_Data).length;id<length;id++){
  //  LOG("on join "+this.options_Data[id].options.user_id+" with payment_id = "+this.options_Data[id].options.payment_id);                 
  // }
     var promise = new Promise((resolve, reject) => {   
                var intervalRef =   this.clock.setInterval(() => { 
                  //  LOG("ON JOIN :========= this.options_Data length = "+Object.keys(this.options_Data).length);
                  for(var id =0 ,length = Object.keys(this.options_Data).length;id<length;id++){
                    // LOG(options.user_id+" this.options_Data[id].options = "+this.options_Data[id].options.user_id+" onJoin isAuthSuccess = "+this.options_Data[id].options.isAuthSuccess);
                            
                         if(this.options_Data[id].options.user_id == options.user_id &&
                            this.options_Data[id].options.payment_id == options.payment_id 
                           ){ 
                            LOG(this.options_Data[id].options.user_id+" onJoin isAuthSuccess = "+this.options_Data[id].options.isAuthSuccess);
                               if( this.options_Data[id].options.isAuthSuccess == 1){ 
                                      if(intervalRef){
                                          intervalRef.clear();
                                      }
                                     this.onClientJoin(client,this.options_Data[id].options);
                                      resolve({"ready":1});
                                  } 
                           if( this.options_Data[id].options.isAuthSuccess == 2){ 
                                      if(intervalRef){
                                          intervalRef.clear();
                                      }
                                     this.sendMessage(MSG_TYPE.AUTH_FAILED,client);
                                     
                                      resolve({"ready":1});
                                  }  
                        }
                 
                   }
                }, 1000);

            }); 
            promise.then( (result)=>{ 
           
 
            });
    }
    onClientJoin (client,options) {   
    try{
      
      if(typeof client == 'undefined'){
       return; 
       }
     
       Object.assign(options,{roomId:this.roomId,clientId:client.id,sessionId:client.sessionId});
      
      
      var isNewUser = true;
 LOG("onClientJoin --> ",options);
      
console.log(this.playersCount+"onClientJoin --> ",options.payment_id);
        for(var id =0 ;id<Object.keys(this.players_Data).length;id++){ 
          //  LOG("------------------------------------------this.players_Data[id].options.user_id = ",this.players_Data[id].options.user_id); 
            if(this.players_Data[id].options.user_id == options.user_id && this.players_Data[id].options.payment_id == options.payment_id && this.players_Data[id].options.is_bot == 0){

                LOG( this.clients.length +" clients------------------------------------------ Client already with same userId ",options.user_id);

                isNewUser = false;
                var oldClient = this.clients[id];
                client.playerIndex = id;
               this.players_Data[id].client = client;
               database.saveExistingUserJoinRequest( {"user_id":options.user_id,"payment_id":options.payment_id,"client_id":client.id,"session_id":client.sessionId,"game_id":this.game_id,"game_type":this.game_type}); 

            }

        } 
 
    
        Object.assign(options,{"connect_status":CONNECT_STATUS.ON_JOIN});
        
      
        //LOG("total players : ",this.clients.length," Session id : " , options.sessionId,"  options = ",options);

        LOG('client Id :', client.id); 


        if(isNewUser){

            if(this.room_data.currentTurnId == null){// for first user  create room ..fill question buffer..join room in database

                this.currentGameState = GAME_STATE.WAIT_FOR_OPPONENT; 

                database.createGameRoom(options).then((result)=>{ 

                    LOG("-------------------------------------- createGameRoom => Success  "+JSON.stringify(result) );    
                    this.game_id = JSON.parse(JSON.stringify(result)).game_id;
                    
                  Object.assign(options,{"game_id":this.game_id,"tour_id":this.tour_id});
                   for(var id =0 ;id<Object.keys(this.players_Data).length;id++){ 
                      if(this.players_Data[id].options.user_id == options.user_id){
                         Object.assign(this.players_Data[id].options,{"game_id":this.game_id,"tour_id":this.tour_id});
                      break;  
                      }
                   }
                   var promiseUserConnectStatus = database.setUserConnectStatus(options);
                  
                    var promise1 =    database.getQuestionsFromMaster({"game_id":this.game_id,"tour_id":this.tour_id,"masterRefillCount":++this.masterRefillCount}); 
 
                    promise1.then((result)=>{  

                        this.master_que_buffer.en =  JSON.parse(JSON.stringify(result)).en;
                        this.master_que_buffer.hi =  JSON.parse(JSON.stringify(result)).hi;

                        //LOG("Master que_buffer => ",this.master_que_buffer.en[0]);

                        var promise2 =  this.fillQuestionBuffer();
                        promise2.then((result)=>{  

                            Object.assign(options,{"game_id":this.game_id,"tour_id":this.tour_id,"lives":1,"player_count":this.clients.length,"ans_score":0});

                       
                          
                            database.joinUserToGameRoom(options).then((result)=>{ 
                                //LOG("joinUserToGameRoom =>Success "+JSON.stringify(result) );  
                            },(error)=>{
                                console.log("joinUserToGameRoom =>Error "+error);
                            });



                            database.getLevelData(options).then((result)=>{ 

                                this.win_points = JSON.parse(JSON.stringify(result))[0].win_points;
                                this.level_lives = JSON.parse(JSON.stringify(result))[0].lives;
                                this.ans_score =   JSON.parse(JSON.stringify(result))[0].ans_score;
                                this.points_required =  JSON.parse(JSON.stringify(result))[0].points_required;

                                this.isDBRoomReadyToStartGame = true;
                                LOG("getLevelData =>Success "+JSON.stringify(result) );  
                            },(error)=>{
                                console.log("getLevelData =>Error "+error);
                            });


                        });

                    });


                },(error)=>{
                    console.log("createGameRoom =>Error "+error);
                });

            }else{// for other players directly join room in database

                 

                var promise = new Promise((resolve, reject) => {   

                    var intervalRef =   this.clock.setInterval(() => { 
                        LOG("check for isDBRoomReadyToStartGame to join user "+this.isDBRoomReadyToStartGame);
                     
                      if(this.isDBRoomReadyToStartGame){ 
                            if(intervalRef){
                                intervalRef.clear();
                            }
                         
                            resolve({"ready":1});
                        }  
                    }, 100);

                }); 
              
                promise.then( ( )=>{

                   // LOG(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> database.joinUserToGameRoom  ",options); 
                    Object.assign(options,{"game_id":this.game_id,"tour_id":this.tour_id,"lives":1,"player_count":this.clients.length,"ans_score":0});
                
                     for(var id =0 ;id<Object.keys(this.players_Data).length;id++){ 
                      if(this.players_Data[id].options.user_id == options.user_id){
                         Object.assign(this.players_Data[id].options,{"game_id":this.game_id,"tour_id":this.tour_id});
                      break;  
                      }
                   }
                  
                   var promiseUserConnectStatus = database.setUserConnectStatus(options);
      
                  
                  database.joinUserToGameRoom(options).then((result)=>{ 
                        //LOG("joinUserToGameRoom =>Success "+JSON.stringify(result) );  
                    },(error)=>{
                        //console.log("joinUserToGameRoom =>Error "+error);
                    });  



                });


            }           

 
          
            client.playerIndex = this.player_available;       
            this.player_available++;
            this.players_Data[client.playerIndex] = {"client":client,"options":options};
            this.options_Data[client.playerIndex] = {"options":options};
            this.options_Data[client.playerIndex].options.sessionId = options.sessionId; 
            this.options_Data[client.playerIndex].options.clientId = options.clientId; 
            this.playersQueue[client.playerIndex] = client.playerIndex;
           LOG("this.player_available ==> ",this.player_available); 
             //this.options_Data[client.playerIndex] = {"options":options};
            this.room_data.players[client.playerIndex] = {id: client.playerIndex,"lives":1,"avatar_url":options.avatar_url,"mob_no":this.getMaskNumber(options.mob_no,2,2,"x"),"ans_score":0,win_amount:0,bid_coins:0,tickets:0,tour_lives:options.tour_lives,points:options.points,level:options.level}; 
            if(this.room_data.currentTurnId == null)
            {
              LOG("***********************************first user**********************************************");
                this.room_data.currentTurnId = client.playerIndex; 

                if(this.opponentWaitTimeOutRef){
                    this.opponentWaitTimeOutRef.clear();
                }
                this.opponentWaitTimeOutRef = this.clock.setTimeout(this.oponentWaitTimeOut.bind(this), OPPONENT_WAIT_TIME * 1000);

                if(this.botWaitTimeRef){
                    this.botWaitTimeRef.clear();
                }
                if(this.isBotAvailable){
                this.botWaitTimeRef = this.clock.setTimeout(this.botWaitTime.bind(this,options), (OPPONENT_WAIT_TIME - this.randomInt(OPPONENT_WAIT_TIME-10,OPPONENT_WAIT_TIME-5))*1000);//10*1000 // (OPPONENT_WAIT_TIME - this.randomInt(OPPONENT_WAIT_TIME-10,OPPONENT_WAIT_TIME-5))*1000);
                }
            }
        }   

        if(this.is_fixed_price){
            this.pot_amount = this.amount;
        }else {
            this.pot_amount = this.amount*this.player_available;
        }
        this.state = {"totalPlayers":Object.keys(this.players_Data).length,"pot_amount":this.pot_amount,level_win_prize:"Win Rs "+this.pot_amount};
       this.options_Data[client.playerIndex].options.sessionId = client.sessionId; 
       this.options_Data[client.playerIndex].options.clientId = client.id; 
        this.sendMessage(MSG_TYPE.PING_PONG,client);
        this.sendMessage(isNewUser == true ? MSG_TYPE.PLAYER_JOINED:MSG_TYPE.PLAYER_REJOINED , client); 

        if(!isNewUser){

            if(this.room_data.players[client.playerIndex].lives <= 0){
                this.sendMessage(MSG_TYPE.YOU_LOST, client);
            }
            if( typeof this.room_data.winner_list[client.playerIndex] != 'undefined'){
                this.sendMessage(MSG_TYPE.YOU_WIN, client);
            }

        }

        if (this.player_available == this.maxPlayers && isNewUser) 
        {
            LOG("+++++++++++++++++++++++++Max Count reached+++++++++++++++++++++++++++++++++++++");

           if(this.currentGameState == GAME_STATE.WAIT_FOR_OPPONENT) 
                {
                    this.currentGameState = GAME_STATE.READY_TO_START;
                    this.clock.setTimeout( this.readyToStartGame.bind(this),ROOM_LOCK_TIME*1000);
                }
        }
 }catch(e){
     LOG_ERROR("ERROR :: "+e.stack);
      }

    }
     readyToStartGame(){
       try{
         //if(this.currentGameState == GAME_STATE.WAIT_FOR_OPPONENT)
        {
            this.currentGameState = GAME_STATE.READY_TO_START;
          
          this.options_Data = [];
           for(var id =0 ,length = Object.keys(this.players_Data).length;id<length;id++){ 
             this.options_Data[id]={options:""};           
             Object.assign(this.options_Data[id].options,this.players_Data[id].options);
           
            
        }  
          
          this.playersCount = Object.keys(this.options_Data).length;
          LOG("@@@@@@@@@@@@@@@@@@@@@@@@readyToStartGame  this.playersCount "+this.playersCount+" this.player_available"+this.player_available);
          this.maxPlayers = this.player_available;
            // this.lock();
            if(this.opponentWaitTimeOutRef){
                this.opponentWaitTimeOutRef.clear();
            } 

            if(this.botWaitTimeRef){
                this.botWaitTimeRef.clear();
            }

            var promise = new Promise((resolve, reject) => {   
                var intervalRef =   this.clock.setInterval(() => { 
                    LOG("check for isDBRoomReadyToStartGame to start game "+this.isDBRoomReadyToStartGame+" this.player_available "+this.player_available+"  this.playersCount = "+this.playersCount);
              if(this.isDBRoomReadyToStartGame){                    
                   // if(this.isDBRoomReadyToStartGame && this.player_available >=  this.playersCount){ 
                       this.maxPlayers = this.player_available;
                        if(intervalRef){
                            intervalRef.clear();
                        }
                        resolve({"ready":1});
                    }  
                }, 1000); 

            }); 
            promise.then( (result)=>{
            LOG("this.room_data = ",this.room_data);
             this.maxPlayers = this.player_available; 
             this.sendMessage(MSG_TYPE.READY_TO_START);             
             this.clock.setTimeout(this.startGame.bind(this), READY_TO_START_TIME * 1000);


            });
        }
        }catch(e){
      LOG_ERROR("ERROR :: "+e.stack);
      }
     } 
    startGame(){
      try{
          this.currentGameState = GAME_STATE.IN_GAME;
          this.room_data.points_required = this.points_required ;
                for(var id =0 ;id<Object.keys(this.players_Data).length;id++){
                    this.room_data.players[id].lives = this.level_lives
                    Object.assign(this.players_Data[id].options,{points_required:this.points_required,win_points:this.win_points});
                  
                    if(this.game_type == GAME_TYPE.VS_MODE && this.players_Data[id].options.connect_status == CONNECT_STATUS.ON_LEAVE){ //to refund amount if user is not connected to game at game start
                          this.players_Data[id].client.close(); 
                          Object.assign(this.players_Data[id].options,{"isRefund":true}); 
                          Object.assign(this.options_Data[id].options,{"isRefund":true});                       
                         var promiseRefund =  database.refundAmountToUser(this.players_Data[id].options);
                           
                        }
                }  
                database.startGame({"game_id":this.game_id,"tour_id":this.tour_id}); 
                this.setRoomQuestion();
                this.sendMessage(MSG_TYPE.START_GAME);
         }catch(e){
      LOG_ERROR("ERROR :: "+e.stack);
      }

    } 

    async onLeave (client) {
        // flag client as inactive for other users 
        //this.state.inactivateClient(client);

        try {
          
          if(typeof client == 'undefined'){
       return;
       }
            // allow disconnected client to rejoin into this room until 10 seconds
            LOG("onLeave >>>>>>>>>>>> client.playerIndex = "+client.playerIndex +" client.sessionId = "+client.sessionId); 

            Object.assign(this.players_Data[client.playerIndex].options,{"connect_status":CONNECT_STATUS.ON_LEAVE}); 
           var promiseUserConnectStatus = database.setUserConnectStatus(this.players_Data[client.playerIndex].options);
                var rejoinClient;
                rejoinClient = await this.allowReconnection(client, CLIENT_REJOIN_WAIT_TIME); 
 if(this.room_data.players[client.playerIndex].lives >=1 &&  (this.currentGameState == GAME_STATE.WAIT_FOR_OPPONENT ||this.currentGameState == GAME_STATE.IN_GAME  )){
           
               LOG("onLeave Reconnected step 1 >>>>>>>>>>>> client.sessionId = "+client.sessionId+" rejoinClient.sessionId =>"+rejoinClient.sessionId);

                 if(this.room_data.players[client.playerIndex].lives >=1 && 
                    this.players_Data[client.playerIndex].options.isRefund != true &&
                    (this.currentGameState == GAME_STATE.IN_GAME )){
  //if(    this.players_Data[client.playerIndex].options.isRefund != true)
   //               {
                   LOG("onLeave Reconnected step 2 >>>>>>>>>>>> client.sessionId = "+client.sessionId+" rejoinClient.sessionId =>"+rejoinClient.sessionId);

                    for(var id =0 ;id<Object.keys(this.players_Data).length;id++){ 

                        if(this.players_Data[id].client.sessionId == client.sessionId){ 

                            LOG("onLeave Reconnected step 3 >>>>>>>>>>>> client.sessionId = "+client.sessionId+" rejoinClient.sessionId =>"+rejoinClient.sessionId);

                            rejoinClient.playerIndex = id;
                            this.players_Data[id].client = rejoinClient; 
                            this.options_Data[id].options.sessionId = rejoinClient.sessionId;
                            this.options_Data[id].options.clientId = rejoinClient.clientId;
                            this.sendMessage(MSG_TYPE.PLAYER_REJOINED,rejoinClient);
                            this.sendMessage(MSG_TYPE.PING_PONG,rejoinClient); 
                            Object.assign(this.players_Data[id].options,{"connect_status":CONNECT_STATUS.RECONNECTED}); 
                            var promiseUserConnectStatus = database.setUserConnectStatus(this.players_Data[id].options);
        
                        }
                    }    
 
                }
           } //if close
    
        } catch (e) {
//             for(var id =0 ;id<Object.keys(this.players_Data).length;id++){ 

//                         if(this.players_Data[id].client.sessionId == client.sessionId){ 
          
//                                if(typeof this.players_Data[id] != 'undefined' && typeof this.players_Data[id].options != 'undefined'){
//                                         this.presence.publish("DEL_PAYMENT_ID",""+this.players_Data[id].options.payment_id); 
//                                         this.presence.del(""+this.players_Data[id].options.payment_id);
//                                }
//                         }
//             }
            console.log(CLIENT_REJOIN_WAIT_TIME+" seconds expired. let's remove the client."+client.sessionId);        
             // if(Object.keys(this.clients).length <= 0){
             // this.presence.unsubscribe("ADD_PAYMENT_ID");
             // this.presence.unsubscribe("DEL_PAYMENT_ID");  
             // }
            // 60 seconds expired. let's remove the client.
            // this.state.removeClient(client);
        } 
    } 

    sendMessage(msg_type,client = null){

        try{
          
          if(typeof client == 'undefined'){
       return;
       }

            if(msg_type != MSG_TYPE.PING_PONG){
                LOG("Server send Message--->"+msg_type);
            }
            var message ;//= {};
            this.room_data.currentGameState = this.currentGameState;
            switch(msg_type){

                case  MSG_TYPE.PING_PONG:
                    message = {
                        msg_type : msg_type                
                    }; 
                for(var id =0 ;id<Object.keys(this.players_Data).length;id++){ 
                     if(this.players_Data[id].client.sessionId == client.sessionId){
                        this.send(this.players_Data[id].client,message);  
                       break;
                     }
                    } 
                //   this.send(client,message);
                    break;
                case  MSG_TYPE.AUTH_FAILED:
                    message = {
                        msg_type : msg_type                
                    }; 
                    this.send(client,message);
                    break;
                case  MSG_TYPE.PLAYER_JOINED:
                  
                    message = {
                        msg_type : msg_type,
                        id:client.playerIndex,
                        room_data:this.room_data,
                        OPPONENT_WAIT_TIME:(OPPONENT_WAIT_TIME+ROOM_LOCK_TIME+1)
                    }; 
                            this.setTranslatedRoomQueData(this.players_Data[client.playerIndex].options.lang);//step1
                             message.room_data = this.room_data;//step2
                    this.send(client,message);
                    break;
                case  MSG_TYPE.PLAYER_REJOINED:
                 
                    message = {
                        msg_type : msg_type,
                        id:client.playerIndex,
                        room_data:this.room_data 
                    }; 
                            this.setTranslatedRoomQueData(this.players_Data[client.playerIndex].options.lang);//step1
                             message.room_data = this.room_data;//step2
                    this.send(client,message);
                    break; 
                case  MSG_TYPE.NO_OPPONENTS:
                    message = {
                        msg_type : msg_type        
                    }; 
                    for(var id =0 ;id<Object.keys(this.players_Data).length;id++){                
                        this.send(this.players_Data[id].client,message);                              
                    } 
                    break; 
                case  MSG_TYPE.READY_TO_START:
                          message = {
                                  msg_type : msg_type,
                                  room_data:this.room_data,
                                  READY_TO_START_TIME:READY_TO_START_TIME
                               };          
                   // this.broadcast(message); 
                
                 for(var id =0 ;id<Object.keys(this.players_Data).length;id++){                       
                             this.setTranslatedRoomQueData(this.players_Data[id].options.lang);
                             message.room_data = this.room_data;
                            this.send(this.players_Data[id].client,message);                   
                        
                    } 
                    break;
                case  MSG_TYPE.START_GAME:
                    message = {
                        msg_type : msg_type,
                        room_data:this.room_data 
                    }; 
                   
                 for(var id =0 ;id<Object.keys(this.players_Data).length;id++){
                        if(this.room_data.players[id].lives >=1){
                            this.setTranslatedRoomQueData(this.players_Data[id].options.lang);
                            message.room_data = this.room_data;
                            this.send(this.players_Data[id].client,message);                   
                        }
                    } 
                    break;

                case MSG_TYPE.ANSWER_RESULT:
                    message = {
                        msg_type : msg_type,
                        room_data:this.room_data, 
                        answer_status:this.answer_status
                    }; 
                    //this.broadcast(message);  
                 for(var id =0 ;id<Object.keys(this.players_Data).length;id++){                     
                           this.setTranslatedRoomQueData(this.players_Data[id].options.lang);
                             message.room_data = this.room_data;
                            this.send(this.players_Data[id].client,message);                   
                     
                    } 
                    break;

                case MSG_TYPE.TURN_CHANGED:      

                    message = {
                        msg_type : msg_type,
                        room_data:this.room_data                
                    }; 
                    // this.broadcast(message); 
                    for(var id =0 ;id<Object.keys(this.players_Data).length;id++){
                        if(this.room_data.players[id].lives >=1){
                           this.setTranslatedRoomQueData(this.players_Data[id].options.lang);
                             message.room_data = this.room_data;
                            this.send(this.players_Data[id].client,message);
                        }
                    } 
                    break;
                case MSG_TYPE.QUE_TIME_OUT:
                    message = {
                        msg_type : msg_type,
                        room_data:this.room_data, 
                        answer_status:this.answer_status
                    }; 
                   // this.broadcast(message); 
                 for(var id =0 ;id<Object.keys(this.players_Data).length;id++){
                     
                             this.setTranslatedRoomQueData(this.players_Data[id].options.lang);
                             message.room_data = this.room_data;
                            this.send(this.players_Data[id].client,message);                   
                       
                    } 
                    break;

                case MSG_TYPE.YOU_WIN:      

                    message = {
                        msg_type : msg_type,
                        room_data:this.room_data
                    }; 
                    for(var id =0 ;id<Object.keys(this.players_Data).length;id++){
                        if(this.room_data.players[id].lives >=1){
                           this.setTranslatedRoomQueData(this.players_Data[id].options.lang);
                             message.room_data = this.room_data;
                            this.send(this.players_Data[id].client,message);                   
                        }
                    } 
                    break;
                case MSG_TYPE.YOU_LOST:      

                    message = {
                        msg_type : msg_type,
                        room_data:this.room_data
                    }; 
                      this.setTranslatedRoomQueData(this.players_Data[client.playerIndex].options.lang);//step1
                      message.room_data = this.room_data;
                    this.send(client,message);
                    // this.send(this.players_Data[this.room_data.currentTurnId],message);

                    break;

                case MSG_TYPE.GAME_DRAW:      

                    message = {
                        msg_type : msg_type,
                        room_data:this.room_data
                    }; 
                    for(var id =0 ;id<Object.keys(this.players_Data).length;id++){
                        if(this.room_data.players[id].lives >=1){
                            this.setTranslatedRoomQueData(this.players_Data[id].options.lang);
                             message.room_data = this.room_data;
                            this.send(this.players_Data[id].client,message);                   
                        }
                    } 
                    break;
            }
         }catch(e){
           LOG_ERROR("ERROR :: "+e.stack);
      }
    } 

     setTranslatedRoomQueData(lang){
       try{
        this.room_data.que_data.question = this.currentQueData[lang].question;
        this.room_data.que_data.opt_1 = this.currentQueData[lang].opt_1;
        this.room_data.que_data.opt_2 = this.currentQueData[lang].opt_2;
        this.room_data.que_data.opt_3 = this.currentQueData[lang].opt_3;
        this.room_data.que_data.opt_4 = this.currentQueData[lang].opt_4;  
         
          }catch(e){
           LOG_ERROR("ERROR :: "+e.stack);
        }
         
     } 
    setRoomQuestion(){
  try{
        this.current_que_index++;
    
        this.que_level=this.que_buffer.en[this.current_que_index].que_level
    
        this.room_data.que_data.type = this.que_buffer.en[this.current_que_index].type;   
      //english
         this.currentQueData.en.question = this.que_buffer.en[this.current_que_index].question;
         this.currentQueData.en.opt_1 = this.que_buffer.en[this.current_que_index].opt_1;
         this.currentQueData.en.opt_2 = this.que_buffer.en[this.current_que_index].opt_2;
         this.currentQueData.en.opt_3 = this.que_buffer.en[this.current_que_index].opt_3;
         this.currentQueData.en.opt_4 = this.que_buffer.en[this.current_que_index].opt_4;  
      //hindi
         this.currentQueData.hi.question = this.que_buffer.hi[this.current_que_index].question;
         this.currentQueData.hi.opt_1 = this.que_buffer.hi[this.current_que_index].opt_1;
         this.currentQueData.hi.opt_2 = this.que_buffer.hi[this.current_que_index].opt_2;
         this.currentQueData.hi.opt_3 = this.que_buffer.hi[this.current_que_index].opt_3;
         this.currentQueData.hi.opt_4 = this.que_buffer.hi[this.current_que_index].opt_4;  
      
        this.answer_id = this.que_buffer.en[this.current_que_index].ans_id;

        switch(this.room_data.que_data.type){
            case 0:
                this.room_data.que_data.image_path = "";
                break;
            case 1:

                this.room_data.que_data.image_path = QUE_IMAGE_PATH+this.que_buffer.en[this.current_que_index].image_path;

                break;
        } 
        this.que_available --;   
        if(this.current_que_index >= Object.keys(this.que_buffer.en).length-2){
            //this.que_index =0;
            this.fillQuestionBuffer();//Fetch question from database and fill into this.que_buffer
        }

        if (this.queTimeIntervalRef) {
            this.queTimeIntervalRef.clear();
        }
        this.room_data.que_time =  QUE_TIME;

        LOG(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> currentTurnId = ",this.room_data.currentTurnId);
        this.queTimeIntervalRef = this.clock.setInterval(this.queTimeInterval.bind(this),1000);

        this.roomLog[this.current_que_index]={"room_data" :JSON.stringify(this.room_data),
                                              "user_id":this.players_Data[ this.room_data.currentTurnId ].options.user_id,
                                              "payment_id":this.players_Data[ this.room_data.currentTurnId ].options.payment_id,
                                              "que_id":this.que_buffer.en[this.current_que_index].id,
                                              "ans_id":this.answer_id,
                                              "ans_submit":"not answer",
                                              "ans_time":this.room_data.que_time,
                                              "ans_status":0,                                                                   
                                              "lives":this.room_data.players[this.room_data.currentTurnId].lives,
                                              "tour_id":this.tour_id
                                             } ; 

        //LOG("-----------------------------current roomLog ",this.roomLog); 
      LOG("Current Question servr Id = "+this.que_buffer.en[this.current_que_index].id);
        var promiseServeCount =   database.updateQuestionServeCount({"que_id":this.que_buffer.en[this.current_que_index].id});
        promiseServeCount.then((result)=>{  

            LOG(" >>>>>>>>>>> updateQuestionServeCount "+JSON.stringify(result));
        },(error)=>{
            LOG(" >>>>>>>>>>> updateQuestionServeCount error "+error);
        });
        this.que_buffer.en[this.current_que_index] = {}; // to remove question from buffer
        this.que_buffer.hi[this.current_que_index] = {}; // to remove question from buffer
 }catch(e){
            LOG_ERROR("ERROR :: "+e.stack);
      }
    } 

   

    changeTurn(){ 
      try{
//return;//cheat
        if (this.queTimeIntervalRef) {
            this.queTimeIntervalRef.clear();
        }
       LOG("change Turn Object.keys(this.players_Data).length = "+Object.keys(this.players_Data).length);
       LOG("change Turn Object.keys(this.players_Data).length = "+Object.keys(this.room_data.players).length);
        for(var _id = 0 ;_id<Object.keys(this.players_Data).length;_id++){
            if(this.room_data.players[_id].lives <= 0){     
                if(this.players_Data[_id].options.is_bot== 0){
                    //this.players_Data[_id].client.close();
                }
            }
        } 
        
          var id = this.playersQueue.indexOf(this.room_data.currentTurnId); 
        if(this.room_data.players[this.room_data.currentTurnId].lives <=0){
          this.playersQueue.splice( this.playersQueue.indexOf(this.room_data.currentTurnId),1);
        }else{
        id++;
        }
        
        
        if(id >= Object.keys(this.playersQueue).length){ 
            id = 0;
        }
         id = this.playersQueue[id];
        LOG("********************** on changeTurn new id selected = "+id);
        
//         for(;id<Object.keys(this.players_Data).length;id++){ 
//             if(this.room_data.players[id].lives > 0){
//                 break;
//             }
//         } 
//         if(id >= Object.keys(this.players_Data).length){ 

//             for(id = 0;id<Object.keys(this.players_Data).length;id++){ 
//                 if(this.room_data.players[id].lives > 0){
//                     break;
//                 }
//             } 
//         }

        /*
        var id = this.room_data.currentTurnId; 
        if(this.room_data.players[id].lives <=0){
          this.playersQueue.splice( this.playersQueue.indexOf(id),1);
        }
        
        id++;
        if(id >= Object.keys(this.players_Data).length){ 
            id = 0;
        }
        for(;id<Object.keys(this.players_Data).length;id++){ 
            if(this.room_data.players[id].lives > 0){
                break;
            }
        } 
        if(id >= Object.keys(this.players_Data).length){ 

            for(id = 0;id<Object.keys(this.players_Data).length;id++){ 
                if(this.room_data.players[id].lives > 0){
                    break;
                }
            } 
        }
*/
        if(this.player_available ==1 || Object.keys(this.playersQueue).length == 1){// || this.que_available <=0 player win condition || //game over condition         

            this.currentGameState = GAME_STATE.GAME_END;
            // this.lock();
            if(this.player_available == 1){ 

                for(var i =0;i<Object.keys(this.players_Data).length;i++){          
                    if(this.room_data.players[i].lives > 0 && this.players_Data[i].options.isRefund != true){
                        this.room_data.winner_list[i] = {id:i};

                       
                        this.room_data.players[i].win_amount = this.pot_amount;
                         Object.assign(this.players_Data[i].options,{"win_amount":this.room_data.players[i].win_amount});
                      
                        Object.assign( this.room_data.players[i],{rewards:this.gameWinRewards,"winning_id":this.winning_id});
                        Object.assign( this.players_Data[i].options,{rewards:this.gameWinRewards,"winning_id":this.winning_id});
      
                      var promiseRewards = database.insertUserRewards(this.players_Data[i].options);
     
                      switch(this.game_type){
                            case GAME_TYPE.VS_MODE:
                               // if( this.players_Data[i].options.user_id>12){// 1--12 are test users ..and
                                 this.players_Data[i].options.tour_lives = 0;
                                //}
                                  this.players_Data[i].options.level = 0;
                                  this.players_Data[i].options.points = 0; 
                                  this.room_data.players[i].points = 0; 
                                var promiseTourLives =  database.updateLivesCount(this.players_Data[i].options);
                                var promiseAddAmountToWallet = database.addAmountToUserWallet(this.players_Data[i].options);  
                                var promise2 =  database.addUserToWinnerList({"game_id":this.game_id,"tour_id":this.tour_id,"user_id":this.players_Data[i].options.user_id,"payment_id":this.players_Data[i].options.payment_id,"points":0,"level":0});

                                break;
                            case GAME_TYPE.TOUR_MODE:
                            
                               this.players_Data[i].options.points += this.win_points; 
                               this.room_data.players[i].points += this.win_points;                         
                               this.players_Data[i].options.total_points+= this.win_points;   
                                   //  this.players_Data[i].options.level = 0;//cheat
                                   // this.players_Data[i].options.points = 0;//cheat
                                   //  this.players_Data[i].options.total_points = 0;//cheat
                                if(this.players_Data[i].options.points  >= this.players_Data[i].options.points_required){                                  
                                     
                                    this.players_Data[i].options.tour_lives += this.win_lives;                                  
                                  
                                    this.players_Data[i].options.level++;// Level up
                                   if(this.players_Data[i].options.level > 11){
                                     this.players_Data[i].options.level = 0;
                                     this.players_Data[i].options.total_points = 0;
                                     this.players_Data[i].options.tour_lives  = 0;
                                   }
                                    this.players_Data[i].options.points = 0;
                                    var promiseTourLives =  database.updateLivesCount(this.players_Data[i].options);
                                    var promiseAddAmountToWallet = database.addAmountToUserWallet(this.players_Data[i].options); 
                                 } 

                                var promise =  database.setUserLevelAndPoints(this.players_Data[i].options);
                                promise.then((result)=>{  

                                    LOG("Adding data to winner list "+JSON.stringify(result));
                                    var user_id = JSON.parse(JSON.stringify(result)).user_id;
                                    var points  = JSON.parse(JSON.stringify(result)).points;
                                    var level   = JSON.parse(JSON.stringify(result)).level;                        
                                    var promise3 =  database.addUserToWinnerList({"game_id":this.game_id,"tour_id":this.tour_id,"user_id":user_id,"points":points,"level":level});
                                },(error)=>{

                                });

                                break;

                        }


                        var promise1 =  database.updateUserTable({"game_id":this.game_id,"tour_id":this.tour_id,"user_id":this.players_Data[i].options.user_id,"lives":this.room_data.players[i].lives,"ans_score":this.room_data.players[i].ans_score,"status":1});





                    }  
                }         
                this.sendMessage(MSG_TYPE.YOU_WIN); 


            }else{

                for(var i =0;i<Object.keys(this.players_Data).length;i++){          
                    if(this.room_data.players[i].lives > 0){
                        //this.room_data.winner_list[i] = {id:i};
                        //this.players_Data[i].options.points += 1;
                        //  var promise =  database.setUserLevelAndPoints(this.players_Data[i].options);
                        var promise1 =  database.updateUserTable({"game_id":this.game_id,"tour_id":this.tour_id,"user_id":this.players_Data[i].options.user_id,"payment_id":this.players_Data[i].options.payment_id,"lives":this.room_data.players[i].lives,"ans_score":this.room_data.players[i].ans_score,"status":1});

                    }  
                }   


                this.sendMessage(MSG_TYPE.GAME_DRAW);
            }      


            for(var _id = 0 ;_id<Object.keys(this.players_Data).length;_id++){ 
                if(this.room_data.players[_id].lives >=1){     
                    if(this.players_Data[_id].options.is_bot== 0){
                        //this.players_Data[_id].client.close();
                    }
                }
            } 
            this.onGameEnd ();

        }else{// continue game with next question          
            this.room_data.que_time = 0;
            this.room_data.currentTurnId = id;
            this.setRoomQuestion();      
            this.sendMessage(MSG_TYPE.TURN_CHANGED);

            if(typeof this.players_Data[id]!= 'undefined' && this.players_Data[id].options.is_bot == 1){
              var anstime = [ 
                {min :3,max:7},//0
                 {min :3,max:7},//1
                 {min :4,max:8},//2
                 {min :4,max:8},//3
                {min :5,max:9},//4
                {min :5,max:9},//5 
                 {min :5,max:9},//6 
                 {min :5,max:9},//7
                 {min :5,max:9},//8 
              ];
            
             this.clock.setTimeout(this.onSubmitBotAnswer.bind(this), this.randomInt(anstime[this.que_level].min,anstime[this.que_level].max) * 1000); 
                //this.clock.setTimeout(this.onSubmitBotAnswer.bind(this), this.randomInt(3,8) * 1000);
            }

        } 

     }catch(e){
            LOG_ERROR("ERROR :: "+e.stack);
      }
    }

    onSubmitBotAnswer(){
      try{
       LOG("--------------------------------------------- onSubmitAnswer");
        var botAnswer = this.answer_id;
        var rndVal = this.randomInt(1,100);
        var options = ["option1_id","option2_id","option3_id","option4_id"];
        options.splice(options.indexOf(this.answer_id),1);
        // var prob = [
        //   80,//0
        //   60,//1
        //   40,//2
        //   40,//3
        //   30,//4
        //   20//5
        // ];
        LOG("Difficulty level===============> "+this.prob[this.que_level]);
         if(rndVal <= this.prob[this.que_level]){
            botAnswer = this.answer_id;//correct answer
            }else{
                botAnswer = options[this.randomInt(1,3)];//wrong answer
            }

      //  botAnswer = "wrong"//cheat
        this.onMessage(this.players_Data[this.room_data.currentTurnId].client,{msg_type:MSG_TYPE.ANSWER_SUBMIT,answer:botAnswer});
         }catch(e){
          LOG_ERROR("ERROR :: "+e.stack);
      }
    } 

    onMessage (client, data) {
  try{
       if(typeof client == 'undefined'){
       return;
       }
    
        if(data.msg_type != MSG_TYPE.PING_PONG){
            LOG("message from client  : "+client.id+" client.sessionId "+client.sessionId ,JSON.stringify(data));
        }
        var msg_type = data["msg_type"];

        switch(msg_type){
            case MSG_TYPE.PING_PONG:
                this.sendMessage(MSG_TYPE.PING_PONG,client);
                break;
            case  MSG_TYPE.ANSWER_SUBMIT: 

                if(client.sessionId == this.players_Data[this.room_data.currentTurnId].client.sessionId){

                    if(this.room_data.que_time > 0 && this.currentGameState == GAME_STATE.IN_GAME ){

                        if (this.queTimeIntervalRef) {
                            this.queTimeIntervalRef.clear();
                        }
                        if(data["answer"] == this.answer_id){ 
                            this.answer_status = 1;
                            this.players_Data[client.playerIndex].options.ans_score += this.ans_score;
                            this.room_data.players[client.playerIndex].ans_score += this.ans_score;

                        }else{
                            this.answer_status = 0;
                            this.room_data.players[client.playerIndex].lives--;                     

                        }
                        this.sendMessage(MSG_TYPE.ANSWER_RESULT);



                        Object.assign(this.roomLog[this.current_que_index],{"user_id":this.players_Data[client.playerIndex].options.user_id,
                                                                            "payment_id":this.players_Data[client.playerIndex].options.payment_id,
                                                                            //  "que_id":this.que_buffer[this.current_que_index].id,
                                                                            "ans_id":this.answer_id,
                                                                            "ans_submit":data["answer"],
                                                                            "ans_time":this.room_data.que_time,
                                                                            "ans_status":this.answer_status,                                                                   
                                                                            "lives":this.room_data.players[client.playerIndex].lives,"tour_id":this.tour_id} );
                        //   LOG("}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}  "+JSON.stringify(this.roomLog));
                        if(this.room_data.players[client.playerIndex].lives <= 0){// player out condition
                            this.playerOut(client);
                        }
                        setTimeout(this.changeTurn.bind(this), CHANGE_TURN_WAIT_TIME * 1000);
                        //this.changeTurn(); 
                    }
                }else{
                    console.log("Alert ::   MSG_TYPE.ANSWER_SUBMIT by wrong user");
                }

                break;
        }
    }catch(e){
          LOG_ERROR("ERROR :: "+e.stack);
      }

    }



    fillQuestionBuffer(){

      try{
        return  new Promise((resolve, reject) => { 



            for(var count = 1;count<=10 && this.masterQueIndex<Object.keys(this.master_que_buffer.en).length;count++,this.masterQueIndex++){
                this.que_buffer.en= this.que_buffer.en.concat(this.master_que_buffer.en[this.masterQueIndex]); 
                this.que_buffer.hi= this.que_buffer.hi.concat(this.master_que_buffer.hi[this.masterQueIndex]); 

            }
            //LOG(JSON.stringify(this.que_buffer)+"______________________ this.que_buffer.length "+Object.keys(this.que_buffer).length);
            if(this.masterQueIndex >= Object.keys(this.master_que_buffer.en).length){
                LOG("______________________ Refill master_que_buffer ");
                var promise =    database.getQuestionsFromMaster({"game_id":this.game_id,"tour_id":this.tour_id,"masterRefillCount":++this.masterRefillCount});     
                promise.then((result)=>{ 
                    this.master_que_buffer.en =  JSON.parse(JSON.stringify(result)).en;
                    this.master_que_buffer.hi =  JSON.parse(JSON.stringify(result)).hi;
                    this.masterQueIndex = 0;
                    LOG("______________________ Refill master_que_buffer Success");
                    resolve(this.master_que_buffer.en);
                },(error)=>{ console.log("______________________ Refill master_que_buffer Failed ",error); });
            }else{
                resolve(this.que_buffer);
            }
        }); 
      }catch(e){
                LOG_ERROR("ERROR :: "+e.stack);
            }

    }   

    queTimeInterval(){  
      try{
        var client = this.players_Data[this.room_data.currentTurnId].client;
        //  if(client.sessionId == this.players_Data[this.room_data.currentTurnId].client.sessionId){
        this.room_data.que_time--;
        LOG("question timer = ",this.room_data.que_time);
        if(this.room_data.que_time <= -QUE_TIME_OUT_BUFFER){   

            if (this.queTimeIntervalRef) {
                this.queTimeIntervalRef.clear();   
            }
           // this.room_data.que_time = 0;
            this.answer_status = 0;
            this.room_data.players[client.playerIndex].lives--;                    
            this.sendMessage(MSG_TYPE.QUE_TIME_OUT);

            Object.assign(this.roomLog[this.current_que_index],{"user_id":this.players_Data[client.playerIndex].options.user_id,
                                                                "payment_id":this.players_Data[client.playerIndex].options.payment_id,
                                                                // "que_id":this.que_buffer[this.current_que_index].id,
                                                                "ans_id":this.answer_id,
                                                                "ans_submit":"TIME_OUT",
                                                                "ans_time":this.room_data.que_time,
                                                                "ans_status":this.answer_status,                                                                   
                                                                "lives":this.room_data.players[client.playerIndex].lives,"tour_id":this.tour_id} );
            //LOG("}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}  "+JSON.stringify(this.roomLog));

            if(this.room_data.players[client.playerIndex].lives <= 0){// player out condition
                this.playerOut(client);
            }


            setTimeout(this.changeTurn.bind(this), CHANGE_TURN_WAIT_TIME * 1000);
            //this.changeTurn(); 
        }
        //}
     }catch(e){
           LOG_ERROR("ERROR :: "+e.stack);
      }
    }

    playerOut(client){
       try{
         if(typeof client == 'undefined'){
       return;
       }
        this.room_data.players[client.playerIndex].tour_lives--;
        this.players_Data[client.playerIndex].options.tour_lives--;
       if(this.players_Data[client.playerIndex].options.tour_lives <= 0){
            this.players_Data[client.playerIndex].options.points = 0;  
            this.players_Data[client.playerIndex].options.level = 0;  
            this.players_Data[client.playerIndex].options.total_points = 0;
        }
        this.room_data.players[client.playerIndex].win_amount = 0;
       Object.assign( this.room_data.players[client.playerIndex],{rewards:this.gameLostRewards});
       Object.assign( this.players_Data[client.playerIndex].options,{rewards:this.gameLostRewards});
       if( this.players_Data[client.playerIndex].options.isRefund != true){
       var promiseRewards = database.insertUserRewards(this.players_Data[client.playerIndex].options);
       }
       var promiseTourLives = database.updateLivesCount(this.players_Data[client.playerIndex].options);
       
       var promise =  database.setUserLevelAndPoints(this.players_Data[client.playerIndex].options);
       var promise1 =  database.updateUserTable({"game_id":this.game_id,"tour_id":this.tour_id,"user_id":this.players_Data[client.playerIndex].options.user_id,"payment_id":this.players_Data[client.playerIndex].options.payment_id,"lives":this.room_data.players[client.playerIndex].lives,"ans_score":this.room_data.players[client.playerIndex].ans_score,"status":0});
   
        setTimeout(this.sendMessage.bind(this,MSG_TYPE.YOU_LOST,client), (CHANGE_TURN_WAIT_TIME-0.9) * 1000);
     
        this.player_available--; 
         }catch(e){
         LOG_ERROR("ERROR :: "+e.stack);
      }
    }
     
    onGameEnd(){
   try{
        LOG("_________________________________________ onGameEnd _________________________",this.game_id);
       // this.lock();
        if(this.currentGameState != GAME_STATE.NONE){
            LOG("**********************************  Room Log = "+this.currentGameState );         

 
//             for(var id = 0 ;id<Object.keys(this.players_Data).length;id++){
//                 // give coins to lost user
//                 if(this.players_Data[id].options.is_bot== 0 && this.players_Data[id].options.ans_score > 0 ){
//                     database.updateCoinRewards({"game_id":this.game_id,"user_id":this.players_Data[id].options.user_id,"payment_id":this.players_Data[id].options.payment_id,"ans_score":this.room_data.players[id].ans_score});
//                 }

//             } 
          for(var id =0 ;id<Object.keys(this.options_Data).length;id++){ 
                   var payment_id = this.options_Data[id].options.payment_id;
                     if(payment_id != 'bot_payment_id'){
                         this.presence.publish("DEL_PAYMENT_ID",""+payment_id);                       
                     }
                 } 

           //this.presence.unsubscribe("ADD_PAYMENT_ID");
           //  this.presence.unsubscribe("DEL_PAYMENT_ID");  
          
            database.saveLog({"game_id":this.game_id,"tour_id":this.tour_id,"end_time":null,"game_state":this.currentGameState,"room_log":this.roomLog});
        }
     // if(this.currentGameState != GAME_STATE.GAME_CLOSE){
     //  this.clock.setTimeout(this.closeRoom.bind(this), CLIENT_REJOIN_WAIT_TIME * 3000);
     // }else{
       this.closeRoom();
     // }
     
     }catch(e){
           LOG_ERROR("ERROR :: "+e.stack);
      }

    }
   closeRoom(){
     try{
        this.lock();
        //this.setSeatReservationTime(1);  
        //this.disconnect ();
        for(var id =0 ,length = this.clients.length;id<length;id++){   
           this.clients[id].close();        
        } 
       }catch(e){
          LOG_ERROR("ERROR :: "+e.stack);
      }
   }
      
    onDispose () {


        /*
  for(var i=0;i<Object.keys(this.roomLog).length;i++){
         LOG("** ",JSON.stringify(this.roomLog[i]));
  }
  */
      try{

        if(this.currentGameState == GAME_STATE.IN_GAME){
            LOG("**********************************  Room Log = "+this.currentGameState );
            database.saveLog({"game_id":this.game_id,"tour_id":this.tour_id,"end_time":null,"game_state":this.currentGameState,"room_log":this.roomLog});
        }
        
              
       // this.disconnect ();

        if(this.opponentWaitTimeOutRef){
            this.opponentWaitTimeOutRef.clear();
        }

        if(this.botWaitTimeRef){
            this.botWaitTimeRef.clear();
        } 

        if (this.queTimeIntervalRef) {
            this.queTimeIntervalRef.clear();
        }
        LOG("Dispose BasicRoom");
        }catch(e){
            LOG_ERROR("ERROR :: "+e.stack);
      }
    }
    randomInt(min,max) // min and max included
    {
        return Math.floor(Math.random()*(max-min+1)+min);
    } 
    getMaskNumber(number,startUnMask,endUnMask,maskBy){
        var first = number.substring(0, startUnMask);
        var last = number.substring(number.length - endUnMask);

        var mask = number.substring(startUnMask, number.length - endUnMask).replace(/\d/g,maskBy);
        return (first + mask + last);
    }


}