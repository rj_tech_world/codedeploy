
export class DateTime{
  locale = 'en-US';
 options = {
timeZone: 'Asia/Kolkata',
year: 'numeric',
month: 'numeric',
day: '2-digit',
hour12:false,
hour :'2-digit',
minute :'2-digit',
second:'2-digit',
formatMatcher:'best fit'
};
   format(x, y) {
    var z = {
        M: x.getMonth() + 1,
        d: x.getDate(),
        h: x.getHours(),
        m: x.getMinutes(),
        s: x.getSeconds()
    };
    y = y.replace(/(M+|d+|h+|m+|s+)/g, function(v) {
        return ((v.length > 1 ? "0" : "") + eval('z.' + v.slice(-1))).slice(-2)
    });

    return y.replace(/(y+)/g, function(v) {
        return x.getFullYear().toString().slice(-v.length)
    });
}
  getDateTime(locale ,options,_format){
  return this.format(new Date(new Date().toLocaleString(locale,options)),_format);
  }
 
}
//var nDate = new Date().toLocaleString('en-US',options);

//var dt =  dateTime(new Date(nDate), 'yyyy-MM-dd hh:mm:ss');
